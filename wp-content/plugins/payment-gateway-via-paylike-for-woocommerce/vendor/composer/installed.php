<?php return array(
    'root' => array(
        'pretty_version' => '1.8.7',
        'version' => '1.8.7.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'lunar/plugin-woocommerce',
        'dev' => false,
    ),
    'versions' => array(
        'lunar/plugin-woocommerce' => array(
            'pretty_version' => '1.8.7',
            'version' => '1.8.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'paylike/php-api' => array(
            'pretty_version' => '1.0.8',
            'version' => '1.0.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paylike/php-api',
            'aliases' => array(),
            'reference' => 'bcdf66a17ff3c595952d02de250264e3d2cb307d',
            'dev_requirement' => false,
        ),
    ),
);
