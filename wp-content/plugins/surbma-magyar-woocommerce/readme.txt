=== HuCommerce | Magyar WooCommerce kiegészítések ===
Contributors: Surbma, xnagyg
Tags: woocommerce, magyar, magyarország, webáruház, hungarian, hungary
Requires at least: 5.3
Tested up to: 6.0
Stable tag: 2022.3.5
Requires PHP: 7.0
License: GPLv3 or later License
URI: http://www.gnu.org/licenses/gpl-3.0.html

Hasznos javítások a magyar WooCommerce webáruházakhoz.

== Description ==

>Hasznos javítások a magyar WooCommerce webáruházakhoz.

A WooCommerce a világ és most már Magyarország legnépszerűbb webáruház platformja is. A HuCommerce kiegészítővel könnyebben és gyorsabban indíthatod a webáruházad, mert a legtöbb fontos funkciót és fejlesztést egy gombnyomással helyettesítheted.

A HuCommerce bővítmény ingyenes változata most már évek óta a magyar WooCommerce webáruház tulajdonosok kedvenc kiegészítője. Több, mint 9000 webáruházban megtalálható és a letöltések száma folyamatosan nő.

Azon dolgozunk, hogy folyamatosan hasznos funkciókkal bővítsük ezt a nagyszerű WooCommerce kiegészítőt. Kérlek, támogasd a fejlesztést és a HuCommerce közösséget azzal, hogy megvásárolod a Pro verziót! Ezzel fedezni tudjuk a fejlesztési költségeket és több időt tudunk arra fordítani, hogy a HuCommerce bővítmény még jobb legyen és még több hasznod legyen belőle.

A HuCommerce ingyenes verziója rengeteg olyan apró, hasznos vagy akár jogilag kötelező funkciót tartalmaz, ami hiányzik a WooCommerce bővítményből. Többek között rendbe teszi a vezetéknév és keresztnév sorrendjét és az adószám mezőt is hozzáadja a pénztárhoz, de folyamatosan bővül hasonló praktikus, hasznos vagy kötelező funkciókkal.

A HuCommerce Pro előfizetés éves díja egy kezdő magyar fejlesztő pár órás munkadíja, miközben neked napokat vagy akár hónapokat spórol meg fejlesztési és kutatási oldalon. Ez a kiegészítő a leghasznosabb és nélkülözhetetlen funkciókkal egészíti ki a webáruházad. A fizetős konstrukció biztosítja, hogy folyamatosan bővüljön újabb hasznos funkciókkal.

A funkciók folyamatosan bővülnek, de mindenki megírhatja a véleményét, ötletét, hogyan tudjuk egyre jobbá tenni ezt a bővítményt.

#### HuCommerce támogatói Facebook csoport

Csatlakozzatok a HuCommerce hivatalos támogatói Facebook csoportjához, ahol lehet a bővítménnyel kapcsolatban kérdezni, beszélgetni, ötletelni. Mindenkit szívesen látunk: [HuCommerce Facebook csoport](https://www.facebook.com/groups/HuCommerce.hu/)

### HuCommerce (ingyenes)

Több, mint 9000 webáruház tulajdonos választása Magyarországon. Rengeteg hasznos funkció, kötelező választás minden magyar WooCommerce webáruházhoz.

[Bővebben a HuCommerce bővítményről →](https://www.hucommerce.hu/bovitmenyek/hucommerce/)

**HuCommerce ingyenes funkciói:**

- Vezetéknév és keresztnév rendbetétele (WooCommerce 4.4 verzió fölött is)
- Adószám bekérése vásárlásnál
- Megye mező elrejtése
- Pénztár mezők ellenőrzése
- Város automatikus kitöltése az irányítószám alapján
- Pénztár oldal formázása
- Kupon mező módosítások
- Plusz/minusz mennyiségi gombok a termékekhez
- Automatikus frissítés a Kosár oldalon
- Egy termék vásárlásonként
- Kosár átirányítása a Pénztár oldalra
- Vásárlás folytatása gomb megjelenítése a Kosár és/vagy a Pénztár oldalakon
- Belépés és regisztráció utáni átirányítás
- Ingyenes szállítás értesítés
- Szállítási módok elrejtése
- Egyedi “Kosárba teszem” gombok
- Termék extra beállítások
- Globális adatok, amiket shortcode-dal lehet bárhol megjeleníteni
- SMTP szolgáltatás beállítása
- Fordítási hiányosságok javítása
- WPML és Polylang kompatibilitás
- Alap ügyfélszolgálat és támogatás
- További funkciók hamarosan…

### HuCommerce Pro (fizetős)

A HuCommerce kibővített kiadása, további hasznos és folyamatosan bővülő funkciókkal. Támogatói verzió, amivel te is hozzájárulsz a további fejlesztésekhez.

[Bővebben a HuCommerce Pro bővítményről →](https://www.hucommerce.hu/bovitmenyek/hucommerce/)

>[Iratkozz fel a HuCommerce hírlevélre](https://www.hucommerce.hu/hc/hirlevel-feliratkozas/) és cserébe kapsz tőlünk egy 5.000 Ft értékű kupont, amit a HuCommerce Pro előfizetésnél beválthatsz.

**HuCommerce Pro további funkciói:**

- Jogi megfelelés (Fogyasztóvédelem, GDPR, ePrivacy, stb.)
- Termék ár történet (új Fogyasztóvédelemi rendelet) - BÉTA
- Kiegészítők és sablonok fordításai
- Kiemelt ügyfélszolgálat és támogatás
- További funkciók hamarosan…

**HuCommerce tudásbázis**

A bővítmény használatáról, a Pro verzió aktiválásáról és az egyes modulokról részletesen a [HuCommerce tudásbázisunkban](https://www.hucommerce.hu/tudasbazis/) olvashatsz.

#### Vezetéknév és keresztnév rendbetétele (WooCommerce 4.4 verzió fölött is)

A keresztnév és vezetéknév sorrendjének a megfordítása, ha a webáruház magyar nyelvre van állítva. Ahogy mi szeretjük. :) Mindezt úgy, hogy reszponzív nézetben is jó legyen és a CRM, számlázó programok is tudják értelmezni.

A megjelenítés kompatibilis a WPML bővítménnyel, így többnyelvű webáruháznál is magyar sorrendben jelenik meg a név, ha magyar nyelven nézik a webáruházat a látogatók.

A név sorrendje a megrendelés visszaigazolásánál, a vásárló fiókjában és az értesítő levelekben is jól jelenik meg.

#### Adószám bekérése vásárlásnál

A Pénztár oldalon a Cégnév mező alatt már Adószámot is be lehet kérni. Az Adószám mező csak akkor jelenik meg, ha a Cégnév mezőbe lett írva. Az adószám ebben az esetben kötelező mező. Az adószám a rendelésen kívül az adott felhasználó profil adatainál is elmentésre kerül. Az adószám megjelenik mind a visszaigazoláson, mind a rendelés szerkesztésénél, valamint az értesítő levelekben is.

Már maszkolás és validálás is beállítható az Adószám mezőhöz és placeholder megjelenítése is bekapcsolható, ami azoknál a sablonoknál nagyon jól jön, amiknél nem jelennek meg a mezők címkéi.

#### Megye mező elrejtése

Mert ezt nálunk nem szokás használni, így csak plusz felesleges lépés. De ha valakinek nagyon kell, akkor visszaállítható.

#### Pénztár mezők ellenőrzése

Beállítható, hogy a Pénztár oldalon a Számlázási adatoknál az Adószám, Irányítószám és Telefonszám mezőket, a Szállítási adatoknál pedig az Irányítószámot maszkolja, azaz formai szempontból validálja, illetve a megadott beviteli értéket ellenőrizze. Így a következő értékek lesznek érvényben:

- Számlázási Adószám: 00000000-0-00 (13 karakter), 00000000000 (11 szám), HU00000000 (HU előtag és 8 szám)
- Számlázási és Szállítási Irányítószám: 0000 (4 karakter)
- Számlázási Telefonszám: +36000000000 (11-12 karakter)

#### Város automatikus kitöltése az irányítószám alapján

A Pénztár oldalon az irányítószám mező kitöltése után automatikusan megjeleníti a várost. Ha már manuálisan lett módosítva a város, akkor nem módosítja az irányítószám alapján.

Vannak olyan irányítószámok, amikkel nem működik, mert vagy még hiányzik az indexből vagy egy irányítószám több településhez is tartozik. Igyekszem az ilyen hiányosságokat javítani.

#### Fordítási hiányosságok javítása

Ideiglenes fordítási hiányosságok javítása a WooCommerce bővítményhez és a legnépszerűbb sablonokhoz (Divi, Storefront), amíg a hivatalos fordításban esetleg nem jelenik meg vagy nem frissíti a rendszer. Én hivatalos szerkesztője is vagyok a magyar WooCommerce fordítási csapatának, ezért ott sokmindent megcsinálok, de néha szükség van erre a kis trükkre.

#### Alap ügyfélszolgálat és támogatás

Ügyfélszolgálati kérdésekkel kapcsolatban a [Facebook csoportban](https://www.facebook.com/groups/HuCommerce.hu/) és itt, a [WordPress.org saját fórum felületén](https://wordpress.org/support/plugin/surbma-magyar-woocommerce/) van lehetőség írni nekünk.

#### Jogi megfelelés (Fogyasztóvédelem, GDPR, ePrivacy, stb.)

Lehetőség van az Általános Szerződési Feltételek és az Adatkezelési tájékoztató aktív cselekvésen alapuló elfogadtatására, azaz ki kell pipálnia a vásárlónak ezek elfogadását, mielőtt a rendelést leadhatná. Az adatok a rendeléseknél kerülnek elmentésre és a rendelés szerkesztése oldalon megjelenik az elfogadott státusz. Ebben az esetben a vásárló profil adatainál nem kerül elmentésre az elfogadás, így azt bejelentkezve is minden vásárlás alkalmával el kell fogadnia.

Opcionális a két mező használata, így ha valakinek csak az egyik elfogadtatására és megerősítésére van szüksége, akkor egyik vagy másik kikapcsolható.

További két extra mező is hozzáadható, amihez teljesen szabadon lehet szöveget, linket megadni. Ezekre további jogi megfelelés céljából lehet szükség, ha az adott webáruház megköveteli ezt. Amennyiben bármelyik mező aktiválva lett, azok mindenképpen kötelezőek.

A Pénztár oldalon a Megrendelés gomb fölött és/vagy alatt közvetlenül elhelyezhető jogi szöveg, ami esetleg fontos vagy kötelező eleme a vásárlási folyamatnak. Ilyen például a távollévők közötti szerződéshez szükséges tudomásulvétel vagy a "fizetési kötelezettséggel járó megrendelés" kötelező megjelenítése a megrendelés során.

A regisztrációs űrlapnál is kérhető az Adatkezelési Tájékoztató kötelező elfogadtatása. Ez az adat már elmentésre kerül a felhasználó profil adatainál. Külön beállítható, hogy a felhasználó IP címét is elmentse a rendszer. A regisztrációs adatokat (elfogadás ténye, regisztráció dátuma, IP cím) mind az admin felületen, mind pedig a felhasználó fiókadatainál megjeleníti, de ezek a mezők nem módosíthatók sem a felhasználók, sem az adminisztrátorok részéről.

>**FIGYELEM!** A webáruház jogi megfelelése az aktuális törvényeknek és adatvédelmi rendeleteknek minden esetben a webáruház tulajdonosának a felelőssége. Ez az opció nem mentesít senkit sem az alól, hogy a megfelelést felülvizsgáltassa szakértővel vagy jogásszal. A fejlesztők nem vállalnak semmilyen felelősséget a webáruház jogi megfeleléséért.

#### Termék ár történet (új Fogyasztóvédelemi rendelet: 4/2009. (I. 30.) NFGM-SZMM együttes rendelet 2/A. §) - BÉTA

Ez a modul nincs minden körülmény között tesztelve és nem tudja 100%-ban teljesíteni a funkcionális és/vagy jogi igényeket, feltételeket. Ezért a használata esetén fokozott figyelmet igényel.

>**FIGYELEM!** A HuCommerce ügyfélszolgálatára beküldött visszajelzések és javaslatok jelentősen gyorsítják a modul fejlesztését, ezért szívesen várjuk az ilyen témájú megkereséseket. Köszönjük!

Az Európai Unió területén, így Magyarországon is 2022. május 28-tól egységes szabályok vonatkoznak az árcsökkentésre.

Az új szabályozás célja annak megakadályozása, hogy a kereskedők megtévesztő árcsökkentéseket alkalmazzanak azzal, hogy az akciók előtt megemeljék a korábbi árat és ezáltal megtévesszék a fogyasztókat az árengedmény mértékével kapcsolatban.

>A 93/13/EGK tanácsi irányelvnek, valamint a 98/6/EK, a 2005/29/EK és a 2011/83/EU európai parlamenti és tanácsi irányelvnek az uniós fogyasztóvédelmi szabályok hatékonyabb végrehajtása és korszerűsítése tekintetében történő módosításáról szóló, 2019. november 27-i (EU) 2019/2161 európai parlamenti és tanácsi irányelv érintett rendelkezései a termékek eladási ára és egységára, továbbá a szolgáltatások díja feltüntetésének részletes szabályairól szóló 4/2009. (I. 30.) NFGM-SZMM együttes rendelet (a továbbiakban: Árrendelet) 2/A. §-ában kerültek átültetésére.

Ez a modul biztosítja a rendeletnek való megfelelést. A termékhez kapcsolódó árak folyamatos mentésével és annak statisztikai megjelenítésével egy átfogó képet ad a webáruház tulajdonosnak az árváltozás és az aktuális kedvezmény rendelet alapján történő meghatározásáról. A modul minden termékhez létrehoz egy adatlapot, ahol táblázatban és vonal diagramon is mutatja az adatokat. A teljes adatbázis itt JSON és CSV formátumban kimásolható.

A termék végoldalon pedig automatikusan létrehozott és számolt vagy manuálisan megadott szöveg is megadható, ami biztosítja a látogatók megfelelő információval való ellátását. Ezáltal tud megfelelni a webáruház a Fogyasztóvédelmi elvárásoknak.

>**FIGYELEM!** A webáruház jogi megfelelése az aktuális törvényeknek és adatvédelmi rendeleteknek minden esetben a webáruház tulajdonosának a felelőssége. Ez az opció nem mentesít senkit sem az alól, hogy a megfelelést felülvizsgáltassa szakértővel vagy jogásszal. A fejlesztők nem vállalnak semmilyen felelősséget a webáruház jogi megfeleléséért.

#### Pénztár oldal módosítások

Céges számlázási adatok feltételes megjelenítése. Ebben az esetben egy checkbox jelenik meg és ha a látogató bepipálja, akkor jelennek csak meg a céges számlázás mezői, mint például a Cégnév és Adószám.

A Cégnév és Adószám, az Irányítószám és Város, valamint a Telefonszám és Email cím mezőket nagy monitoron be lehet állítani, hogy egymás mellé kerüljenek. Az Irányítószám és Város mezők az Ország mező alatt jelennek meg közvetlenül, hogy logikusabb legyen a megjelenési sorrend.

Az Ország és a Rendelés jegyzetek mezőket akár ki is lehet kapcsolni, ha ezek a mezők nem relevánsak a te webáruházadnál. Ha az Ország mezőt elrejted, akkor a Bolt beállításainál kiválasztott ország lesz alapértelmezettként beállítva megrendelésnél.

#### Kupon mező módosítások

A Pénztár oldalon megjelenő kupon mező áthelyezése, elrejtése vagy éppen mindig láthatóvá tétele lehetséges ezzel a modullal.

#### Plusz/mínusz mennyiségi gombok a termékekhez

A WooCommerce alapból csak egy szám típusú mezőt használ a termékek mennyiségénél, de ez felhasználói szemmel nézve nem elég. Ez a funkció a mennyiségi mező elé és után betesz egy plusz/minusz gombot, amivel a felhasználók könnyedén tudják változtatni a mennyiséget mind a termék végoldalon, mind pedig a kosár összegzés oldalán.

FIGYELEM! A gombok a különböző sablonoknál esetleg másképp vagy nem megfelelően jelenhetnek meg. Ez minden esetben javítható egy kis CSS segítségével. Én azon vagyok, hogy a lehető legnépszerűbb sablonoknál már automatikusan jól nézzen ki, illetve kap egy alap formázást is, hogy a legtöbb esetben megfelelő legyen, de biztosan lesznek olyan sablonok, ahol ez még így is igényel majd további CSS formázást. A bővítmény támogatás fórumában lehet ezeket jelezni, de nem tudom vállalni, hogy mindenkinek, gyorsan tudok segíteni.

Jelenleg ezeket a sablonokat biztosan támogatja alapból a bővítmény:

- Storefront
- Divi
- Avada

#### Automatikus frissítés a Kosár oldalon

A Kosár oldalon a termékek mennyiségének a módosításakor nincs szükség a "Kosár frissítése" gomb megnyomására a darabszám módosítása után, mert így automatikusan frissül a Kosár tartalma.

#### Egy termék vásárlásonként

A modul bekapcsolásával a látogatók egy adott vásárlási folyamatban csak egy terméket tudnak a kosárba tenni és megvásárolni. További vásárláshoz újabb rendelést kell leadniuk.

#### Kosár átirányítása a Pénztár oldalra

A modul lehetővé teszi, hogy a Kosár oldal automatikusan a Pénztár oldalra legyen irányítva, ezzel is gyorsítva a vásárlási folyamatot. Mindegy, hogy melyik oldalt használod Kosár oldalként vagy Pénztár oldalként, az átirányítás mindig működni fog.

#### Vásárlás folytatása gomb megjelenítése a Kosár és/vagy a Pénztár oldalakon

A Kosár és a Pénztár oldalakon megjeleníthető egy plusz Vásárlás folytatása gomb, ami az üzlet oldalra viszi a látogatókat, hogy esetleg még tovább válogassanak a termékek között. Sokszor csak kíváncsiságból kattintanak a látogatók a Kosár gombra, de még nem fejezték be a vásárlásukat.

A gombok pozíciója mind a Kosár, mind a Pénztár oldalon beállítható, valamint van lehetőség egyedi üzenet megjelenítésére is bizonyos pozíciókban.

#### Belépés és regisztráció utáni átirányítás

Beállítható, hogy a látogatók a belépés és regisztráció után a meghatározott oldalra legyenek automatikusan átirányítva. A belépéshez és regisztrációhoz külön-külön állítható be a cél URL. A Pénztár oldalon nem veszi figyelembe az egyedi beállítást, hogy ott ne zavarja a vásárlás befejezését.

#### Ingyenes szállítás értesítés

A termék listaoldalakon, illetve a Kosár és Pénztár oldalakon jeleníthető meg az értesítés, hogy mennyi vásárlási összeg hiányzik még az ingyenes szállításhoz. A szöveg módosítható és többnyelvűsíthető.

#### Szállítási módok elrejtése

Ingyenes szállítás esetén ezzel a modullal elrejtheted az összes nem releváns szállítási módot. Lehetőséged van a személyes átvétel és a “pont” szállítási módok megtartására is.

#### Egyedi "Kosárba teszem" gombok

Lehetőséged van teljesen egyedi szövegek megadására a “Kosárba teszem” szöveg helyett. Sőt a különböző termék típusok (egyszerű, előfizetés, tagság, stb.) esetén különböző szövegeket is megadhatsz.

#### Termék extra beállítások

Új lehetőségekkel bővülnek a Termékekhez kapcsolódó beállítások. Ez a funkció folyamatosan fog bővülni.

**Jelenleg ezek a lehetőségek lettek beépítve:**

- Kosárba tesz gomb – A termék lista oldalakon az egyes termékekhez hozzá lehet adni a Kosárba tesz gombot.
- Termék alcím hozzáadása – Egyedi alcímek adhatók a Termékekhez, melyek a listaoldalon és a Termék végoldalon is megjelennek. Az alcím a cím heading HTML kódjába lesz illesztve külön, szemantikus megjelöléssel. Ezt fontos figyelembe venni keresőoptimalizálási (SEO) szempontból is!
- Kapcsolódó termékek kikapcsolása a termék végoldalakon
- Termék lista oldal: termékek számának beállítása oldalanként
- Termék lista oldal: oszlopok számának beállítása
- Upsell termékek száma a termék végoldalakon
- Upsell termékek száma soronként a termék végoldalakon
- Kapcsolódó termékek száma a termék végoldalakon
- Kapcsolódó termékek száma soronként a termék végoldalakon

#### Globális adatok

Előre beállítható adatok, amiket rövidkódokkal a webáruház bármely részén megjeleníthetsz. Például a webáruház telefonszáma, ami ha egyszer változik, csak itt kell módosítanod és minden helyen egyszerre a jó telefonszám fog megjelenni. Egyes adatok aktívan jelennek meg, így a telefonszám egy kattintással hívható lesz és az email cím is kattintható formában és titkosítva jelenik meg, hogy a botok ne tudják kiolvasni a kódból.

Ezek az adatok állíthatók be:

- Név
- Cégnév
- Székhely
- Adószám
- Cégjegyzékszám
- Cím
- Bankszámlaszám
- Mobiltelefon
- Telefon
- Email cím
- Rólunk szöveg

#### SMTP szolgáltatás beállítása

Minden webáruházban fontos a kommunikáció az érdeklődővel és vásárlóval mind a vásárlás előtt, mind pedig a megrendelés leadása után. Ezért nagyon fontos, hogy a levelek kézbesítése biztonságos és megbízható legyen. Ezt a külső SMTP szolgáltatók tudják biztosítani, mint például a Mailgun vagy Sendgrid.

A HuCommerce Pro SMTP beállításával összekapcsolhatod a webáruházad levelezését az SMTP szolgáltatóval.

#### Prémium WooCommerce kiegészítők és sablonok fordításai

Magyar fordítások a legnépszerűbb WooCommerce kiegészítőkhöz és sablonokhoz. Vannak olyanok, amikhez nincsenek fordítások és vannak olyanok, amikhez van, de nagyon rossz a hivatalos fordítás. A fordítások folyamatosan bővülnek.

#### WPML és Polylang kompatibilitás

A szöveges mezők kompatibilisek a WPML, Polylang bővítményekkel, így azok beállíthatók a különböző nyelveken is.

#### Kiemelt ügyfélszolgálat és támogatás

A HuCommerce Pro ügyfelek számára email és live chat csatornákon is biztosítjuk az ügyfélszolgálati ügyek intézését és a támogatást a bővítményhez.

>Azon dolgozunk, hogy folyamatosan hasznos funkciókkal bővítsük ezt a nagyszerű WooCommerce kiegészítőt. Kérlek, támogasd a fejlesztést és a HuCommerce közösséget azzal, hogy megvásárolod a Pro verziót! Ezzel fedezni tudjuk a fejlesztési költségeket és több időt tudunk arra fordítani, hogy a HuCommerce bővítmény még jobb legyen és még több hasznod legyen belőle.

### Szeretnél többet tudni rólunk és a szolgáltatásainkról?

Nézd meg a weboldalunkat: [HuCommerce.hu →](https://www.hucommerce.hu/)

== Installation ==

### Automatikus telepítés

1. A "Bővítmények -> Új hozzáadása" menüpont alatt keress rá a *HuCommerce | Magyar WooCommerce kiegészítések* bővítményre.
2. A bővítmény dobozában kattints a "Telepítés most" gombra.
3. Telepítés után ugyanebben a dobozban kattints a "Bekapcsol" gombra, hogy aktiváld a *HuCommerce | Magyar WooCommerce kiegészítések* bővítményt.
4. A "WooCommerce -> HuCommerce" menüpont alatt állítsd be, hogy melyik modult szeretnéd használni.
5. Ennyi az egész. :)

### Manuális telepítés az admin felületen

1. Töltsd le a bővítményt: [HuCommerce | Magyar WooCommerce kiegészítések](https://downloads.wordpress.org/plugin/surbma-magyar-woocommerce.zip)
2. Töltsd fel a `surbma-magyar-woocommerce.zip` fájlt a "Bővítmények -> Új hozzáadása" menüpont alatt a "Bővítmény feltöltése" gombra kattintva.
3. Aktiváld a *HuCommerce | Magyar WooCommerce kiegészítések* bővítményt a feltöltés után.
4. A "WooCommerce -> HuCommerce" menüpont alatt állítsd be, hogy melyik modult szeretnéd használni.
5. Ennyi az egész. :)

### Manuális telepítés FTP használatával

1. Töltsd le a bővítményt: [HuCommerce | Magyar WooCommerce kiegészítések](https://downloads.wordpress.org/plugin/surbma-magyar-woocommerce.zip)
2. Tömörítsd ki a zip fájlt a számítógépeden.
3. Töltsd fel a `surbma-magyar-woocommerce` mappát a `/wp-content/plugins/` mappába.
4. Aktiváld a *HuCommerce | Magyar WooCommerce kiegészítések* bővítményt a "Bővítmények" menüpont alatt a WordPress admin felületen.
5. A "WooCommerce -> HuCommerce" menüpont alatt állítsd be, hogy melyik modult szeretnéd használni.
6. Ennyi az egész. :)

== Frequently Asked Questions ==

= Hol találom a bővítmény beállításait? =

A *HuCommerce | Magyar WooCommerce kiegészítések* bővítmény beállításait a "WooCommerce -> HuCommerce" menüpont alatt éred el.

= Nem cserélődtek meg a nevek a Pénztár oldalon. =

Először töröld a szerver oldali és a böngésző gyorsítótárát és frissítsd az oldalt! Győződj meg róla, hogy esetleg más bővítmény nem okoz-e konfliktust! Ha a fordítást módosítottad, az is lehet probléma. Illetve a sablonok is tartalmazhatnak olyan egyedi kódokat, amivel ez a funkció felülírható.

Figyelem! A nevek cseréje csak akkor történik meg, ha magyar nyelvre van állítva a webáruház.

= Mit jelent az, hogy Surbma? =

A vezetéknevem visszafelé. ;)

== Changelog ==

= 2022.3.5 =

Kiadás dátuma: 2022-06-19

- A Szállítási módok elrejtése modul további javítása, hogy minden beállításnál a megfelelő szállítási módokat mutassa.

= 2022.3.4 =

Kiadás dátuma: 2022-06-19

- SMTP szolgáltatás kiegészítése teszt email küldéssel.
- Új értesítés a teszt email küldés után.
- Új értesítés az API szinkronizálás után.

= 2022.3.3 =

Kiadás dátuma: 2022-06-19

- A Szállítási módok elrejtése modul javítása, hogy minden beállításnál a megfelelő szállítási módokat mutassa.

= 2022.3.2 =

Kiadás dátuma: 2022-06-13

- A Jogi megfelelés pozíció értékének a validálása kiszűri az esetlegesen rosszul elmentett értéket az adatbázisban.

= 2022.3.1 =

Kiadás dátuma: 2022-06-13

- A Pénztár oldali checkbox mezők kódjának a javítása, hogy minden WooCommerce natív jelölést megkapjanak.
- A Beállítások oldalon az Adószám mező megjelenítésének a javítása.
- A Jogi megfelelés egyik opciójának az alapértelmezett értékét kellett javítani.

= 2022.3.0 =

Kiadás dátuma: 2022-06-11

Ebben a verzióban a Termék ár történet modul lett továbbfejlesztve. Megbízhatóbb ár mentéssel és egy új opcióval érkezik, hogy a termék ár statisztikát a látogatók számára is meg lehessen jeleníteni.

- Új opció a Termék ár történet statisztika megjelenítésére a látogatók számára.
- Régi felhasználóknak kiírt figyelmeztetés törlése.
- Egyedi előfizetés azonosító megadási lehetősége a licensz kezelésnél.
- Információknál a weboldal információk másolásánál a figyelmeztetés módosítása natív böngésző kisablakra.
- Termék ár történet kód optimalizálás és biztonsági feltételek hozzáadása.
- Termék ár történet algoritmusának a jelentős javítása.
- Termék ár történet most már a WooCommerce importálással és a WP All Import bővítmény importálásával is kompatibilis.
- Termék ár történet korlátozott statisztika megjelenítése a látogatók számára.
- Termék ár történet statisztika megjelenítésének a módosítása.
- Termék ár történet statisztika kiegészítése CSV formátummal.

= 2022.2.1 =

Kiadás dátuma: 2022-06-08

- Az SMTP szolgáltatás modul is átkerült az ingyenes modulok közé. :)

= 2022.2.0 =

Kiadás dátuma: 2022-06-07

Ez a verzió újra elérhetővé teszi a régi modulokat az ingyenes verzióban, sőt, több fizetős modul is ingyenes lett. :) A frissítése éppen ezért ajánlott és biztonságos.

- Leírás kisebb módosítása.
- A kisebb funkcióval rendelkező modulok mostantól ingyenesek újra. A legtöbb új funkció is mind ingyenes lett.
- A régi HuCommerce felhasználóknak szóló értesítések törlése, illetve áthelyezése, hogy ne legyen zavaró.
- A beállítások oldalon az ajánlat banner törlése, hogy ne legyen zavaró.
- Kisebb javítás a productpricehistory-discounttext mező mentésénél.
- A legacyuser mentése, hogy lehessen feltételeket megadni a régi felhasználókra.
- A Pro modulok vizuális különválasztása a beállítások oldalon.
- A Jogi megfelelés modul korlátlan ideig használható az ingyenes verzióval is, csak nem módosítható.

= 2022.1.5 =

Kiadás dátuma: 2022-06-01

- Hiányzó fájlok javítása.

= 2022.1.1 =

Kiadás dátuma: 2022-06-01

- Fordítások javítása az admin felületen.
- Kisebb módosítások a leírásban.

= 2022.1.0 =

Kiadás dátuma: 2022-05-31

- A HuCommerce és a HuCommerce Pro első éles kiadása.
- Minden változás a következő verziókban olvasható: 2022.0.0.1 - 2022.0.35
- Boldog születésnapot Dorina! :)

= 2022.0.35 =

Kiadás dátuma: 2022-05-31

- A termék ár történet figyelt időszakának javítása, hogy az aktuális akciós árhoz képest megtalálja az előző 30 nap legalacsonyabb aktív árát.
- HuCommerce beállítások oldalon a moduloknál kisebb igazítások, hogy helyesen jelöljék az új modulokat és az opcióikat.
- HuCommerce beállítások oldalon a modulok kaptak egy "Bővebben" linket a leírásnál.
- Leírás aktualizálása, kiegészítése.

= 2022.0.34 =

Kiadás dátuma: 2022-05-30

- Pro modulok feltételeinek a pontosítása.
- Termék ár történet modul értékeinek megőrzése akkor is, ha nem aktív az előfizetés.

= 2022.0.33 =

Kiadás dátuma: 2022-05-30

- Új opció hozzáadása a termékekhez, amivel a termék oldalon el lehet rejteni a legalacsonyabb ár szöveget.

= 2022.0.32 =

Kiadás dátuma: 2022-05-30

- CPS SDK frissítése a 8.9.0 verzióra.

= 2022.0.31 =

Kiadás dátuma: 2022-05-30

- Pénztár mezők kompatibilitásának a javítása az Oxygen sablonnal.

= 2022.0.30 =

Kiadás dátuma: 2022-05-30

- Új modul: Termék ár történet
- Új badge a modulokhoz: Beta
- Az Adószám megjelenítése modul megjelenítésének kisebb igazítása.
- Disclaimer szöveg hozzáadása több modulhoz.

= 2022.0.20 =

Kiadás dátuma: 2022-05-26

- CPS SDK frissítése a 8.8.0 verzióra.
- Feed cache beállítása 24 órára.
- A manuális API kérések kiegészítése a status lekéréssel.
- A licensz kezelő menüpont kiegészítése a Pro promo-val.
- API instance lecserélése a domainre. Így a későbbi újraaktiválás is egyszerűbb, mert weboldalhoz kötött.
- Aktiválás és deaktiválás folyamat egyszerűsítése két gombra.
- API szinkronizálás és API kezelés linkek hozzáadása.
- A régi felhasználók értesítés feltételének a javítása.
- A HuCommerce Pro promo banner törlése a beállítások oldalról.
- A HuCommerce főmenüpont is kapott címet. A szűrők pedig eltüntek.
- Hírlevél feliratkozás átadott paramétereinek a leegyszerűsítése.
- A HuCommerce beállítások teljes UI módosítása.

= 2022.0.19 =

Kiadás dátuma: 2022-05-25

- Felhasználói jogosultság ellenőrzésének a kikapcsolása a license.php fájlban.

= 2022.0.18 =

Kiadás dátuma: 2022-05-24

- API kulcs kezelés teljeskörű átdolgozása. Most minden feltétel esetén megfelelően frissül és a státusz lekérés is jobb és optimálisabb lett.

= 2022.0.17 =

Kiadás dátuma: 2022-05-24

- CPS SDK frissítése a 8.7.2 verzióra.

= 2022.0.16 =

Kiadás dátuma: 2022-05-22

- Admin értesítés - ami a régi felhasználóknak jelenik meg - feltételeinek és szövegének a módosítása.
- API státusz lekérés feltétel hozzáadása, hogy naponta csak egyszer fusson le.

= 2022.0.15 =

Kiadás dátuma: 2022-05-19

- Manuális API hívásnál az URL paraméter törlése a lekérés után, hogy az esetleges oldalfrissítés ne okozzon konfliktust.
- Az instancebackup és a licensekeybackup értékek feltételes mentése.
- Kompatibilitás ellenőrzése a WordPress 6.0 verzióval.

= 2022.0.14 =

Kiadás dátuma: 2022-05-18

- A product ID változó elnevezésének a módosítása.

= 2022.0.13 =

Kiadás dátuma: 2022-05-18

- Licensz kezelésnél a product_id is kapott mezőt, hogy lehessen más terméket is tesztelni.
- Fix product_id lecserélése dinamikusra.
- A licensz kezelő menüpont alatt a mezők placeholder-t kaptak.

= 2022.0.12 =

Kiadás dátuma: 2022-05-17

- Mezők formátumának ellenőrzése modul kiegészítése a mezőnkénti aktiválás lehetőségével.
- Mezők értékének ellenőrzése modul kiegészítése a mezőnkénti aktiválás lehetőségével.

= 2022.0.11 =

Kiadás dátuma: 2022-05-17

- Admin értesítésnél licensz állapottól függő szövegezés.
- A license.php fájlban a feltételes kód javítása.
- Katalógus szűrő ideiglenes elrejtése, amíg nem lesz több elem a katalógusban.
- A licensz kezelő menüpont alatt a licensz átírása API-ra.
- A licensz kezelő menüpont alatt debug módban több információ jelenik meg.
- A HuCommerce menüpont alatt az eddigi debug rész törlése, mert arra már nincs szükség.

= 2022.0.10 =

Kiadás dátuma: 2022-05-15

- Help Scout Beacon feltételek módosítása, hogy azoknak is megjelenjen, akiknek lejárt vagy nem érvényes a licensz kulcsuk.
- HuCommerce admin sidebar törlése, mert már nincs rá szükség az új UI miatt.
- Admin értesítések feltételeinek a finomításai.
- Admin értesítések átszövegezése.
- Admin widget kisebb módosítása.
- A licensz kezelés átrakása külön fájlba a /lib/start.php fájlból.
- Modul aktiválások feltételeinek a módosítása, hogy a licensz aktiválást és a régi HuCommerce felhasználók beállításait is megfelelően kezelje.
- A /lib/license.php fájl kitisztítása.
- Manuális lekérések hozzáadása a licensz kezeléshez. Ezeket most a külön aktiváláshoz és deaktiváláshoz használjuk egyelőre.
- Az API lekéréshez a wp_error feltétel beállítása, hogy hiba esetén se legyen végzetes hiba.
- API válasz mentése az adatbázisba későbbi felhasználási lehetőségekhez.
- Licensz állapotokhoz kapcsolódó értesítések hozzáadása.
- A kupon modul fájljának átnevezése.
- ÚJ - Kupon megjelenítése nagybetűkkel az admin és a látogatói felületen is.
- A kosár automatikus frissítése modul javítása, hogy több sablonnal is kompatibilis legyen.
- HuCommerce beállításoknál az Információk fül kibővítése pár hasznos adattal.
- A licensz kezelés fül teljes átszabása. Igazítás minden körülményhez és licensz állapothoz.
- A licensz kezelés fül alatt megjelennek a licensz adatok.
- A modulok mentésénél figyelmeztető szöveg hozzáadása a régi HuCommerce felhasználóknál.
- A modulok mentését követő validálások kiegészítése az összes mezőre.
- A modulok beállításait megörző logika kialakítása, hogy a régi felhasználóknál vagy lejárt licensz esetén is megmaradjanak a felhasználók beállításai.
- Új brandnewuser mező mentése, amivel ellenőrizhető, hogy mióta használja a felhasználó a HuCommerce bővítményt.
- Licensz kezeléshez kapcsolódó mezők validálása.
- Licensz kezelő menüpont ikonjának módosítása dinamikusra, hogy a licensz állapottól függően legyen zárva vagy nyitva a lakat.
- Licenszhez kapcsolódó értesítések beállítása a HuCommerce beállítások oldalon is.

= 2022.0.9 =

Kiadás dátuma: 2022-05-15

- CPS SDK frissítése a 8.7.1 verzióra.
- Kisebb módosítás a leírásban.
- Hírlevél feliratkozás hozzáadása a leíráshoz.
- Kompatibilitás ellenőrzése a WooCommerce 6.5 verzióval.

= 2022.0.8 =

Kiadás dátuma: 2022-05-06

- Licensz kezelés első verziója. Az API lekérés még nem működik, de a licensz kulcs elmenthető és az érvényesség alapján kezeli a modulokat, egyéb megjelenéseket, illetve a licensz kezelő űrlapot.
- A demo licensz kezelés kódja a license.php fájlba került. További módosítás szükséges a működéséhez.
- Új licensz menü.
- Licensz mező validálása hozzáadva.

= 2022.0.7 =

Kiadás dátuma: 2022-05-06

- Az uninstall folyamat kiegészítése további opciókkal.

= 2022.0.6 =

Kiadás dátuma: 2022-05-06

- CPS SDK frissítése a 8.4.0 verzióra.

= 2022.0.5 =

Kiadás dátuma: 2022-05-06

- HuCommerce beállítások menüpontjai külön fájlokba lettek kiszervezve.
- A social links sorrendjének a módosítása.
- HuCommerce beállítások oldalon a footer kiegészítése és kisebb módosítása.
- A settings-options.php fájl törlése, mert már nincs rá szükség.
- A settings-nav-modules.php fájlba átkerültek a globális változók a beállításokhoz.

= 2022.0.4 =

Kiadás dátuma: 2022-04-18

- HuCommerce Modulok menüpont kiszervezése külön fájlba. Idővel az összes menüpont külön fájlba kerül.
- HuCommerce Extensions törlése, egyelőre nem lesz.
- Az Ajánlatok, Katalógus és Hírek visszaállítása.
- A settings.php fájlba visszakerül a beállítások oldal szerkezete, de minden menüpont külön fájlba lesz kiszervezve. Egyelőre a settings-options.php fájlt megtartottam, hogy most látszódjanak a különbségek, de már nem kell, a következő verzióban ki lesz törölve.

= 2022.0.3 =

Kiadás dátuma: 2022-04-18

- Kisebb kód optimalizálás.
- HuCommerce Beállítások oldal teljeskörű átszabása, új struktúra, új elrendezés, új menüpontok.
- Az új beállítások oldal egyes elemeinek a kiszervezése külön fájlokba a könnyebb áttekinthetőség érdekében.
- Az új modulok beállításainak a megjelenítése az új UI-ban.

= 2022.0.2 =

Kiadás dátuma: 2022-04-17

- Törölve lett a HuCommerce beállítások oldalról a 2022.0.0 verzió búcsú üzenete.
- Kompatibilitás ellenőrzése a WooCommerce 6.4 verzióval.
- CPS SDK frissítése a 8.3.0 verzióra.

= 2022.0.1 =

Kiadás dátuma: 2022-04-09

- HuCommerce Pro link a bővítmények oldalon csak akkor jelenik meg, ha az ingyenes verzió van használatban.
- Help Scout Beacon visszaállítása. Van külön az ingyenes és a Pro verzióhoz is beállítva.
- A HuCommerce beállítások oldalon is be lettek állítva a megfelelő feltételek a Pro és az ingyenes verziókhoz.
- A tervezett funkciók visszakerültek az oldalsávba.
- A Welcome notice visszaállítása az első aktiváláskor.
- A Pro verzió ellenőrzésének az első verziója. Még nem végleges.
- Új modulok kezelésének a hozzáadása. Egyelőre kezelőfelület nélkül, mert azok már az új UI részei lesznek.
- Új modul: Kupon mező módosítások
- Új modul: Egyedi "Kosárba teszem" gombok
- Új modul: Szállítási módok elrejtése
- Új modul: "Egy termék egyszerre" korlátozás
- Új modul: Kosár oldal átugrása
- Mezők maszkolásának a továbbfejlesztése: az adószám mező már több formátumot is tud engedélyezni, ráadásul dinamikusan.
- Mezők validálásának a továbbfejlesztése: új adószám formátumok, dinamikus érvényesítéssel kitöltéskor.
- Új opció a Pénztár oldal módosításokhoz: már a További információk rész is kikapcsolható.
- Az ingyenes szállítás értesítőnél már beállítható, hogy hol jelenjen meg: termék listaoldalakon, kosár és pénztár oldalon.
- Globális adatok modul visszaállítása.
- Termék extra beállítások modul visszaállítása.
- SMTP modul visszaállítása.
- Adószám megjelenítése a Fiókom oldalon is.
- A kosár automatikus frissítésének a javítása az Astra sablonnál.
- Az uninstall műveletből is törölve lett a Freemius funkció.
- A readme.txt frissítése az új modulokkal és leírásokkal.

= 2022.0.0 =

Ez a HuCommerce legutolsó nem fizetős verziója, amiben az összes eddigi modul és funkció elérhető. Amennyiben újabb verzióra frissítesz, bizonyos funkciók nem lesznek elérhetők mentés után. Ez egy stabil és biztonságos verzió, ami további fejlesztéseket nem kap, maximum biztonsági frissítéseket. Minden további fejlesztés a HuCommerce Pro verziójában kap helyet, amire mindenképpen érdemes előfizetni, hiszen nem csak javításokat tartalmaz, hanem rengeteg új és hasznos funkcióval bővül szinte minden hónapban.

- A HuCommerce Pro vásárlás linkek cseréje ugrólinkekre, hogy mindig működjön.
- A HuCommerce beállítások oldalon a HuCommerce Pro banner szövegének módosítása.

**2022.0.0.4**

- A HuCommerce beállítások oldalon külön blokkba került a HuCommerce Pro banner.
- A HuCommerce beállítások oldalaon az ajánlat banner módosítása.
- Kisebb szöveg javítás.

**2022.0.0.3**

- Tervezett funkciók rész törlése.
- Kisebb megjelenítés igazítás az értékelésnél.

**2022.0.0.2**

- Admin értesítések szövegének a módosítása.
- Termék módosítások modul törlése.
- Kisebb szöveg módosítások.

**2022.0.0.1**

- Freemius törlése.
- HS Beacon kódok törlése.
- Üdvözlő üzenet törlése.
- A Globális adatok és az SMTP szolgáltatás modulok törlése.
- A Termék alcím opció törlése.
- Beállítások oldal letisztítása, hogy ne jelenjenek meg felesleges információk, opciók.
- Engedélyezett html tagok listájának a generálása.
- A változók átnevezése, hogy tükrözzék a modulok logikáját.

= 30.3.0 =

Kiadás dátuma: 2022-03-04

Kisebb hibajavítások a HuCommerce bővítményben és a Freemius frissítése a sebezhetőség miatt. A frissítés biztonságos és mindenképpen ajánlott.

JAVÍTÁSOK

- Ingyenes szállítás értesítés javítása, hogy üresen ne jelenjen meg.
- A Jogi megfelelés modul szöveg mezők megjelenítésének a javításai, hogy html tagokat is lehessen használni.
- A Fiók oldal javítása, hogy alapértelmezettként ne módosítson rajta semmit.

EGYÉB

- Linkek elhelyezése a Bővítmények menüpont alatt a HuCommerce bővítménynél.

= 30.2.0 =

Kiadás dátuma: 2021-10-25

További apró finomítások és javítások a kódban. A frissítés biztonságos és ajánlott!

EGYÉB

- Kódok optimalizálása.
- Kosár automatikus frissítése script optimalizálása.
- Kosár automatikus frissítése kompatibilis a Blocksy sablonnal.

JAVÍTÁSOK

- Kisebb javítás a beállításoknál.
- Mezők hozzáadása a WPML konfigurációhoz.

= 30.1.0 =

Kiadás dátuma: 2021-10-17

Ebben a verzióban semmi új funkció nincs, de sok fontos javítás történt. A frissítése biztonságos és mindenképpen ajánlott!

JAVÍTÁSOK

- Az Ingyenes szállítás értesítés modul javítása, hogy nettó árak megjelenítése esetén is jó összeget írjon ki.
- A Jogi megfelelés modulnál az egyik hibaüzenet módosítása, hogy igazodjon a többi üzenet formátumához.
- Adószám mező prioritásának a javítása, hogy az mindenképpen a Cégnév után jelenjen meg.
- A Beállítások oldalon az alapértelmezett állapotok javítása, hogy minden ki legyen kapcsolva.
- PRÉMIUM - SMTP Port beállítás javítása, hogy elmentse a választott értéket.
- PRÉMIUM - Az uninstall funkciók ideiglenes törlése, hogy a HuCommerce Pro verzióra váltásnál ne törölje a beállításokat.

EGYÉB

- Az új verziós értesítés végleges törlése.
- A Magyar formátum javítások modul is ki van kapcsolva alapértelmezettként.
- Kompatibilitás ellenőrzése a WordPress 5.8 verzióval.
- Kompatibilitás ellenőrzése a WooCommerce 5.8 verzióval.

= 30.0.1 =

Kiadás dátuma: 2021-06-02

JAVÍTÁSOK

- Pénztár oldali nonce ellenőrzés kisebb javítása.

EGYÉB

- Kompatibilitás ellenőrzése a WordPress 5.7 verzióval.

= 30.0.0 (29.6 → 30.0) =

Kiadás dátuma: 2021-05-31

Új korszak kezdődik a HuCommerce életében. Az ingyenes verzió mellett megjelent a bővítmény fizetős verziója is: a HuCommerce Pro további fantasztikus funkciókat ad az ingyenes HuCommerce Start bővítményhez:

- Globális adatok, amiket shortcode-dal lehet bárhol megjeleníteni.
- SMTP szolgáltatás beállítása.
- Termék extra beállítások - egyelőre a termékekhez lehet alcímet hozzáadni.

A mostani kiadás az ingyenes változathoz is hozott új funkciókat és fejlesztéseket:

- Lényegében a teljes kódbázis át lett írva a legszigorúbb biztonsági és kódolási szabványok alapján.
- Pénztár oldalon a céges megrendelés checkbox javítása, hogy kompatibilis legyen más sablonok egyedi stílusaival.
- A regiszrtációs checkbox validálása csak a front-end-en, hogy manuális regisztráció esetén ne zavarjon be.
- Az adószám már az admin felületen a megrendelésnél is módosítható.
- Új funkció hozzáadása: Kosárba teszem gomb a termék listaoldalakon.
- Új funkció: város mező validálása. Csak betűket és szóközt tartalmazhat.
- Új funkció: cím mező validálása. Mindenképpen kell tartalmaznia betűt, szóközt és számot.

EGYÉB

- Admin értesítések további rendberakása.
- Uninstall kiegészítése az új admin értesítésekkel.

- Boldog születésnapot Dorina! :)

= 29.15.3 =

Kiadás dátuma: 2021-05-28

EGYÉB

- Welcome admin értesítés módosítása.

= 29.15.2 =

Kiadás dátuma: 2021-05-26

EGYÉB

- Admin értesítések javítása, módosítása és kibővítése.
- Admin dasboard widget kiegészítése.

== Changelog ==

= 29.15.1 =

Kiadás dátuma: 2021-05-26

EGYÉB

- HuCommerce ikon hozzáadása a Freemius installációs folyamatához.

= 29.15.0 =

Kiadás dátuma: 2021-05-26

EGYÉB

- Freemius aktiválási logika bevezetése, hogy az ingyenes és fizetős bővítmények ne legyenek konfliktusban.

= 29.14.2 =

Kiadás dátuma: 2021-05-25

JAVÍTÁSOK

- Pénztár oldali megjelenítés javítása, hogy a html kódot ne kiírja, hanem feldolgozza.

= 29.14.1 =

Kiadás dátuma: 2021-05-25

JAVÍTÁSOK

- PRÉMIUM - Help Scout Beacon kód javítása, boztonság növelése.

= 29.14.0 =

Kiadás dátuma: 2021-05-25

Új funkcióként mostantól globális adatok is megadhatók, melyeket shortcode segítségével lehet megjeleníteni bárhol a weboldalon. Így ha változik egy adat, például a telefonszám, akkor azt elég egyszer módosítani és mindenhol az új adat jelenik meg.

ÚJDONSÁGOK

- PRÉMIUM - Új modul: Globális adatok.

= 29.13.6 =

Kiadás dátuma: 2021-05-24

EGYÉB

- SMTP beállítások külön blokkba került a könnyebb áttekinthetőség miatt.
- Az SMTP beállításoknál ikonokat kaptak a fiók azonosító mezők.
- Az SMTP beállításoknál a jelszó mező típusa módosítva lett, hogy a jelszó ne látszódjon.

= 29.13.5 =

Kiadás dátuma: 2021-05-24

JAVÍTÁSOK

- A prémium verzió konfigurálásának kisebb módosítása.

= 29.13.4 =

Kiadás dátuma: 2021-05-23

JAVÍTÁSOK

- A prémium kódok ellenőrzése, hogy aktív-e az előfizetés.
- Prémium beállítások engedélyezése csak aktív előfizetés mellett.
- SMTP beállítás validálásának javítása, hogy az alapértelmezett beállítás a megfelelő legyen.
- A prémium beállítások megőrzése akkor is, ha lejár a licensz vagy ingyenesre vált a felhasználó.

= 29.13.3 =

Kiadás dátuma: 2021-05-23

JAVÍTÁSOK

- Kisebb javítások a kódban a WPCS és WCCS szabvány alapján.

= 29.13.2 =

Kiadás dátuma: 2021-05-23

EGYÉB

- PRÉMIUM - A termék alcím függvény módosítása, hogy a Freemius megfelelően kezelje a prémium kódot.

= 29.13.1 =

Kiadás dátuma: 2021-05-23

EGYÉB

- Az érkező funkciókból ki lett véve egy, ami bekerül a prémium verzióba.

= 29.13.0 =

Kiadás dátuma: 2021-05-23

Mostantól külsős SMTP szolgáltatót is beállíthatsz a weboldaladhoz, hogy az értesítők kézbesítése még biztosabb legyen. Ez minden webáruház tulajdonosnak ajánlott funkció!

ÚJDONSÁGOK

- PRÉMIUM - Új modul: SMTP szolgáltatás beállítása.

= 29.12.0 =

Kiadás dátuma: 2021-05-22

Újabb validálások a Pénztár oldalon. Most már a város és cím mezőket is validálja.

ÚJDONSÁGOK

- Új funkció: város mező validálása. Csak betűket és szóközt tartalmazhat.
- Új funkció: cím mező validálása. Mindenképpen kell tartalmaznia betűt, szóközt és számot.

= 29.11.0 =

Kiadás dátuma: 2021-05-22

Új lehetőségként a "Kosárba teszem" gomb megjeleníthető a termék lista oldalakon (Üzlet, kategóriák, stb.).

ÚJDONSÁGOK

- Új funkció hozzáadása: Kosárba teszem gomb a termék listaoldalakon.

= 29.10.1 =

Kiadás dátuma: 2021-05-21

JAVÍTÁSOK

- Kisebb javítás a dashboard widget megjelenítésében.

= 29.10.0 =

Kiadás dátuma: 2021-05-20

Új lehetőségként a termékekhez alcím adható, ami megjelenik a termékek végoldalain és a gyüjtőoldalakon. Az alcím a címsor része, ami külön szemantikus kódként van beillesztve.

ÚJDONSÁGOK

- Új modul hozzáadása: Termék extra beállítások
- PRÉMIUM - Új funkció hozzáadása: Termék alcím

= 29.9.3 =

Kiadás dátuma: 2021-05-17

JAVÍTÁSOK

- Link javítása az egyik admin értesítésnél.

= 29.9.2 =

Kiadás dátuma: 2021-05-17

JAVÍTÁSOK

- Feed frissítés idejének a javítása.

= 29.9.1 =

Kiadás dátuma: 2021-05-17

JAVÍTÁSOK

- A bővítmény törlése esetén az adatbázis további tisztítása.

= 29.9.0 =

Kiadás dátuma: 2021-05-17

A HuCommerce bővítmény előkészítése a prémium verzió kiadásához.

EGYÉB

- Freemius SDK hozzáadása.
- Freemius script-ek hozzáadása, hogy admin felületen lehessen megvásárolni a prémium verziót.
- HuCommerce Pro banner-ek hozzáadása.
- HuCommerce feed-ek hozzáadása.
- Admin értesítések feltételeinek a módosítása, hogy csak a megadott helyeken jelenjenek meg.
- Új admin értesítés a legújabb verzióhoz.
- Új HuCommerce dashboard widget hozzáadása.

= 29.8.0 =

Kiadás dátuma: 2021-05-16

A bővítmény teljes kódbázisa újra lett írva a legszigorúbb szintaktikai és biztonsági szempontok alapján.

JAVÍTÁSOK

- A teljes kódbázis átírása a WPCS és WCCS kódolási szabványok alapján.

EGYÉB

- Az újnak jelölt moduloknál az "ÚJ" címke eltávolítása.

= 29.7.0 =

Kiadás dátuma: 2021-05-10

JAVÍTÁSOK

- Pénztár oldalon a céges megrendelés checkbox javítása, hogy kompatibilis legyen más sablonok egyedi stílusaival.
- A regiszrtációs checkbox validálása csak a front-end-en, hogy manuális regisztráció esetén ne zavarjon be.
- Az adószám már az admin felületen a megrendelésnél is módosítható.
- Admin CSS kisebb javítása.
- Admin CSS előkészítése az új menühöz.

EGYÉB

- Kompatibilitás ellenőrzése a WooCommerce 5.2 verzióval.

= 29.6.3 =

Kiadás dátuma: 2021-05-09

EGYÉB

- Új ignore szabály bevezetése a .gitignore fájlban, hogy könnyen lehessen fájlokat kivonni a verziókövetés alól.
- A new-settings.php fájl átnevezése, hogy az már megfeleljen a fenti szabálynak.

= 29.6.2 =

Kiadás dátuma: 2021-05-09

EGYÉB

- CPS SDK hozzáadása submodule-ként. Emiatt az SDK útvonala is változott.

= 29.6.1 =

Kiadás dátuma: 2021-05-09

EGYÉB

- WCCS ellenörző hozzáadása és a .gitignore módosítása ennek alapján.

= 29.6 =

Kiadás dátuma: 2020-11-27

JAVÍTÁSOK

- A Jogi megfelelés checkbox-ok kisebb módosítása, hogy ne legyen konfliktusban másik stílusokkal.

= 29.5 =

Kiadás dátuma: 2020-11-27

HTML struktúra kisebb módosítása a Pénztár oldalon. A frissítés biztonságos.

EGYÉB

- Jogi megfelelés checkbox-ok HTML struktúrájának a kisebb módosítása, hogy CSS-sel könnyebben lehessen formázni.

= 29.4 =

Kiadás dátuma: 2020-11-22

Admin üzenet javítása. Egyéb kisebb optimalizálás a kódban. A frissítés biztonságos.

JAVÍTÁSOK

- A kód kisebb javításai, hogy a kimenetek is biztonságosak legyenek.
- Kód szintaktika optimalizálása.
- CPS SDK módosítása, előkészítése a következő verzióhoz.
- PAnD kód javítása.

= 29.3 =

Kiadás dátuma: 2020-10-28

Kisebb karbantartás. A frissítés biztonságos.

EGYÉB

- CPS SDK frissítése a 7.2 verzióra.

JAVÍTÁSOK

- Elírás javítása.

= 29.2 =

Kiadás dátuma: 2020-10-26

A Pénztár modul aktív állapotának ellenőrzése, hogy ne legyen semmilyen beállítás mellett sem konfliktusban az Adószámmal. A frissítése biztonságos és mindenképpen ajánlott!

JAVÍTÁSOK

- A Pénztár modul aktív állapotának ellenőrzése az Adószám modulnál.

EGYÉB

- Kompatibilitás ellenőrzése a WooCommerce 4.6 verzióval.

= 29.1 =

Kiadás dátuma: 2020-10-01

Ez csak egy karbantartási frissítés. A Vásárlás folytatása gombok modul kódja lett módosítva. A frissítése biztonságos.

EGYÉB

- A Vásárlás folytatása gombok kódjának újraírása.

= 29.0 =

Kiadás dátuma: 2020-09-30

A Jogi megfeleléshez kapcsolódó checkbox mezők pozíciója választható. Most már a Megrendelés gomb fölött is megjeleníthetők ezek a mezők, sőt ez az alapértelmezett beállítás. További két extra checkbox mezőt is hozzá lehet adni, amennyiben további megerősítések is szükségesek az oldalnál.

ÚJDONSÁGOK

- Beállítható lett a Jogi megfeleléshez kapcsolódó checkbox mezők pozíciója.
- Két extra checkbox mező hozzáadási lehetősége a Jogi megfeleléshez.

JAVÍTÁSOK

- Szövegek kiírásának a validálásánál kisebb javítások.

EGYÉB

- Kisebb módosítások a szövegben.
- Bővítmény leírásának a kibővítése.

= 28.0 =

Kiadás dátuma: 2020-09-17

A HuCommerce mostantól bármilyen nyelvre fordítható. Minden szöveg angol a kódban, a magyar is egy fordítás, amit frissítés után külön kell letölteni. A Magyar formátum javítások kompatibilisek a legújabb és a régebbi verziókkal is.

ÚJDONSÁGOK

- WooCommerce verzió ellenőrzéshez külön függvény. Lehetőséget ad a különböző verziók támogatására funkcionális szinten.

MÓDOSÍTÁSOK

- A HuCommerce előkészítése a többnyelvűsítésre. Minden szöveg át lett írva angolra a kódban.
- Beállítások oldal kinézetének módosítása: saját márkás fejléc lecserélése a WooCommerce saját fejlécére.
- Az admin oldalon megjelenő üdvözlő üzenet megjelenésének a módosítása, illetve szöveg javítása.
- A Magyar formátum javítások modul átalakítása: kompatibilitás a régebbi verziókkal, illetve a továbbra is szükséges javítások megtartása. Ez azért kell, mert a WooCommerce 4.4 verzió sem javít ki minden esetet.

EGYÉB

- Kisebb kód optimalizálások.
- Kompatibilitás ellenőrzése a WooCommerce 4.5 verzióval.

= 27.2 =

Kiadás dátuma: 2020-08-16

Egy kis módosítás a Pénztár oldalon, hogy a Céges adatok checkbox több sablonnal is kompatibilis legyen.

JAVÍTÁSOK

- A Pénztár oldali Céges adatok checkbox-hoz új class-ek lettek hozzáadva.

EGYÉB

- Kompatibilitás ellenőrzése a WordPress 5.5 verzióval.
- Minimum WordPress verzió növelése 5.3 verzióra.

= 27.1 =

Kiadás dátuma: 2020-08-11

A bővítmény aktiválásának ellenőrzése már Multisite környezetben is jól működik. Ezen kívül kisebb javítás történt a telefonszám maszkolásnál, hogy kevesebb hibalehetőség legyen. A frissítése biztonságos és mindenképpen ajánlott.

JAVÍTÁSOK

- A "WooCommerce aktiválásának ellenőrzése" már Multisite kompatibilis.
- A telefonszám maszkolás javítása, hogy kevesebb elütéses hiba lehessen.

= 27.0 =

Kiadás dátuma: 2020-08-02

Nincs új funkció, de kód szinten nagy változtatások vannak ebben az új főverzióban. Mind biztonsági, mind logikai szinten történtek fejlesztések. Frissítése biztonságos.

MÓDOSÍTÁSOK

- Kód módosítások és optimalizálás a WordPress és WooCommerce kódolási ajánlatai alapján.
- Licensz módosítása GPLv2-ről GPLv3-ra.
- Direkt elérés megakadályozása az összes php fájlhoz a biztonság növelése érdekében.
- A "WooCommerce aktiválásának ellenőrzése" folyamat módosítása a WooCommerce ajánlása alapján.
- A kifejezetten HuCommerce specifikus kódok külön szintaxissal való jelölése és még jobb elkülönítése.
- Start folyamatok újragondolása.
- A Pénztár oldal címek átrendezése átkerült a hu-format-fix.php fájlba.
- A Jogi megfelelés modulhoz tartozó admin adatok megjelenítésének módosítása.

= 26.7 =

Kiadás dátuma: 2020-07-31

JAVÍTÁSOK

- A plusz-minusz gombok további igazítása.

= 26.6 =

Kiadás dátuma: 2020-07-31

JAVÍTÁSOK

- A plusz-minusz gombok kisebb igazítása.

= 26.5 =

Kiadás dátuma: 2020-07-31

A magyar formátum javításnál ki lett véve az autofocus, mert már nem is releváns és így nem okoz konfliktust más bővítményekkel. A frissítés biztonságos.

JAVÍTÁSOK

- A Pénztár oldalon a név mezőknél az autofocus törlése.

= 26.4 =

Kiadás dátuma: 2020-07-02

JAVÍTÁSOK

- Az Adatkezelési Tájékoztató ellenőrzésének a javítása.

= 26.3 =

Kiadás dátuma: 2020-07-02

Az irányítószámok listáját kicsit kibővítettem, így még több irányítószám esetén kitölti a várost. Frissítése biztonságos.

MÓDOSÍTÁSOK

- Irányítószám lista bővítése.
- Irányítószám mező automatikus kitöltésénél újabb JS event hozzáadása, hogy biztosabb legyen a kitöltés.
- Beállítások oldal kisebb módosítása.

= 26.2 =

Kiadás dátuma: 2020-06-29

MÓDOSÍTÁSOK

- "Mezők értékének ellenőrzése" opció már a telefonszám helyes formátumát is ellenőrzi.

= 26.1 =

Kiadás dátuma: 2020-06-29

JAVÍTÁSOK

- "Mezők értékének ellenőrzése" opció aktiválásának a javítása.

= 26.0 =

Kiadás dátuma: 2020-06-25

Kisebb, de fontos módosítások. A maszkolásnál kikapcsolható a placeholder használata, így nem zavar be azoknál a sablonoknál, ahol csak a mezők jelennek meg. Illetve az Adószám mezőhöz is aktiválható a placeholder megjelenítése, ami szintén azoknál a sablonoknál fontos, ahol csak a mezők jelennek meg.

ÚJDONSÁGOK

- Maszkolásnál külön állítható a placeholder megjelenítése.
- Adószám mezőnél bekapcsolható a placeholder használata.

MÓDOSÍTÁSOK

- Változtatások a leírásban.

= 25.0 =

Kiadás dátuma: 2020-06-25

A beállítások oldal menüpontja visszakerült a WooCommerce főmenüpont alá! Az admin felületen az egyes modulok és beállítások át lettek rendezve, hogy logikusabb legyen a megjelenés.

Új funkcióként megjelent a Pénztár mezők értékének a validálása is, egyelőre a karakterek számát figyeli. Így az Adószám, Irányítószám és Telefonszám mezők a maszkolás funkció egyidejű használatával már elég jól kizárják a téves adatok megadását.

További új funkció, hogy a Pénztár oldalon az Email mezőt be lehet állítani legfelülre, így kosárelhagyás esetén nagyobb valószínűséggel lesz meg a látogató email címe.

ÚJDONSÁGOK

- Pénztár oldali mezők validálási lehetősége.
- Email mező beállítása legfelülre a Pénztár oldalon.

MÓDOSÍTÁSOK

- A HuCommerce menüpont visszakerült a WooCommerce főmenüpont alá.
- CPS menüpont törlése.
- Kódok optimalizálása.
- Új mappa és fájlstruktúra a funkciók logikai elszeparálásához.

= 24.0 =

Kiadás dátuma: 2020-06-24

Mostantól beállítható a Pénztár oldalon a beviteli értékek validálása az Adószám, Irányítószám és Telefonszám mezőkhöz. FIGYELEM! Ez a beállítás esetleg konfliktusban lehet bizonyos számlázó és/vagy szállítási kiegészítőkkel. Mindenképpen tesztelni kell, hogy a formátumok megfelelnek-e a számlázó és/vagy szállíási kiegészítőknek! A bővítmény frissítése egyébként biztonságos, mivel az új funkció alapértelmezetten nem aktív.

Visszajelzéseket, hibajalentést a Facebook csoportban várom: HuCommerce támogató csoport

ÚJDONSÁGOK

- Pénztár oldali mezők maszkolási lehetősége.

EGYÉB

- CPS SDK frissítése a 7.0 verzióra.

JAVÍTÁSOK

- Kisebb szintű kódoptimalizálások.

= 23.11 =

Kiadás dátuma: 2020-06-24

További javítások a WooCommerce 4.1 verzióban megjelent hibákra, hogy minden beállítás mellett helyesen működjenek a Pénztár funkciók. Frissítése biztonságos és ajánlott.

JAVÍTÁSOK

- Adószám mező kezelésének javítása.

= 23.10 =

Kiadás dátuma: 2020-06-24

További javítások a WooCommerce 4.1 verzióban megjelent hibákra, hogy minden beállítás mellett helyesen működjenek a Pénztár funkciók. Frissítése biztonságos és ajánlott.

JAVÍTÁSOK

- Adószám mező kezelésének javítása.
- Kisebb logikai műveletek optimalizálása.

= 23.9 =

Kiadás dátuma: 2020-06-11

A Pénztár oldalon javítja a céges adatok mezőinek a kezelését, hogy bizonyos szituációkban ne törölje a már beírt adatokat. A frissítés ajánlott és biztonságos.

JAVÍTÁSOK

- A Pénztár oldalon a céges adatok kezelésének a javítása. (Köszönet: @dosabalint)

= 23.8 =

Kiadás dátuma: 2020-05-13

Kisebb javítás, ami a cím második sorát feltételesen jeleníti csak meg. Frissítése biztonságos.

JAVÍTÁSOK

- A Pénztár oldalon a második címsor feltételes módosítása.

= 23.7 =

Kiadás dátuma: 2020-05-09

A WooCommerce 4.1 verzióban kijött WooCommerce beállítások kezelésének a hibáját javítja ez a verzió. Frissítése biztonságos.

EGYÉB

- Kompatibilitás ellenőrzése a WooCommerce 4.1 verzióval.

JAVÍTÁSOK

- A Pénztár oldalon a WooCommerce beállítások alternatív ellenőrzése.

= 23.6 =

Kiadás dátuma: 2020-05-05

JAVÍTÁSOK

- Kisebb javítások a Pénztár oldal JavaScript kódjaiban.

= 23.5 =

Kiadás dátuma: 2020-05-04

JAVÍTÁSOK

- Kisebb javítások a Pénztár oldal JavaScript kódjaiban.

= 23.4 =

Kiadás dátuma: 2020-05-03

JAVÍTÁSOK

- Kisebb javítások a Pénztár oldal JavaScript kódjaiban.

= 23.3 =

Kiadás dátuma: 2020-04-27

Céges adatok ideiglenes mentése a feltételes megjelenítés esetén, ha mégis kell a vásárlás során. További javítások a kódban az optimálisabb script kezelések érdekében. A frissítés biztonságos.

MÓDOSÍTÁSOK

- Céges adatok ideiglenes mentése a Pénztár oldalon.

JAVÍTÁSOK

- Az automatikus város kitöltés script-jeinek meghívása a megfelelő feltételekkel.
- Az Adószám kezeléséhez szükséges script meghívása a jQuery után.
- Felesleges js fájlok törlése.

= 23.2 =

Kiadás dátuma: 2020-04-25

Apró javítás az inline script-ek kezelésével kapcsolatban. A frissítés biztonságos.

JAVÍTÁSOK

- Inline script-ek betöltése a jQuery után. (Köszönet: Dovalovszki Tamás - @dovi42)

EGYÉB

- CPS SDK frissítése a 6.0 verzióra.

= 23.1 =

Kiadás dátuma: 2020-04-13

Kisebb módosítás, ami javítja az előző verziók üres Cégnév és Adószám mezőinek a felülírásait. A frissítés biztonságos és ajánlott.

JAVÍTÁSOK

- Az üres Cégnév és Adószám mezők az előző verziókban a '- N/A -' értéket kapták alapértelmezettként. Ebben a verzióban ezeket törli a Pénztár oldalon, hogy az újabb megrendelésnél már ne zavarjon be.

= 23.0 =

Kiadás dátuma: 2020-04-13

Új funkció nincs a frissítéssel, de a Pénztár oldalon jelentős módosítások történtek, ami növeli a kompatibilitást más bővítményekkel, igazodik a WooCommerce és a HuCommerce különböző variációjú beállításaihoz és optimalizálja a kódokat, csökkenti a lekérések számát. Frissítése biztonságos.

MÓDOSÍTÁSOK

- Az Adószám mező kezelésének a teljeskörű újragondolása. Elvileg most már kompatibilis minden beállítási variációval és helyesen kezeli a feltételes kötelező jellegét.
- Az adószám mező kezeléséhez szükséges javascript kód inline beillesztése, amivel csökkent a lekérések száma, valamint a beállításoktól függően jelennek csak meg a kódok.
- A Pénztár oldal kódjainak jelentős optimalizálása a különböző beállításokkal való kompatibilitás érdekében.
- A Pénztár oldal kezeléséhez szükséges javascript kód inline beillesztése, amivel csökkent a lekérések száma, valamint a beállításoktól függően jelennek csak meg a kódok.

JAVÍTÁSOK

- További ellenőrzések a Pénztár mezők módosításainál, hogy kompatibilis legyen más bővítményekkel, amik szintén módosítják a Pénztár mezőket. (Köszönet: Viszt Péter - @passatgt)

= 22.1 =

Kiadás dátuma: 2020-03-30

Kicsi, de fontos javításokat tartalmazó frissítés. Mindenképpen ajánlott a frissítés, de minimum a WooCommerce 4.x főverzió esetén!

JAVÍTÁSOK

- Ország elrejtése esetén fontos javítás, hogy ne módosítsa a többi mezőt.
- Mezők sorrendjének a javítása, hogy minden ország esetén rendben legyen a megjelenés.

EGYÉB

- Kompatibilitás ellenőrzése a WordPress 5.4 főverzióval.
- CPS SDK frissítése az 5.10 verzióra.

= 22.0 =

Kiadás dátuma: 2020-03-12

FONTOS! Csak akkor frissíts erre a verzióra, ha a WooCommerce is frissítve lett legalább a 4.0 verzióra!

MÓDOSÍTÁSOK

- A quantity-input.php template fájl törölve lett, mivel a WooCommerce 4.0 verzióban új hook-ok lettek hozzáadva.
- A plusz/minusz gombok az új hook-okkal lettek hozzáadva. Csak a WooCommerce 4.0 verziótól működik!

EGYÉB

- Kód optimalizálás.
- Kompatibilitás ellenőrzése a WooCommerce 4.0 verzióval.
- A minimum PHP követelmény módosítása 7.0 verzióra.

= 21.6 =

Kiadás dátuma: 2020-03-09

Kisebb módosításokat és hibajavítást tartalmaz ez a verzió. A frissítése biztonságos és mindenkinek ajánlott.

MÓDOSÍTÁSOK

- Kódok átírása.

JAVÍTÁSOK

- Fordítás textdomain módosítása egy-két helyen.
- Beállítások globális változóinak a korai hívása, hogy betöltésnél már rendelkezésre álljanak a választások.

= 21.5 =

Kiadás dátuma: 2020-02-20

MÓDOSÍTÁSOK

- Pénztár oldalon a város automatikus kitöltése esetén a mező validálása.

= 21.4 =

Kiadás dátuma: 2020-02-19

Kisebb módosítás, hogy a számlázási és a szállítási adatoknál az irányítószám megadása esetén frissüljön a Pénztár.

MÓDOSÍTÁSOK

- Pénztár frissítése az irányítószám megadása után, az automatikus kitöltés esetén.

= 21.3 =

Kiadás dátuma: 2020-02-19

JAVÍTÁSOK

- Verziószám frissítés.

= 21.2 =

Kiadás dátuma: 2020-02-19

JAVÍTÁSOK

- A Céges számlázás mezőnél a "Nem kötelező" szöveg törlése.
- Felesleges JS kódok törlése.
- Nyelvi mappa törlése, mivel már elérhető a hivatalos forrásból.

= 21.1 =

Kiadás dátuma: 2020-02-19

JAVÍTÁSOK

- Az Adószám mező külön kötelezővé tétele már felesleges, mert alapból is kötelező.

= 21.0 =

Kiadás dátuma: 2020-02-18

Az előző verzióból kimaradt egy fontos fejlesztés, amit most bepótoltam. Ha nem feltételesen jelennek meg a céges számlázási mezők, akkor is csak a Cégnév látszik alapból és az Adószám csak akkor, ha kitölti a Cégnevet. Az Adószám innentől kezdve kötelező mező lett.

ÚJDONSÁGOK

- Adószám kötelező lett, illetve a Cégnév kitöltésétől függően jelenik meg alapból.

JAVÍTÁSOK

- Kisebb javítás a leírásban.

= 20.0 =

Kiadás dátuma: 2020-02-18

A legújabb főverzió igazán nagy és régóta várt újításokat tartalmaz. Mostantól megjeleníthető egy "Céges számlázás" checkbox, ami elrejti vagy megjeleníti a céges számlázási adatokat (Cégnév, Adószám). Illetve egy régi "hibát" is javítottam: az Ország mező elrejtése esetén a megrendelés során alapértelmezettként a Bolt beállított országa lesz az aktuális Ország. Így nem lesz konfliktusban a szállítási és fizetési modulokkal.

ÚJDONSÁGOK

- Céges számlázási adatok feltételes megjelenítése. Aktív állapotban a céges adatok kitöltése kötelező.
- Cégnév és Adószám mezők egymás mellé rendezése.

JAVÍTÁSOK

- Ha az "Ország mező elrejtése" opció aktív, akkor az ország mező csak rejtve van, nem törölve és alapértelmezettként megkapja a Bolt beállított országának az értékét.
- JS fájlok kiegészítése verziószámmal és a kód átmozgatása a footer-be.
- Kisebb kód igazítások.

= 19.1 =

Kiadás dátuma: 2020-02-15

Fontos javítások a fordítási lehetőségekkel kapcsolatban.

JAVÍTÁSOK

- A text domain módosítása a bővítmény slug-jára, mert csak azzal működik együtt a wp.org.
- Két modul javascript hivatkozásának a későbbi meghívása, hogy lehetőleg ne legyen konfliktusban más optimalizáló bővítményekkel.

EGYÉB

- Ideiglenesen a bővítmény fordítását mellékeltem, amíg a hivatalos forrásból nem lesz elérhető.

= 19.0 =

Kiadás dátuma: 2020-02-11

Az irányítószám automatikus kitöltése most már a szállítási címnél is működik. A bővítmény elő lett készítve a fordíthatóságra. Viszont emiatt mindent át kell majd írni angolra, aztán lefordítani magyarra. Ez biztos hosszabb folyamat lesz, de megéri. :)

ÚJDONSÁGOK

- A bővítmény fordíthatóságának előkészítése.

MÓDOSÍTÁSOK

- Autofill beállítása a szállítási címhez.

EGYÉB

- Kompatibilitás ellenőrzése a WordPress 5.3 főverzióval.
- Kompatibilitás ellenőrzése a WooCommerce 3.9 főverzióval.
- Új frissítési mód bevezetése, hogy automatikusan feltöltse a wp.org repo-ba.

= 18.7 =

Kiadás dátuma: 2019-11-03

Mostantól megjelenít egy figyelmeztető szöveget az Adószám megjelenítése opciónál, ha a Viszt Péter által írt Szamlazz.hu vagy Billingo bővítményeknél is be van kapcsolva az adószám mező. Ebben az esetben két adószám mező jelenik meg, így az egyik bővítménynél ki kell ezt az opciót kapcsolni.

EGYÉB

- Kompatibilitás javítása a Viszt Péter által írt Szamlazz.hu és Billingo bővítményekhez.

= 18.6 =

Kiadás dátuma: 2019-11-03

Ez a frissítés az irányítószám adatbázis frissítését tartalmazza. Így most már Budapest és a nagyvárosok is szerepelnek az adatbázisban.

JAVÍTÁSOK

- Irányítószámok frissítése.
- Apró javítás a leírásban, hogy az admin menüpontot helyesen írja le.

= 18.5 =

Kiadás dátuma: 2019-08-08

Ebben a kiadásban a HuCommerce menüpont átkerült a CPS Plugins főmenüpont alá, illetve néhány apró javítás és fejlesztés található. A frissítése biztonságos éles oldalakon is.

JAVÍTÁSOK

- A Plusz/minusz mennyiségi gombok új HTML kódot kaptak, hogy bizonyos bővítményekkel ne legyen konfliktusban a megjelenésük.
- A Product quantity inputs template frissítve lett a 3.6.0 verzióra.
- A Fiók oldalon javítva lett az új logika szerint a nevek megjelenítése, így ott is akkor cserélődik meg a keresztnév és vezetéknév sorrendje, ha magyar nyelven nézi a látogató a weboldalt.

EGYÉB

- CPS SDK frissítése az 5.3 verzióra.
- A HuCommerce menüpont átkerült a CPS Plugins menüpont alá.

= 18.4 =

Kiadás dátuma: 2019-07-25

Semmi funkcionális változtatás most, csak egy kis rendrakás. Kompatibilitás javítása a többi CherryPick Studios bővítménnyel.

JAVÍTÁSOK

- Az új CPS SDK kompatibilitás javítása.
- Admin header filterek csak a HuCommerce admin oldalán futnak le.

EGYÉB

- CPS SDK frissítése az 5.2 verzióra.
- Új assets mappa, hogy logikusabb legyen a mappa struktúra.

= 18.3 =

Kiadás dátuma: 2019-07-18

JAVÍTÁSOK

- WooCommerce bővítmény aktív állapotának az ellenőrzése most már minden esetben jól működik.

= 18.2 =

Kiadás dátuma: 2019-07-18

Betettem egy ellenőrzést, hogy a WooCommerce bővítmény aktív-e. Ha nem, akkor a kódok nem futnak le, csak egy figyelmeztetés jelenik meg az admin felületen.

JAVÍTÁSOK

- WooCommerce bővítmény ellenőrzése, hogy be van-e kapcsolva.

= 18.1 =

Kiadás dátuma: 2019-07-16

Ebben a verzióban az Avada sablonhoz lett egyedi CSS rendelve, hogy a Pénztár oldalon működjenek a magyar formátum javítások.

JAVÍTÁSOK

- CSS hozzáadása az Avada sablon megjelenítésének a javításához.

= 18.0 =

Kiadás dátuma: 2019-07-09

A mostani verzióban külön modul lett hozzáadva a Pénztár oldali módosításokhoz. Itt lehet vezérelni, hogy az Irányítószám és Város, valamint a Telefonszám és Email mezők egymás mellé kerüljenek, illetve az Ország és Rendelés jegyzetek mezőket ki is lehet kapcsolni.

ŰJDONSÁGOK

- Pénztár oldal modul.

EGYÉB

- CPS SDK frissítése a 3.0 verzióra.
- Admin oldal módosítások a CPS SDK 3.0 alapján.

= 17.0 =

Kiadás dátuma: 2019-07-01

Ez a frissítés javítja a WPML bővítmény kompatibilitást és gyorsabb is a betöltés WPML használata mellett. A magyar formátum javítások logikája is megváltozott, mert most bizonyos mezők akkor módosulnak, ha magyar nyelven nézi a látogató, nem pedig akkor, ha Magyarország a számlázási cím. A Divi és Storefront sablonok esetén kisebb fordítással kapcsolatos javításokat is tartalmaz. Admin felületen megjelenik egy üdvözlés, ami lezárás után nem jelenik meg újra.

JAVÍTÁSOK

- WPML kompatibilitás és gyorsítás.

MÓDOSÍTÁSOK

- Pénztár mezők feltételei módosultak a nyelvre a számlázási cím helyett.

ÚJDONSÁGOK

- Admin üdvözlés a bővítmény első bekapcsolása után vagy a mostani frissítés után.
- A Divi és Storefront sablonok esetén javítja a kosár ikon melletti szöveget, hogy "elem" helyett "termék" jelenjen meg a megfelelő többesszám kezeléssel.

= 16.0 =

- FRISSÍTÉS - 2019-06-11
- ÚJ - HuCommerce hírlevél feliratkozási lehetőség a HuCommerce admin oldaláról.

= 15.1 =

- FRISSÍTÉS - 2019-05-07
- JAVÍTÁS - Új opció validálása.

= 15.0 =

- FRISSÍTÉS - 2019-05-07
- ÚJ - Automatikus frissítés a Kosár oldalon. (Köszönet: Nagy Gábor - @xnagyg)
- JAVÍTÁS - Irányítószámok kiegészítése a budapesti kerületekkel. (Köszönet: Nagy Gábor - @xnagyg)

= 14.4 =

- FRISSÍTÉS - 2019-05-02
- JAVÍTÁS - Változó alapértelmezett értékének beállítása. (Köszönet: Szépe Viktor - @szepeviktor)

= 14.3 =

- FRISSÍTÉS - 2019-04-22
- MÓDOSÍTÁS - Kisebb változtatások a leírásban.
- Tesztelve a WooCommerce 3.6 verziójával.

= 14.2 =

- FRISSÍTÉS - 2019-04-15
- JAVÍTÁS - Ingyenes szállítás értesítés link módosítása, hogy mindig az Üzlet oldalra mutasson.

= 14.1 =

- FRISSÍTÉS - 2019-04-08
- JAVÍTÁS - Admin stílusok betöltése csak a szükséges oldalakon.

= 14.0 =

- FRISSÍTÉS - 2019-04-07
- ÚJ - CPS SDK az egységes kinézethez és funkcionalitáshoz a többi bővítmény esetén is.
- CPS SDK 1.0
- MÓDOSÍTÁS - Leírásokban és a bővítmény jellemzőinél CherryPick Solutions megemlítése.
- MÓDOSÍTÁS - Prémium verzió kezelésének előkészítése.

= 13.0 =

- FRISSÍTÉS - 2019-04-02
- ÚJ - Adatkezelési tájékoztató elfogadásának rögzítése a regisztrációs folyamatban.
- ÚJ - IP cím opcionális rögzítése a regisztrációs folyamatban.
- ÚJ - Regisztrációs adatok megjelenítése mind az admin felületen, mind a fiókadatok oldalon.

= 12.0 =

- FRISSÍTÉS - 2019-03-11
- ÚJ - Adatkezelési Tájékoztató kötelező elfogadtatása a regisztrációs űrlapnál.
- FIGYELEM! Ha a Jogi megfelelés opció aktív, akkor a regisztrációs űrlapnál automatikusan megjelenik az Adatkezelési Tájékoztató elfogadtatása pipa. Kikapcsolásához törölni kell az alapértelmezett szöveget a mezőben.

= 11.3 =

- FRISSÍTÉS - 2019-03-06
- JAVÍTÁS - Kosár oldalon a becslés címének javítása az Adószám megjelenítése esetén.

= 11.2 =

- FRISSÍTÉS - 2019-02-28
- JAVÍTÁS - Admin felület nyelvi beállítása.

= 11.1 =

- FRISSÍTÉS - 2019-02-27
- JAVÍTÁS - WPML ellenőrzése csak a front-end felületen.

= 11.0 =

- FRISSÍTÉS - 2019-02-17
- ÚJ - Facebook támogatói csoport jelentkezési lehetőség az admin felületről.
- ÚJ - Jogi szövegek elhelyezése a Pénztár oldalon a Megrendelés gomb fölött és/vagy alatt közvetlenül.
- MÓDOSÍTÁS - Kisebb változtatások az admin felület megjelenésén.

= 10.1 =

- FRISSÍTÉS - 2019-02-17
- JAVÍTÁS - WPML bővítmény ellenőrzése, hogy ne generáljon végzetes hibát.

= 10.0 =

- FRISSÍTÉS - 2019-02-10
- ÚJ - Város automatikus kitöltése az irányítószám alapján.
- JAVÍTÁS - Kisebb javítások az admin felület megjelenítésén.

= 9.0 =

- FRISSÍTÉS - 2019-02-09
- ÚJ - Adószám bekérése vásárlásnál
- ÚJ - Jogi megfelelés (GDPR, ePrivacy, stb.)
- MÓDOSÍTÁS - A magyar név sorrend mostantól nem akkor érvényes, ha a cím magyar, hanem minden esetben, ha magyar nyelvű a webáruház.
- JAVÍTÁS - A magyar név sorrend kompatibilis a WPML bővítménnyel, így többnyelvű webáruháznál igazodik a választott nyelvhez.
- MÓDOSÍTÁS - Kisebb változtatás az admin megjelenésén.

= 8.0 =

- FRISSÍTÉS - 2019-02-06
- ÚJ - Ingyenes szállítás értesítés.
- JAVÍTÁS - Mező értékek validálásának a javítása.

= 7.1 =

- FRISSÍTÉS - 2019-02-06
- JAVÍTÁS - Vásárlás folytatása gomb class meghatározásainak módosítása a könnyebb formázás érdekében.

= 7.0 =

- FRISSÍTÉS - 2019-02-06
- ÚJ - Belépés és regisztráció utáni átirányítás.
- ÚJ - WPML és Polylang kompatibilitás a szöveges mezőkhöz.

= 6.0 =

- FRISSÍTÉS - 2019-02-06
- ÚJ - Vásárlás folytatása gomb megjelenítése a Kosár és/vagy a Pénztár oldalakon. A gomb pozíciója is beállítható.

= 5.2 =

- FRISSÍTÉS - 2019-01-12
- MÓDOSÍTÁS- Admin UIkit frissítése.

= 5.1 =

- FRISSÍTÉS - 2019-01-12
- JAVÍTÁS - Divi sablon egyedi formázások a plusz/minusz gombokhoz.
- JAVÍTÁS - Kisebb kód optimalizálás a plusz/minusz gombok CSS formázásánál.

= 5.0 =

- FRISSÍTÉS - 2019-01-11
- ÚJ - Mennyiségi gombok a termékekhez.
- ÚJ - A bővítmény nevének módosítása: HuCommerce | Magyar WooCommerce kiegészítések
- ÚJ - Admin beállítások a bővítményhez. Minden modul ki- bekapcsolható.
- ÚJ - Moduláris felépítés első lépései, felkészítés további hasznos funkciók optimális implementálásához.
- JAVÍTÁS - A WooCommerce myaccount/form-edit-account.php sablon frissítése a 3.5.0 verzióra.
- MÓDOSÍTÁS - A megye mező elrejtésének a kikapcsolása már nem működik az eddigi globális változó megadásával, ezt is az admin felületen lehet beállítani.
- MÓDOSÍTÁS - Kisebb javítások a leírásban.

= 4.5 =

- Kis javítás a leírásban.
- Tesztelve a WooCommerce 3.5 verziójával.
- Tesztelve a WordPress 5.0 verziójával.

= 4.4 =

- FIX - A form-edit-account template fájl frissítése a legújabb verzióra.

= 4.3 =

- Autofocus beállítása a vezetéknévre. (Köszönet: @nagygabor) #1

= 4.2 =

- A keresztnév mező autofocus törlése. #1

= 4.1 =

- A szerződési feltételek szövegének a felülírása már nem kell, mert az új verzióban helyesen jelenik meg.
- Tesztelve a WooCommerce 3.3 verziójával.

= 4.0 =

- A bővítmény most már felül tudja írni a WooCommerce template fájlokat.
- Fiók adatoknál is jól jelenik meg a nevek sorrendje, ha a számlázási cím Magyarország.
- Szállítási és számlázási címeknél is helyes a név megjelenítése magyarországi cím esetén.
- Kisebb kód javítások.

= 3.0 =

- A WooCommerce 3.2 verziónál a szerződési feltételek elfogadása szöveg fordítása nem jelenik meg, erre van egy javítás, amíg nem lesz elérhető a hivatalos fordítás javított változata.

= 2.0 =

- Az Irányítószám és Város mezők nagy monitoron egymás mellé kerültek és az Ország mező alatt jelennek meg közvetlenül, hogy logikusabb legyen a megjelenési sorrend.
- A Megye mező elrejtését most már WooCommerce filter kezeli. (Köszönet: Viszt Péter - @passatgt)
- További címkék a bővítményhez a jobb keresési találatok miatt.

= 1.1 =

- Kód elírás javítása.
- Leírások hozzáadása a függvényekhez.

= 1.0.0 =

- Első kiadás.
