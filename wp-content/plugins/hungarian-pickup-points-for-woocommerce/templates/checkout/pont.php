<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

//Convert shippings costs so it works as a data attribute
$shipping_costs_json = wp_json_encode( $shipping_costs );
$shipping_costs_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $shipping_costs_json ) : _wp_specialchars( $shipping_costs_json, ENT_QUOTES, 'UTF-8', true );

?>
<?php if($is_vp_pont_selected): ?>
<tr class="vp-woo-pont-review-order">
	<th><?php echo esc_html_x('Pickup point', 'frontend', 'vp-woo-pont'); ?></th>
	<td data-title="<?php echo esc_attr_x('Pickup point', 'frontend', 'vp-woo-pont'); ?>">
		<?php if(!$selected_vp_pont || empty($shipping_cost)): ?>
			<a href="#" id="vp-woo-pont-show-map" data-shipping-costs="<?php echo $shipping_costs_attr; ?>"><?php esc_html_e('Select a pick-up point', 'vp-woo-pont'); ?></a>
		<?php else: ?>
			<div class="vp-woo-pont-review-order-selected">
				<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($selected_vp_pont['provider']); ?>"></i>
				<div class="vp-woo-pont-review-order-selected-info">
					<strong><?php echo esc_html($selected_vp_pont['name']); ?>:</strong>
					<?php if ( WC()->cart->display_prices_including_tax() ): ?>
						<strong><?php echo $shipping_cost['formatted_gross']; ?></strong><br>
					<?php else: ?>
						<strong><?php echo $shipping_cost['formatted_net']; ?></strong><br>
					<?php endif; ?>
					<span><?php echo esc_html($selected_vp_pont['addr']); ?>, <?php echo esc_html($selected_vp_pont['zip']); ?> <?php echo esc_html($selected_vp_pont['city']); ?></span> - <a href="#" id="vp-woo-pont-show-map" data-shipping-costs="<?php echo $shipping_costs_attr; ?>"><?php esc_html_e('Modify', 'vp-woo-pont'); ?></a>
				</div>
			</div>
		<?php endif; ?>
	</td>
</tr>
<?php endif; ?>
