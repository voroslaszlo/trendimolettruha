<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="vp-woo-pont-modal-bg"></div>
<div class="vp-woo-pont-modal">
	<div class="vp-woo-pont-modal-sidebar">
		<div class="vp-woo-pont-modal-sidebar-search">
			<span class="vp-woo-pont-modal-sidebar-search-icon"></span>
			<input class="vp-woo-pont-modal-sidebar-search-field search" type="text" placeholder="<?php esc_attr_e('Search for an address', 'vp-woo-pont'); ?>" inputmode="search">
			<div class="vp-woo-pont-modal-sidebar-search-field-focus"></div>
			<a href="#" class="vp-woo-pont-modal-sidebar-search-clear"></a>
		</div>
		<ul class="vp-woo-pont-modal-sidebar-filters"></ul>
		<div style="display:none">
			<li class="vp-woo-pont-modal-sidebar-result" id="vp-woo-pont-modal-list-item-sample" data-provider="" data-id="">
				<div class="vp-woo-pont-modal-sidebar-result-info">
					<i class="vp-woo-pont-modal-sidebar-result-info-icon icon"></i>
					<div class="vp-woo-pont-modal-sidebar-result-info-text">
						<strong class="name"></strong>
						<span class="addr"></span>
						<em class="cost"></em>
					</div>
				</div>
				<div class="vp-woo-pont-modal-sidebar-result-info-comment comment"></div>
				<a href="#" class="vp-woo-pont-modal-sidebar-result-select"><?php esc_attr_e('Set as my pick-up point', 'vp-woo-pont'); ?></a>
			</li>
			<li class="vp-woo-pont-modal-sidebar-no-result" id="vp-woo-pont-modal-no-result-sample">
				<p><?php esc_html_e('Sadly there are no results for this keyword. Please try to look for another place, or select a nearby pickup point on the map.', 'vp-woo-pont'); ?></p>
			</li>
		</div>
		<ul class="vp-woo-pont-modal-sidebar-results"></ul>

	</div>
	<div class="vp-woo-pont-modal-map">
		<div id="vp-woo-pont-modal-map"></div>
		<a href="#" class="vp-woo-pont-modal-map-close"><span></span></a>
	</div>
</div>
