=== Csomagpontok és szállítási címkék WooCommerce-hez ===
Contributors: passatgt
Tags: postapont, foxpost, packeta, csomagkuldo, woocommerce, magyar, dpd, gls, pick-pack-pont, express-one, sameday, easybox
Requires at least: 5.5
Tested up to: 6.0.1
Stable tag: 1.8.8.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Térképes csomagpont választó és címkenyomtató WooCommerce webáruházakhoz. Foxpost, Postapont, Packeta(Csomagküldő), GLS, DPD, Pick Pack Pont, Express One, DPD, Sameday(easybox)

== Description ==

Ezzel a bővítménnyel megjeleníthetsz egy térképes felületet a pénztár oldalon, ahol a vásárló a kiválasztott szolgáltatók átvételi helyei közül választhat. A beállításokban minden szolgáltatóhoz külön árazást állíthatsz be és a megjelenő felület színét is módosíthatód a Testreszabás menüpontban.

> **PRO verzió**
> A bővítménynek elérhető a PRO verziója 30 Euróért, amelyet itt vásárolhatsz meg: [https://visztpeter.me](https://visztpeter.me/woocommerce-csomagpont-integracio//)
> A licensz kulcs egy weboldalon aktiválható, 1 évig érvényes és természetesen emailes support is jár hozzá beállításhoz, testreszabáshoz, konfiguráláshoz.
> A vásárlással támogathatod a fejlesztést akkor is, ha esetleg a PRO verzióban elérhető funkciókra nincs szükséged.

= Funkciók =

* Postapont, Foxpost, Packeta(Csomagküldő), GLS, Express One, DPD és Pick Pack Pont(Sprinter), DPD és Sameday(easybox) választó egy térképen
* A PRO verzióban lehetőség van Foxpost, Packeta, GLS, DPD és Posta(MPL) címkét generálni, nyomtatni, házhozszállításra is, nem csak csomagpontra
* A csomagpont listát minden nap automatikusan szinkronizálja, így mindig az aktuálisat mutatja bővítmény frissítése nélkül
* Jól kinéző, gyors, egyszerű térképes felület(az Alza oldalán lévőre hasonlít)
* Irányítószám(számlázási adatoknál) alapján automatikusan belenagyít a megfelelő megyére, nem kell helymeghatározást engedélyezni
* Árak beállításai feltételek szerint(termék kategória, súly, kosár végösszeg, térfogat)
* A kiválasztott csomagpontot szállítási címként tárolja el, így minden WooCommerce levélben, profilban, admin felületen automatikusan megjelenik
* A rendelésből eltávolítható a kiválasztott csomagpont(ha másik szállítási módot szeretne a vásárló), vagy lecserélhető másik pontra is
* Lehet saját pontokat hozzáadni, egyedi ikonnal
* A Megjelenés / Testreszabás / WooCommerce menüpontban módosíthatod a színeket és egyéb megjelenítési beállításokat
* Mobilon is egyszerűen és gyorsan működik
* Rendeléskezelőben szűrhetők a rendelések csomagpont szolgáltató szerint
* Számlázz.hu, Woo Billingo Plus, WooCommerce Shipment Tracking és Yith WooCommerce Order Tracking kompatibilitás(PRO verzió)

== Installation ==

1. Töltsd le a bővítményt
2. Wordpress-ben bővítmények / új hozzáadása menüben fel kell tölteni
3. WooCommerce / Beállítások / Szállítás / Csomagpont menüben nézd át a beállításokat
3. Megjelenés / Testreszabás / WooCommerce / Csomagpont menüben beállíthatod a színeket
4. Ha minden jól megy, működik

== Screenshots ==

1. Térképes felület
2. Beállítások oldal

== Changelog ==

1.8.8.1
- Sameday csoportos címkenyomtatásnál 4 A6-os címkét 1 lapra rak

1.8.8
- Csomag adatok külön oszlopban a rendelések tálbázatban(mit lássunk menüben tudod bekapcsolni)
- GLS-nél el lehet rejteni a térképen a csomagautomatákat
- Állítható címkeméret Sameday esetében is
- Ha csak csomagpontos szállítás van, a pont kiválasztása látszik a kosár oldalon is
- Kompatibilitás megjelölése legújabb WC és WP verzióval

1.8.7.1
- Bugfix előző frissítéshezs

1.8.7
- GLS extra szolgáltatások javítás
- Packeta lengyel csomagpontok(InPost)
- A vp_woo_pont_frontend_params filterrel módosítható a térkép alapértelmezett pozíciója
- Manuális címkegeneráláskor GLS eseténben megadható egyedi felvételi nap és címkeszám
- Számlázz.hu / Woo Billingo Plus kompatibilitás javítás

1.8.6
- Webshippy javítás
- Javítás manuális címkegeneráláshoz
- Automatizálás javítás
- Beállítások oldal PHP Warning hibajavítás
- GLS extra szolgáltatások egyértelműsítés
- Varázslóban a PRO verziós link új ablakban nyílik meg
- iLogistic kompatibilitás
- WordPress 6.0 kompatibilitás
- WooCommerce 6.5.1 kompat

1.8.5
- Mobilos hibajavítások térképen

1.8.4
- Térkép működés és design hibajavítások elsősorban mobilon
- E-mail követési szám beillesztés javítás
- Posták kereshetők rendes irányítószám alapján is
- Packeta-nál el lehet rejteni a Z-Boxokat a térképről
- Sameday hibajavítások

1.8.3
- Posta csomagpont irányítószám javítás

1.8.2
- MPL címkenyomtatás hibajavítás

1.8.1
- Webshippy kompatibilitás javítása csomagpontos rendelések esetén
- Hibajavítás, ha a kosárban csak virtuális termék van és nem kell szállítás
- Kompatibilitás javítása Efor témával(és feltehetően egyéb Bootstrap alapú témákkal)
- Ha nincs választható csomagpont(pl egyik árazásnál beállított feltétel sem passzol), akkor nem látszódik a csomagpontos szállítási mód

1.8
- Csomagkövetés megjelenítése: a címkegenerálás után óránként ellenőrzi a csomag állapotát és eltárolja a rendelés adataiban
- Automatizálás csomag állapota alapján: beállítható, hogy módosítson rendelés státuszt, ha a csomag állapota megváltozott(például ki lett szállítva)
- A követési szám megjeleníthető a vásárlói fiók oldalon a rendeléseknél is
- Webshippy kompatibilitás javítása
- Címkegenerálás és csomagkövetés beállítások szétbontása
- Sameday import hibajavítás
- Hibajavítás csoportos műveleteknél
- A térképen ékezet nélkül is lehet keresni városnévre
- Árazásnál termékkategória feltétel javítva
- DPD adatküldés gomb elrejtése, ha nincs pro verzió
- A rendelés előnézet ablakban is látszik a követési szám és a címke
- rendelés státusz figyelés bekapcsolása opció

1.7.3
- A több címke 1 lapra nyomtatása működik DPD-val, Packeta-val és Foxpost-al is.

1.7.2
- GLS +1 üres lap bug javítása címkenyomtatáskor
- https bug javítása a csomagpontos json fájlok betöltésénél
- vp_woo_pont_prepare_label_data filter
- Címkén szállítási név használata, ha külön meg lett adva
- GLS telefonszám javítás
- print.js fájl frissítése

1.7.1
- GLS házhozszállításnál a cégnevet is feltünteti zárójelben a név után
- Csoportos címkegenerálás után egyből látszik a nyomtatás gomb
- Kompatibilitás megjelölése legújabb WC és WP verziókkal

1.7
- Sameday(easybox) csomagpont választó és címkenyomtatás csomagpontra(beta!)
- PRO verzióban lehet automata rendelés státusz módosítást beállítani címkegenerálás után
- GLS-nél az extra szolgáltatások külön beállíthatók házhozszállításnál és csomagpontra szállításnál
- GLS nyomtatásnál 1 lapra rak több címkét a beállításoknak megfelelően(ez majd a többi szolgáltatónál is menni fog később)
- DPD napi zárás gomb
- MPL szállítólevél számát kiírja rendelés oldalon is
- MPL szállítólevél készítés hibakezelés javítás
- Házhozszállítás konfigurálásnál csak azok a szolgáltatók látszódnak, amelyek konfigurálva is vannak címkenyomtatáshoz
- A csomagpont adatbázis frissítés csak azoknál a szolgáltatóknál fut le automatán, amelyek be vannak kapcsolva
- A WooCommerce / MPL menüpont csak akkor látszik, ha ténylegesen használva van az MPL
- Ha negatív érték van megadva szállítási költségnek, akkor elrejti a szolgáltatót

1.6.3
- Webshippy kompatibilitás
- Walkthrough bugfix

1.6.2
- GLS külföldre szállítás hibajavítások
- Beállítás varázsló új telepítésnél
- Új saját fejlesztésú licensz kulcs aktiválós rendszer

1.6.1
- Packeta pénznem és cím javítások
- MPL szállítólevélkezelőben lehet törölni a lezárásra váró csomagokat, ha mégsem kerülnek beküldésre

1.6
- GLS-nél lehet feladó országot választani
- Beállítások törlése opció
- Packeta beállításoknál az egyedi szállítási módok neve látszik sima Átalány helyett
- Címke törléskor megfelelő módon eltárolja a törölt csomagazonosítót(rendelés jegyzetekben látszik)
- Packeta-nál a csomag súlyát kg-ra konvertálja, ha esetleg grammban van megadva WooCommerce-ben
- Packeta import hibajavítás
- MPL szállítólevélkezelőben nem látszódnak azok a rendelések, amiknél törölve lett a címke
- MPL szállítólevél generálás / mentés hibajavítás
- A rendeléskezelőben MPL-es csomagoknál látszik, hogy lett e hozzá szállítólevél generálva vagy sem

1.5.9
- MPL szállítólevél kezelőben a lezárásra váró csomagok nincsenek limitálva max 20-ra
- MPL esetében a csomag értékét átadja akkor is, ha nincs utánvét
- Szintén MPL, az elsődleges és másodlagos cím külön van megadva most
- Hibajavítás akkor, ha csak egy szállítási mód van

1.5.8
- Külön MPL szállítólevél / jegyzék zárás funkció és menüpont
- Térképes választó a kosár oldalon is látszódjon opció javítása
- Packeta beállítások hibajavítása
- Kompatibilitás megjelölése WC 6.2-vel

1.5.7
- A címkebeállításokban kiválasztható, hogy melyik értéket használja referenciaszámként a címkén: rendelésszám, rendelés azonosító vagy számla(számlázz.hu és woo billingo plus) sorszáma
- Kuponnal kapcsolatos kalkuláció javítása
- Fordítási hiányosságok javítása

1.5.6
- Hibajavítás MPL címkegeneráláshoz
- Foxpost ügyfél megjegyzés limitálva 50 karakterre

1.5.5
- PRO verzió hibajavítás

1.5.4
- Tömeges címkegenerálás hibajavítás
- Szállítási cím átírás hibajavítása
- Beállítások oldal hibajavítás

1.5.3
- Házhozszállítás esetén nem írja át a vevő szállítási nevét a szolgáltatóra(csak csomagpontosnál kell így)

1.5.2
- Automata címkekészítés hibajavítás
- Rendelés oldal metabox javítás

1.5.1
- Rendelés végösszeg megfelelő módosítása, ha fizetési módhoz kötve van feltételes ár beállítva

1.5
- DPD csomagpontok megjelenítése a térképen
- Posta(MPL) címkenyomtatás
- DPD címkenyomtatás
- Packeta, Foxpost, GLS Posta(MPL) és DPD-vel lehet házhozszállításos címkét is generálni, nem csak csomagpontosat
- Új Foxpost API használata a címkenyomtatáshoz(azonnal megvan a címke, nem kell a webhook-ra várni) - ha korábban használtad, akkor frissíteni kell az API kulcsokat a beállításokban
- Packeta betölti a külföldi csomagpontokat is
- Beállíthatod, hogy melyik szolgáltatóra legyen érvényes az ingyenes szállítás kupon
- Csoportos számlakészítéskor jobb felül látszódik, ha még folyamatban van a generálás
- Ikonok frissítve, hogy ne legyen levágva az alja/teteje(pl Foxpost)
- Árkalkuláció adózásos hibajavítás
- Hibajavítás akkor, ha csak 1 szolgáltató volt használva
- Javítva, ha 0 ft(vagy üres érték) volt az alapértelmezett ár
- A térkép z-indexe meg lett emelve, így például az Elementor és a Crocoblock sticky menüje nem takarja el
- vp_woo_pont_shipping_cost és vp_woo_pont_provider_costs filter egyedi árazás beállításához(fejlesztőknek)
- Kompatibilitás javítása Flatsome témával
- Beállításokban az árak konfigurálásánál csak azok a szolgáltatók látszanak, amelyek engedélyezve lettek, hogy ne legyen keveredés
- Hibajavítás a pont listák importálásának időzítésével kapcsolatban

1.4.2
- Foxpost címkegenerálás hibajavítások
- Packeta import optimalizálás
- Csomagpont lista importálás hibanaplózása
- Termék kategória szerinti árazás javítása

1.4.1
- Hiba és fejlesztői mód naplózás a címkegeneráláskor
- Foxpost Webhook URL hibajavítás
- PHP warning javítása

1.4
- Foxpost és Packeta utánvét kezelés
- GLS címkenyomtatás
- Számlán a szállítási tétel leírásában benne van a szolgáltató neve és az átvételi hely neve
- CSS javítás Electro témához

1.3.3
* GLS csomaglista import hibajavítás

1.3.2
* Packeta csomag törlés funkció
* Áraknál a fizetési mód feltétel is működik(és ilyenkor frissíti a pénztár oldalt fizetési mód cserekor, hogy pontos legyen a szállítás ára)
* Divi téma kompatibilitás(nem takarja ki a fejléc)
* Pici javítások a beállítás varázslóban
* Apró javítások az adminban a rendelés oldalon lévő átvételi hely dobozban
* Fordítási hibák javítása

= 1.3.1 =
* Hibajavítás a beállítás varázslóhoz

= 1.3 =
* PRO verzió elérhető, benne a csomagpontos Foxpost és Packeta címkenyomtatás
* Beállítás varázsló - telepítéskor pár kérdéssel végigvezet a konfiguráláson
* Egyes átvételi helyek adatai módosíthatók(például ha rosszak a koordináták), vagy akár elrejthetők a térképről
* A manuálisan hozzáadott saját csomagpontok exportálhatók és importálhatók
* A szolgáltató neve, címke és csomagszám(foxpost és packeta egyelőre) látszódik a rendeléskezelőben a szállítási címnél
* A követési számot a WooCommerce e-mailekbe be lehet rakni(PRO verzió)
* Számlázz.hu és Woo Billingo Plus kompatibilitás(PRO verzió): számla megjegyzés feltételeként kiválastható a csomagpontos szolgáltató, illetve megjeleníthető a csomagszám is
* Kompatibilis a WooCommerce Shipment Tracking és Yith WooCommerce Order Tracking bővítménnyel(PRO verzió)
* A beállítások képernyőn elérhetők a szinkronizált csomagpont listák, manuálisan is lehet szinkront indítani

= 1.2 =
* GLS, Express One és Pick Pack Pont(Sprinter) támogatás

= 1.1 =
* Betűméret és térkép szín beállítások a testreszabás menüben
* Ha korábban eltárolta WC a szállítási költséget de utána váltasz egy másik pontra, akkor frissülni fog a szállítási költség
* Mobilon a keresőmező mindig látszik iOS-en
* Ha egy szolgáltató ki van kapcsolva a térképen, de a keresési találatoknál rákattintasz, akkor bekapcsolja
* Mobilon nem olyan pixeles a térkép
* Packeta import javítások
* Ha egy koordináta alatt(vagy közel egymáshoz) több pont van, akkor látszik melyik lett kiválasztva
* Ha csak egy féle ár van beállítva, akkor nem látszik a -tól szöveg a szállítási mód neve után(több variáció is lett, pl ingyenes, ingyenes vagy X Ft stb...)
* Kis ikonok a kosár oldalon is látszódnak
* Ha csak egy szolgáltató van aktiválva, nem látszik feleslegesen a szűrő
* Ha csomagpontos szállításról visszaváltasz normálra, újratölti az oldalt, hogy a szállítási cím mezők újra megjelenjenek
* Mobilos is látszik a térkép copyright
* A térképet a láblécbe szúrja be, így a fejléc/lábléc nem takarja ki(néhány témánál előjött ilyen hiba)

= 1.0.1 =
* Debug mód hibajavítás
* Téma kompatibilitás javítások(Blocksy, Astra, Storefront, Hello Elementor, OceanWP)

= 1.0 =
* A plugin első verziója
