// @koala-prepend "tiny-autocomplete.js"
// @koala-prepend "leaflet.js"

jQuery(document).ready(function($) {

  //Settings page
  var vp_woo_pont_settings = {
		settings_groups: ['general', 'automations', 'packeta', 'foxpost'],
		settings_groups: ['checkbox'],
		$pricing_table: $('.vp-woo-pont-settings-pricings'),
		$automations_table: $('.vp-woo-pont-settings-automations'),
		$tracking_automations_table: $('.vp-woo-pont-settings-tracking-automations'),
		$packeta_carriers_table: $('.vp-woo-pont-settings–inline-table-packeta-carriers'),
		$enabled_providers: $('.vp-woo-pont-settings-checkbox-group-enabled_providers'),
		json_data_points: [],
		init: function() {
			this.init_toggle_groups();
			this.index_packeta_carrier_fields();
			this.toggle_sections();

			$('.vp-woo-pont-settings-section-title').on('click', this.toggle_section);

			//Packeta carriers buttons
			this.$packeta_carriers_table.on('click', '.add-row', this.packeta_carrier_add);
			this.$packeta_carriers_table.on('click', '.delete-row', this.packeta_carrier_remove);
			this.$packeta_carriers_table.on('change', 'select', this.index_packeta_carrier_fields);

			//When enabled providers changed
			this.$enabled_providers.on('change', 'input', this.on_provider_change);
			this.on_provider_change(); //also run on page load

			//Pro version actions
			$('#woocommerce_vp_woo_pont_pro_email').keypress(this.submit_pro_on_enter);
			$('#vp_woo_pont_activate_pro').on('click', this.submit_activate_form);
			$('#vp_woo_pont_deactivate_pro').on('click', this.submit_deactivate_form);
			$('#vp_woo_pont_validate_pro').on('click', this.submit_validate_form);

			//Conditional logic controls
			var conditional_fields = [this.$pricing_table, this.$automations_table, this.$tracking_automations_table];
			var conditional_fields_ids = ['pricings', 'automations', 'tracking_automations'];

			//Setup conditional fields for pricing
			conditional_fields.forEach(function(table, index){
				var id = conditional_fields_ids[index];
				var singular = id.slice(0, -1);
				singular = singular.replace('_', '-');
				table.on('change', 'select.condition', {group: id}, vp_woo_pont_settings.change_x_condition);
				table.on('change', 'select.vp-woo-pont-settings-repeat-select', function(){vp_woo_pont_settings.reindex_x_rows(id)});
				table.on('click', '.add-row', {group: id}, vp_woo_pont_settings.add_new_x_condition_row);
				table.on('click', '.delete-row', {group: id}, vp_woo_pont_settings.delete_x_condition_row);
				table.on('change', 'input.condition', {group: id}, vp_woo_pont_settings.toggle_x_condition);
				table.on('click', '.delete-'+singular, {group: id}, vp_woo_pont_settings.delete_x_row);
				$('.vp-woo-pont-settings-'+singular+'-add a.add:not([data-disabled]').on('click', {group: id, table: table}, vp_woo_pont_settings.add_new_x_row);

				//If we already have some notes, append the conditional logics
				table.find('ul.conditions[data-options]').each(function(){
					var saved_conditions = $(this).data('options');
					var ul = $(this);

					saved_conditions.forEach(function(condition){
						var sample_row = $('#vp_woo_pont_'+id+'_condition_sample_row').html();
						sample_row = $(sample_row);
						sample_row.find('select.condition').val(condition.category);
						sample_row.find('select.comparison').val(condition.comparison);
						sample_row.find('.value').removeClass('selected');
						sample_row.find('.value[data-condition="'+condition.category+'"]').val(condition.value).addClass('selected').attr('disabled', false);
						ul.append(sample_row);
					});
				});

				if(table.find('.vp-woo-pont-settings-'+singular).length < 1) {
					//$('.vp-woo-pont-settings-'+singular+'-add a:not([data-disabled]').trigger('click');
				}

				//Reindex the fields
				vp_woo_pont_settings.reindex_x_rows(id);

			});

			//Load JSON files
			this.load_json_files(function(){

				//Init autocomplete
				$("#vp-woo-pont-settings-points-search").tinyAutocomplete({
					data: vp_woo_pont_settings.json_data_points,
					itemTemplate: '<li class="autocomplete-item">{{name}} - <em>{{addr}}</em></li>',
					showNoResults: true,
					noResultsTemplate: '<li class="autocomplete-no-item">No results for {{title}}</li>',
					groupTemplate: '<li class="autocomplete-group"><h2>{{title}}</h2><ul class="autocomplete-items" data-provider="{{provider}}" /></li>',
					onSelect:function(el, val){

						//Append item into list
						var item = val;
						var provider = $(el).parent().data('provider');
						vp_woo_pont_settings.create_point(provider, item);

						//Reset search field
						$("#vp-woo-pont-settings-points-search").val('');

					},
					grouped: true
				});

			});

			//Click function for points
			$('.vp-woo-pont-settings-points-list').on('click', '.vp-woo-pont-settings-point-header', vp_woo_pont_settings.toggle_point);
			$('.vp-woo-pont-settings-points-header').on('click', '.add', vp_woo_pont_settings.add_custom_point);
			$('.vp-woo-pont-settings-points-list').on('click', '.delete-point', vp_woo_pont_settings.delete_point);
			$('.vp-woo-pont-settings-points-list').on('click', '.point-value-coordinates', vp_woo_pont_settings.show_coordinates_modal);
			$(document).on( 'click', '#save_coordinates', this.save_coordinates );
			this.reindex_point_rows();

			//Import export pricing & points options
			$('.vp-woo-pont-settings-pricing-add .import, .vp-woo-pont-settings-points-add .import').click(this.import_modal);
			$('.vp-woo-pont-settings-pricing-add .export, .vp-woo-pont-settings-points-add .export').click(this.export_settings);
			$(document).on( 'click', '#vp-woo-pont-modal-import-button', this.import_settings );

			//Rate request, setup wizard trigger
			$('.vp-woo-pont-settings-widget-rating .button-secondary').on('click', this.hide_rate_request);
			$('.vp-woo-pont-restart-setup-wizard').on('click', this.restart_setup_wizard);

			//Trigger JSON import manually
			$('.vp-woo-pont-settings-widget-files .import').on('click', this.trigger_json_import);

			//Show loading indicators
			var document_types = ['tracking_number'];
			document_types.forEach(function(type){
				var $select = $('#woocommerce_vp_pont_email_'+type).parent();
				$select.block({
					message: null,
					overlayCSS: {
						background: '#F5F5F5 url(' + vp_woo_pont_params.loading + ') no-repeat center',
						backgroundSize: '16px 16px',
						opacity: 0.6
					}
				});
			});

			//Load email id values with ajax
			var data = {
				action: 'vp_woo_pont_get_email_ids',
				nonce: $('#vp_woo_pont_load_email_ids_nonce').data('nonce')
			};

			$.post(ajaxurl, data, function(response) {
				response.data.forEach(function(select){
					var selectField = $('#woocommerce_vp_pont_email_'+select.field);
					select.options.forEach(function(field){
						var option = new Option(field.label, field.id, false, field.selected);
						selectField.append(option).trigger('change');
					});
					selectField.parent().unblock();
				});
			});

			//Reload buttons
			var reloadableFields = ['sameday_pickup_point'];
			reloadableFields.forEach(function(field){
				var $field = $('#woocommerce_vp_pont_'+field);
				var self = this;
				if($field.length) {
					$field.parent().find('p.description').before('<a href="#" id="woocommerce_vp_pont_'+field+'_reload"><span class="dashicons dashicons-update"></span></a>');
					$field.parent().on('click', '#woocommerce_vp_pont_'+field+'_reload', function(){
						var $button = $(this);
						vp_woo_pont_settings.refresh_field(field, $button);
						return false;
					});
				}

			});

		},
		init_toggle_groups: function() {
			$.each(vp_woo_pont_settings.settings_groups, function( index, value ) {
				var checkbox = $('.vp-woo-pont-toggle-group-'+value);
				var group_items = $('.vp-woo-pont-toggle-group-'+value+'-item').parents('tr');
				var group_items_hide = $('.vp-woo-pont-toggle-group-'+value+'-item-hide').parents('tr');
				var single_items_hide = $('.vp-woo-pont-toggle-group-'+value+'-cell-hide');
				var checked = checkbox.is(":checked");

				if(value == 'emails' && $('.vp-woo-pont-toggle-group-'+value+':checked').length) {
					checked = true;
				}

				if(checked) {
					group_items.show();
					group_items_hide.hide();
					single_items_hide.hide();
				} else {
					group_items.hide();
					group_items_hide.show();
					single_items_hide.show();
				}
				checkbox.change(function(e){
					e.preventDefault();

					var checked = $(this).is(":checked");
					if(value == 'emails' && $('.vp-woo-pont-toggle-group-'+value+':checked').length) {
						checked = true;
					}

					if(checked) {
						group_items.show();
						group_items_hide.hide();
						single_items_hide.hide();
					} else {
						group_items.hide();
						group_items_hide.show();
						single_items_hide.show();
					}
				});
			});
		},
		toggle_section: function() {
			$(this).toggleClass('open');

			//Remember selection
			var sections = [];
			$('.vp-woo-pont-settings-section-title.open').each(function(){
				sections.push($(this).find('h3').attr('id'));
			});
			localStorage.setItem('vp_woo_pont_settings_open_sections', JSON.stringify(sections));
		},
		toggle_sections: function() {
			var data = JSON.parse(localStorage.getItem('vp_woo_pont_settings_open_sections'));
			if(data) {
				data.forEach(function(section_id){
					$('#'+section_id).parent().addClass('open');
				});
			} else {
				$('#woocommerce_vp_pont_section_general').parent().addClass('open');
			}
		},
		submit_pro_on_enter: function(e) {
			if (e.which == 13) {
				$('#vp_woo_pont_activate_pro').click();
				return false;
			}
		},
		submit_activate_form: function() {
			var key = $('#woocommerce_vp_woo_pont_pro_key').val();
			var button = $(this);
			var form = button.parents('.vp-woo-pont-settings-widget');

			var data = {
				action: 'vp_woo_pont_license_activate',
				key: key
			};

			form.block({
				message: null,
				overlayCSS: {
					background: '#ffffff url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			form.find('.vp-woo-pont-settings-widget-pro-notice').hide();

			$.post(ajaxurl, data, function(response) {
				//Remove old messages
				if(response.success) {
					window.location.reload();
					return;
				} else {
					form.find('.vp-woo-pont-settings-widget-pro-notice p').html(response.data.message);
					form.find('.vp-woo-pont-settings-widget-pro-notice').show();
				}
				form.unblock();
			});

			return false;
		},
		submit_deactivate_form: function() {
			var button = $(this);
			var form = button.parents('.vp-woo-pont-settings-widget');

			var data = {
				action: 'vp_woo_pont_license_deactivate'
			};

			form.block({
				message: null,
				overlayCSS: {
					background: '#ffffff url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			form.find('.notice').hide();

			$.post(ajaxurl, data, function(response) {
				//Remove old messages
				if(response.success) {
					window.location.reload();
					return;
				} else {
					form.find('.notice p').html(response.data.message);
					form.find('.notice').show();
				}
				form.unblock();
			});
			return false;
		},
		submit_validate_form: function() {
			var button = $(this);
			var form = button.parents('.vp-woo-pont-settings-widget');

			var data = {
				action: 'vp_woo_pont_license_validate'
			};

			form.block({
				message: null,
				overlayCSS: {
					background: '#ffffff url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			form.find('.notice').hide();

			$.post(ajaxurl, data, function(response) {
				window.location.reload();
			});
			return false;
		},
		change_x_condition: function(event) {
			var condition = $(this).val();

			//Hide all selects and make them disabled(so it won't be in $_POST)
			$(this).parent().find('.value').removeClass('selected').prop('disabled', true);
			$(this).parent().find('.value[data-condition="'+condition+'"]').addClass('selected').prop('disabled', false);
		},
		add_new_x_condition_row: function(event) {
			var sample_row = $('#vp_woo_pont_'+event.data.group+'_condition_sample_row').html();
			$(this).closest('ul.conditions').append(sample_row);
			vp_woo_pont_settings.reindex_x_rows(event.data.group);
			return false;
		},
		delete_x_condition_row: function(event) {
			$(this).parent().remove();
			vp_woo_pont_settings.reindex_x_rows(event.data.group);
			return false;
		},
		reindex_x_rows: function(group) {
			var group = group.replace('_', '-');
			$('.vp-woo-pont-settings-'+group).find('.vp-woo-pont-settings-repeat-item').each(function(index){
				$(this).find('textarea, select, input').each(function(){
					var name = $(this).data('name');
					if(name) {
						name = name.replace('X', index);
						$(this).attr('name', name);
					}
				});

				//Reindex conditions too
				$(this).find('li').each(function(index_child){
					$(this).find('select, input').each(function(){
						var name = $(this).data('name');
						if(name) {
							name = name.replace('Y', index_child);
							name = name.replace('X', index);
							$(this).attr('name', name);
						}
					});
				});

				$(this).find('.vp-woo-pont-settings-repeat-select').each(function(){
					var val = $(this).val();
					if($(this).hasClass('vp-woo-pont-settings-advanced-option-property')) {
						$('.vp-woo-pont-settings-advanced-option-value option').hide();
						$('.vp-woo-pont-settings-advanced-option-value option[value^="'+val+'"]').show();

						if(!$('.vp-woo-pont-settings-advanced-option-value').val().includes(val)) {
							$('.vp-woo-pont-settings-advanced-option-value option[value^="'+val+'"]').first().prop('selected', true);
						}
					}

					var label = $(this).find('option:selected').text();
					$(this).parent().find('label span').text(label);
					$(this).parent().find('label span').text(label);
					$(this).parent().find('label i').removeClass().addClass(val);
				});

			});

			if(group == 'pricings') {
				vp_woo_pont_settings.on_provider_change();
			}

			$( document.body ).trigger( 'wc-enhanced-select-init' );

			return false;
		},
		add_new_x_row: function(event) {
			var group = event.data.group;
			var table = event.data.table;
			var singular = group.slice(0, -1);
			var sample_row = $('#vp_woo_pont_'+singular+'_sample_row').html();
			var sample_row_conditon = $('#vp_woo_pont_'+group+'_condition_sample_row').html();
			sample_row = $(sample_row);
			sample_row.find('ul.conditions').append(sample_row_conditon);
			table.append(sample_row);
			vp_woo_pont_settings.reindex_x_rows(group);
			$( document.body ).trigger( 'wc-enhanced-select-init' );
			return false;
		},
		toggle_x_condition: function(event) {
			var group = event.data.group;
			var checked = $(this).is(":checked");
			var note = $(this).closest('.vp-woo-pont-settings-repeat-item').find('ul.conditions');
			if(checked) {
				//Add empty row if no condtions exists
				if(note.find('li').length < 1) {
					var sample_row = $('#vp_woo_pont_'+group+'_condition_sample_row').html();
					note.append(sample_row);
				}
				note.show();
			} else {
				note.hide();
			}

			//Slightly different for automations
			if(group == 'pricings' ) {
				var automation = $(this).closest('.vp-woo-pont-settings-pricing').find('.vp-woo-pont-settings-pricing-if');
				if(checked) {
					automation.show();
				} else {
					automation.hide();
				}
			}

			//Slightly different for automations
			if(group == 'automations' ) {
				var automation = $(this).closest('.vp-woo-pont-settings-automation').find('.vp-woo-pont-settings-automation-if');
				if(checked) {
					automation.show();
				} else {
					automation.hide();
				}
			}

			vp_woo_pont_settings.reindex_x_rows(event.data.group);
		},
		delete_x_row: function(event) {
			$(this).closest('.vp-woo-pont-settings-repeat-item').remove();
			vp_woo_pont_settings.reindex_x_rows(event.data.group);
			return false;
		},
		load_json_files: function(callback) {
			var providers = vp_woo_pont_params.providers;
			providers['postapont'] = 'Postapont';

			var calls = vp_woo_pont_params.files.map(function(file) {
				return $.getJSON( file.url, function( data ) {
					vp_woo_pont_settings.json_data_points.push({provider: file.type, title: providers[file.type], data: data});
				});
			});

			$.when.apply($, calls).fail(function (jqXhr, status, error) {

			}).always(function (test) {
				callback();
			});
		},
		create_point: function(provider, item) {
			var $table = $('.vp-woo-pont-settings-points-list');
			var sample_row = $('#vp_woo_pont_point_sample_row').html();
			var $sample_row = $(sample_row);

			$sample_row.addClass('open');

			$sample_row.find('.point-value-title').text(vp_woo_pont_params.providers[provider]+': '+item.name+' #'+item.id);
			$sample_row.find('.point-value-id').val(item.id);
			$sample_row.find('.point-value-provider').val(provider);
			$sample_row.find('.point-value-name').val(item.name);
			$sample_row.find('.point-value-coordinates').val(item.lat+';'+item.lon);
			$sample_row.find('.point-value-city').val(item.city);
			$sample_row.find('.point-value-zip').val(item.zip);
			$sample_row.find('.point-value-addr').val(item.addr);
			$sample_row.find('.point-value-comment').val(item.comment);

			//If its not a custom item, disable ID change
			if(provider != 'custom') {
				$sample_row.find('.point-value-id').attr('readonly', true);
			}

			$table.append($sample_row);
			vp_woo_pont_settings.reindex_point_rows();
		},
		reindex_point_rows: function() {
			$('.vp-woo-pont-settings-points-list').find('.vp-woo-pont-settings-point').each(function(index){
				$(this).find('textarea, select, input').each(function(){
					var name = $(this).data('name');
					name = name.replace('X', index);
					$(this).attr('name', name);
				});
			});
		},
		toggle_point: function() {
			$(this).parents('.vp-woo-pont-settings-point').toggleClass('open');
			return false;
		},
		add_custom_point: function() {
			var points = $('.vp-woo-pont-settings-point').length;
			vp_woo_pont_settings.create_point('custom', {
				name: 'Pickup point '+points+1,
				id: 'point_'+points+1,
				lat: 0,
				lon: 0
			});

			return false;
		},
		delete_point: function(event) {
			$(this).closest('.vp-woo-pont-settings-point').remove();
			vp_woo_pont_settings.reindex_point_rows();
			return false;
		},
		show_coordinates_modal: function() {
			$(this).WCBackboneModal({
				template: 'vp-woo-pont-modal-coordinates',
				variable : {}
			});

			//Get existing coordinates
			var coordinates = $(this).val();
			coordinates = coordinates.split(';');
			var $field = $(this);

			//Create a map
			var mymap = L.map('map-coordinates')

			//Set coordinates
			if(coordinates[0] != '0') {
				mymap.setView(coordinates, 17);
			} else {
				mymap.setView([47.25525656277509, 19.54590752720833], 5); //Just to center in Hungary
			}

			//Load images into map
			L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				maxZoom: 19,
				attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(mymap);

			//Store coordinates
			mymap.on('moveend', function(ev) {
				var latlng = mymap.getCenter();
				$('#save_coordinates').data('coordinates', latlng.lat+';'+latlng.lng)
				$('#save_coordinates').data('field', $field);
			});

			return false;
		},
		save_coordinates: function() {
			var $field = $(this).data('field');
			var coordinates = $(this).data('coordinates');
			$field.val(coordinates);
			$('.modal-close-link').trigger('click');
			return false;
		},
		export_settings: function() {
			var nonce = $(this).data('nonce');
			var type = $(this).data('type');

			//Create request
			var data = {
				action: 'vp_woo_pont_export_settings',
				type: type,
				nonce: nonce,
			};

			//Make request
			$.post(ajaxurl, data, function(response) {

				//Convert JSON Array to string.
				var json = JSON.stringify(response.data);

				//Convert JSON string to BLOB.
				json = [json];
				var blob1 = new Blob(json, { type: "text/plain;charset=utf-8" });

				//Check the Browser.
				var isIE = false || !!document.documentMode;
				if (isIE) {
						window.navigator.msSaveBlob(blob1, 'vp-woo-pont-'+type+'.json');
				} else {
						var url = window.URL || window.webkitURL;
						link = url.createObjectURL(blob1);
						var a = $("<a />");
						a.attr("download", 'vp-woo-pont-'+type+'.json');
						a.attr("href", link);
						$("body").append(a);
						a[0].click();
						$("body").remove(a);
				}

			});

			return false;
		},
		import_modal: function() {
			var type = $(this).data('type');
			console.log(type);

			$(this).WCBackboneModal({
				template: 'vp-woo-pont-modal-import',
				variable : {
					type: type
				}
			});
			return false;
		},
		import_settings: function() {

			//Setup form
			var formData = new FormData($(".vp-woo-pont-modal-import form")[0]);

			//Make request
			$.ajax({
				url: ajaxurl,
				type: "POST",
				data : formData,
				processData: false,
				contentType: false,
				success: function(data){
					window.location.reload();
				},
				error: function(xhr, ajaxOptions, thrownError) {
				}
			});

			return false;

		},
		hide_rate_request: function() {
			var nonce = $(this).data('nonce');
			var form = $(this).parents('.vp-woo-pont-settings-widget');
			var data = {
				action: 'vp_woo_pont_hide_rate_request',
				nonce: nonce
			};

			form.block({
				message: null,
				overlayCSS: {
					background: '#ffffff url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			$.post(ajaxurl, data, function(response) {
				form.slideUp();
			});
		},
		restart_setup_wizard: function() {
			var nonce = $(this).data('nonce');
			var form = $(this).parents('.vp-woo-pont-settings-widget');
			var url = $(this).data('url');
			var data = {
				action: 'vp_woo_pont_restart_setup_wizard',
				nonce: nonce
			};

			form.block({
				message: null,
				overlayCSS: {
					background: '#ffffff url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			$.post(ajaxurl, data, function(response) {
				window.location.href = url;
			});

			return false;
		},
		trigger_json_import: function() {
			var form = $(this).parents('.vp-woo-pont-settings-widget');
			var nonce = form.data('nonce');
			var provider_id = $(this).data('provider');
			var $message = form.find('.vp-woo-pont-settings-widget-files-notice');
			var $button = $(this);

			var data = {
				action: 'vp_woo_pont_import_json_manually',
				nonce: nonce,
				provider: provider_id
			};

			$message.hide();
			$message.removeClass('success');
			$message.removeClass('error');

			form.block({
				message: null,
				overlayCSS: {
					background: '#ffffff url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			$.post(ajaxurl, data, function(response) {
				$message.find('p').html(response.data.message);
				$message.show();
				form.unblock();
				if(response.success) {
					$message.addClass('success');
					console.log($(this).parent().find('a.download-link'));
					$button.parent().find('a.download-link').attr('href', response.data.url);
					$button.parent().find('a.download-link').show();
					$button.parent().find('span').hide();
				} else {
					$message.addClass('error');
				}
			});

			return false;
		},
		packeta_carrier_add: function() {
			var table = $(this).closest('tbody');
			var row = $(this).closest('tr');
			table.append(row.clone());
			vp_woo_pont_settings.index_packeta_carrier_fields();
			return false;
		},
		packeta_carrier_remove: function() {
			var row = $(this).closest('tr');
			row.remove();
			vp_woo_pont_settings.index_packeta_carrier_fields();
			return false;
		},
		index_packeta_carrier_fields: function() {
			vp_woo_pont_settings.$packeta_carriers_table.find('tbody tr').each(function(index, row){
				$(this).find('select, input[type="hidden"]').each(function(){
					var name = $(this).data('name');
					name = name.replace('X', index);
					$(this).attr('name', name);
				});

				var selected_country = $(this).find('.vp-woo-pont-packeta-carriers-table-country select').val();
				if(!$(this).find('.vp-woo-pont-packeta-carriers-table-country select').length) {
					selected_country = $(this).find('.vp-woo-pont-packeta-carriers-table-country input').val();
				}

				var $carrier_select = $(this).find('.vp-woo-pont-packeta-carriers-table-carrier select');
				var selected_carrier = $carrier_select.val();

				$carrier_select.find('optgroup').each(function(){
					if($(this).attr('label') == selected_country) {
						$(this).removeAttr('hidden');

						if(!$(this).find('option:selected').length) {
							$carrier_select.find('option').removeAttr('selected');
							$(this).find('option').first().attr('selected', true);
						}
					} else {
						$(this).attr('hidden', true);
						$(this).find('option').removeAttr('selected');
					}
				});

				if(!$carrier_select.find('option:selected').length) {
					$carrier_select.find('option').first().attr('selected', true);
				}

			});
		},
		on_provider_change: function() {
			var enabled_providers = vp_woo_pont_settings.$enabled_providers.find('input:checked');
			$('.vp-woo-pont-settings-pricing-points li').hide();

			enabled_providers.each(function(){
				var provider = $(this).val();
				$('.vp-woo-pont-settings-pricing-points').find('li.provider-'+provider).show();
			});
		},
		refresh_field: function(field, button) {
			var $this = button;
			var data = {
				action: 'vp_woo_pont_reload_'+field
			};

			if(!$this.hasClass('loading')) {
				$this.addClass('loading');
				$.post(ajaxurl, data, function(response) {
					$this.removeClass('loading');
					$this.addClass('loaded');
					setTimeout(function() {
						$this.removeClass('loaded');
					}, 1000);
					if(response.data) {
						var $select = $('#woocommerce_vp_wpont_'+field);
						var currentOption = $select.val();
						$select.val(null).empty();
						response.data.forEach(function(block){
							var newOption = new Option(block.name, block.id, true, true);
							$select.append(newOption).trigger('change');
						});
						$select.val(currentOption);
						$select.trigger('change');
					}
				});
			}

			return false;
		},
  }

  //Init settings page
  if($('.vp-woo-pont-settings-content').length) {
    vp_woo_pont_settings.init();
  }

	//Store management links
	if(window.location.search.indexOf('page=wc-admin') > -1) {
		var waitForEl = function(selector, callback) {
			if (!jQuery(selector).size()) {
				setTimeout(function() {
					window.requestAnimationFrame(function(){ waitForEl(selector, callback) });
				}, 100);
			}else {
				callback();
			}
		};

		waitForEl('.woocommerce-quick-links__category', function() {
			var sampleLink = $('.woocommerce-quick-links__item').last();
			var category = sampleLink.parent();
			var newLink = sampleLink.clone();
			newLink.find('div').text('WooCommerce Pont');
			newLink.find('a').attr('href', vp_woo_pont_params.settings_link);
			newLink.find('svg').html('<path d="M12.0343082,0 C18.6550698,0.0191048779 24.009494,5.39673067 23.9998973,12.0175125 C23.9902487,18.6382944 18.6202155,24.0003333 11.9994266,24.0000922 C5.35799889,23.9855985 -0.0142689467,18.5900518 -0.000102749027,11.9486235 C0.0283390863,5.3278955 5.41354658,-0.0189034005 12.0343082,0 Z M15.5212315,5.26059858 L9.74665195,5.26035672 C8.72090474,5.2865489 7.83672571,5.98938077 7.57980676,6.98277698 L5.89679363,13.7640884 C5.56124459,14.9610343 5.79607372,16.2458303 6.53332835,17.2466941 C7.27472842,18.1757371 8.41025143,18.7015248 9.59832787,18.6658984 L12.6085953,18.6658984 C14.8608784,18.5770482 16.792551,17.0312663 17.3730275,14.8532579 C17.6653802,13.8034253 17.4578303,12.6774471 16.8103812,11.8008468 C16.7665955,11.7461146 16.7184312,11.6941191 16.6719089,11.6426709 C17.4873396,11.0260188 18.0728851,10.1541553 18.3352185,9.16604182 C18.5995274,8.21280727 18.4103804,7.19096896 17.8223784,6.39550118 C17.257564,5.69611309 16.4151966,5.28281103 15.5212315,5.26059858 Z M16.2373358,7.6422992 C16.4417205,7.94622868 16.4951923,8.32719 16.3823759,8.67564189 C16.082346,9.69565048 15.1994368,10.4359392 14.142737,10.5535014 L14.1224861,10.5535014 L14.06447,10.5567853 L11.4220026,10.5567853 L10.9217509,12.5714752 L10.9217509,12.5807797 L14.0633754,12.5807797 C14.0890995,12.5807797 14.1142763,12.5807797 14.139453,12.5774957 C14.5542791,12.5577198 14.9549386,12.7308742 15.2247913,13.0465501 C15.4897295,13.4318303 15.5614667,13.9180267 15.4190903,14.3634053 C15.0593092,15.6402013 13.9344003,16.5520111 12.6107845,16.6397147 L9.59887519,16.6413567 C9.03345423,16.6702507 8.48693879,16.4336298 8.12110757,16.0015381 C7.76543127,15.4907287 7.66620773,14.8440579 7.85237279,14.2501098 L9.53538591,7.47153494 C9.57390076,7.38030506 9.65152685,7.31132643 9.74665195,7.28380372 L15.3796832,7.28380372 C15.7061399,7.26115359 16.0241084,7.39406322 16.2373358,7.6422992 Z"></path>');
			category.append(newLink);
		});
	}

	// Hide notice
  $( '.vp-woo-pont-notice .vp-woo-pont-hide-notice').on('click', function(e) {
		e.preventDefault();
		var el = $(this).closest('.vp-woo-pont-notice');
		$(el).find('.vp-woo-pont-wait').remove();
		$(el).append('<div class="vp-woo-pont-wait"></div>');
		if ( $('.vp-woo-pont-notice.updating').length > 0 ) {
			var button = $(this);
			setTimeout(function(){
				button.triggerHandler( 'click' );
			}, 100);
			return false;
		}
		$(el).addClass('updating');
		$.post( ajaxurl, {
				action: 'vp_woo_pont_hide_notice',
				security: $(this).data('nonce'),
				notice: $(this).data('notice'),
				remind: $(this).hasClass( 'remind-later' ) ? 'yes' : 'no'
		}, function(){
			$(el).removeClass('updating');
			$(el).fadeOut(100);
		});
  });

	//Order management
	$('.vp-woo-pont-remove-from-order').click(function(){
		$this = $(this);
		var r = confirm($this.data('question'));
		var order = $this.data('order');
		var security = $this.data('nonce');

		//Check for question
		if (r != true) {
			return false;
		}

		//Make request
		$.post( ajaxurl, {
				action: 'vp_woo_pont_remove_from_order',
				security: security,
				order: order
		}, function(){
			$this.parent().hide();
		});

		return false;
	});

	//Metabox functions
	var vp_woo_pont_metabox = {
		prefix: 'vp_woo_pont_',
		prefix_id: '#vp_woo_pont_',
		prefix_class: '.vp-woo-pont-',
		selected_provider: '',
		$metaboxContent: $('#vp_woo_pont_metabox .inside'),
		$optionsContent: $('.vp-woo-pont-metabox-generate-options'),
		$generateContent: $('.vp-woo-pont-metabox-generate'),
		$optionsButton: $('#vp_woo_pont_label_options'),
		$generateButtonLabel: $('#vp_woo_pont_label_generate'),
		$pointRow: $('.vp-woo-pont-metabox-rows-data-provider'),
		$labelRow: $('.vp-woo-pont-metabox-rows-link-pdf'),
		$trackingRow: $('.vp-woo-pont-metabox-rows-link-tracking'),
		$voidRow: $('.vp-woo-pont-metabox-rows-data-void'),
		$removeRow: $('.vp-woo-pont-metabox-rows-data-remove'),
		$replaceRow: $('.vp-woo-pont-metabox-rows-data-replace'),
		$messages: $('.vp-woo-pont-metabox-messages-label'),
		$providerRow: $('.vp-woo-pont-metabox-rows-data-provider'),
		$homeDeliveryProviders: $('.vp-woo-pont-metabox-rows-data-home-delivery-providers'),
		$modifyProviderButton: $('#vp_woo_pont_modify_provider'),
		$homeDeliveryProviderInput: $('input[name="home_delivery_provider"]'),
		nonce: $('.vp-woo-pont-metabox-content').data('nonce'),
		order: $('.vp-woo-pont-metabox-content').data('order'),
		$updateTrackingInfoButton: $('#vp_woo_pont_update_tracking_info'),
		$trackingInfoList: $('#vp_woo_pont_tracking_info_list'),
		$trackingMessages: $('.vp-woo-pont-metabox-messages-tracking'),
		selected_replacement: false,
		init: function() {
			this.$optionsButton.on( 'click', this.show_options );
			this.$generateButtonLabel.on( 'click', this.generate_label );
			this.$removeRow.find('a').on( 'click', this.remove_point );
			this.$replaceRow.find('a').on( 'click', this.replace_point );
			this.$voidRow.find('a').on( 'click', this.void_label );
			this.$messages.find('a').on( 'click', this.hide_message );
			this.$trackingMessages.find('a').on( 'click', this.hide_message );
			this.$modifyProviderButton.on( 'click', this.show_provider_options );
			this.$homeDeliveryProviderInput.on( 'change', this.on_provider_change );
			this.$updateTrackingInfoButton.on( 'click', this.update_tracking_info );
			$(document).on( 'click', '.vp-woo-pont-modal-replace #save_point', this.save_replacement_point );

			//Use the heartbeat api to update the order data, if the label was generated in the bg(this is for Foxpost for example, which uses a webhook to notify the store)
			$(document).on( 'heartbeat-tick', function ( event, data ) {
				if (data.vp_woo_pont_label_pdf) {
						vp_woo_pont_metabox.$labelRow.removeClass('pending');
						vp_woo_pont_metabox.$labelRow.find('a').attr('href', data.vp_woo_pont_label_pdf);
				}
			});

			//Set provider id
			this.selected_provider = $('.vp-woo-pont-metabox-content').data('provider_id');
			vp_woo_pont_metabox.toggle_options();

		},
		loading_indicator: function(button, color) {
			vp_woo_pont_metabox.hide_message();
			button.block({
				message: null,
				overlayCSS: {
					background: color+' url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});
		},
		show_options: function() {
			vp_woo_pont_metabox.$optionsButton.toggleClass('active');
			vp_woo_pont_metabox.$optionsContent.slideToggle();
			return false;
		},
		toggle_options: function() {
			vp_woo_pont_metabox.$optionsContent.find('li').each(function(){
				var $item = $(this);
				var supported_providers = $item.data('providers');
				var selected_provider = vp_woo_pont_metabox.selected_provider;

				//Show all
				$item.show();

				//Hide if not supported
				if(supported_providers && !supported_providers.includes(selected_provider)) {
					$item.hide();
				}

			});
		},
		generate_label: function() {
			var $this = $(this);
			var r = confirm($this.data('question'));
			if (r != true) {
				return false;
			}

			//Set custom options
			var package_count = $('#vp_woo_pont_package_count').val();
			var pickup_date = $('#vp_woo_pont_pickup_date').val();
			var package_contents = $('#vp_woo_pont_package_contents').val();

			//Create request
			var data = {
				action: vp_woo_pont_metabox.prefix+'generate_label',
				nonce: vp_woo_pont_metabox.nonce,
				order: vp_woo_pont_metabox.order,
				package_count: package_count,
				pickup_date: pickup_date,
				package_contents: package_contents
			};

			//If a custom provider is set
			if($('input[name="home_delivery_provider"]:checked').val()) {
				data.provider = $('input[name="home_delivery_provider"]:checked').val();
			}

			//Show loading indicator
			vp_woo_pont_metabox.loading_indicator(vp_woo_pont_metabox.$metaboxContent, '#fff');

			//Make request
			$.post(ajaxurl, data, function(response) {

				//Hide loading indicator
				vp_woo_pont_metabox.$metaboxContent.unblock();

				//Show success/error messages
				vp_woo_pont_metabox.show_messages(response);

				//On success and error
				if(!response.data.error) {
					vp_woo_pont_metabox.$labelRow.slideDown();
					vp_woo_pont_metabox.$labelRow.find()
					vp_woo_pont_metabox.$labelRow.find('strong').text(response.data.number);
					vp_woo_pont_metabox.$labelRow.find('a').attr('href', response.data.pdf);
					vp_woo_pont_metabox.$trackingRow.find('strong').text(response.data.number);

					if(response.data.pending) {
						vp_woo_pont_metabox.$labelRow.addClass('pending');
						vp_woo_pont_metabox.add_to_heartbeat('label');
					} else {
						vp_woo_pont_metabox.$trackingRow.slideDown();
						vp_woo_pont_metabox.$voidRow.slideDown();
					}

				}

			});

			return false;
		},
		void_label_timeout: false,
		void_label: function() {
			var $this = $(this);

			//Do nothing if already marked completed
			if($this.hasClass('confirm')) {

				//Reset timeout
				clearTimeout(vp_woo_pont_metabox.void_label_timeout);

				//Show loading indicator
				vp_woo_pont_metabox.loading_indicator(vp_woo_pont_metabox.$voidRow, '#fff');

				//Create request
				var data = {
					action: vp_woo_pont_metabox.prefix+'void_label',
					nonce: vp_woo_pont_metabox.nonce,
					order: vp_woo_pont_metabox.order,
				};

				$.post(ajaxurl, data, function(response) {

					//Hide loading indicator
					vp_woo_pont_metabox.$voidRow.unblock();

					//Show success/error messages
					vp_woo_pont_metabox.show_messages(response);

					//On success and error
					if(!response.data.error) {
						vp_woo_pont_metabox.$labelRow.slideUp();
						vp_woo_pont_metabox.$trackingRow.slideUp();
						vp_woo_pont_metabox.$voidRow.slideUp(function(){
							$this.text(response.data.completed);
							$this.removeClass('confirm');
						});

						$('#vp_woo_pont_metabox_tracking').slideUp();
						vp_woo_pont_metabox.$generateContent.slideDown();
					}

					//On success and error
					$this.fadeOut(function(){
						$this.text($this.data('trigger-value'));
						$this.fadeIn();
						$this.removeClass('confirm');
					});

				});

			} else {
				vp_woo_pont_metabox.void_invoice_timeout = setTimeout(function(){
					$this.fadeOut(function(){
						$this.text($this.data('trigger-value'));
						$this.fadeIn();
						$this.removeClass('confirm');
					});
				}, 5000);

				$this.addClass('confirm');
				$this.fadeOut(function(){
					$this.text($this.data('question'))
					$this.fadeIn();
				});
			}

			return false;
		},
		remove_point_timeout: false,
		remove_point: function() {
			var $this = $(this);

			//Do nothing if already marked completed
			if($this.hasClass('confirm')) {

				//Reset timeout
				clearTimeout(vp_woo_pont_metabox.remove_point_timeout);

				//Show loading indicator
				vp_woo_pont_metabox.loading_indicator(vp_woo_pont_metabox.$removeRow, '#fff');

				//Create request
				var data = {
					action: vp_woo_pont_metabox.prefix+'remove_point',
					nonce: vp_woo_pont_metabox.nonce,
					order: vp_woo_pont_metabox.order,
				};

				$.post(ajaxurl, data, function(response) {

					//Hide loading indicator
					vp_woo_pont_metabox.$removeRow.unblock();

					//Show success/error messages
					vp_woo_pont_metabox.show_messages(response);

					//On success and error
					if(!response.data.error) {
						vp_woo_pont_metabox.$pointRow.slideUp();
						vp_woo_pont_metabox.$removeRow.slideUp(function(){
							$this.text(response.data.completed);
							$this.removeClass('confirm');
						});
					}

					//On success and error
					$this.fadeOut(function(){
						$this.text($this.data('trigger-value'));
						$this.fadeIn();
						$this.removeClass('confirm');
					});

				});

			} else {
				vp_woo_pont_metabox.void_invoice_timeout = setTimeout(function(){
					$this.fadeOut(function(){
						$this.text($this.data('trigger-value'));
						$this.fadeIn();
						$this.removeClass('confirm');
					});
				}, 5000);

				$this.addClass('confirm');
				$this.fadeOut(function(){
					$this.text($this.data('question'))
					$this.fadeIn();
				});
			}

			return false;
		},
		show_messages: function(response) {
			var $messages = this.$messages;
			if(response.container) {
				$messages = response.container;
			}

			if(response.data.messages && response.data.messages.length > 0) {
				$messages.removeClass('vp-woo-pont-metabox-messages-success');
				$messages.removeClass('vp-woo-pont-metabox-messages-error');

				if(response.data.error) {
					$messages.addClass('vp-woo-pont-metabox-messages-error');
				} else {
					$messages.addClass('vp-woo-pont-metabox-messages-success');
				}

				$ul = $messages.find('ul');
				$ul.html('');

				$.each(response.data.messages, function(i, value) {
					var li = $('<li>')
					li.append(value);
					$ul.append(li);
				});
				$messages.slideDown();
			}
		},
		hide_message: function() {
			vp_woo_pont_metabox.$messages.slideUp();
			vp_woo_pont_metabox.$trackingMessages.slideUp();
			return false;
		},
		replace_point: function() {
			$(this).WCBackboneModal({
				template: 'vp-woo-pont-modal-replace',
				variable : {}
			});

			//Show loading indicator
			vp_woo_pont_metabox.loading_indicator(vp_woo_pont_metabox.$replaceRow, '#fff');

			//Load JSON files
			vp_woo_pont_settings.load_json_files(function(){

				//Hide loading indicator
				vp_woo_pont_metabox.$replaceRow.unblock();

				//Init autocomplete
				$("#vp-woo-pont-modal-replace-search").tinyAutocomplete({
					data: vp_woo_pont_settings.json_data_points,
					itemTemplate: '<li class="autocomplete-item">{{name}} - <em>{{addr}}</em></li>',
					showNoResults: true,
					noResultsTemplate: '<li class="autocomplete-no-item">No results for {{title}}</li>',
					groupTemplate: '<li class="autocomplete-group"><h2>{{title}}</h2><ul class="autocomplete-items" data-provider="{{provider}}" /></li>',
					onSelect:function(el, val){

						//Append item into list
						var item = val;
						var provider = $(el).parent().data('provider');
						if(provider == 'postapont') provider += '_'+item.group;
						vp_woo_pont_metabox.selected_replacement = {provider: provider, id: item.id}

						$('.vp-woo-pont-modal-replace-selected').find('.name').text(provider+' '+item.name);
						$('.vp-woo-pont-modal-replace-selected').find('.addr').text(item.addr);

					},
					grouped: true
				});

			});

			return false;

		},
		save_replacement_point: function() {
			var $modal = $('.vp-woo-pont-modal-replace');

			//Show loading indicator
			vp_woo_pont_metabox.loading_indicator($modal, '#fff');

			//Make AJAX request to replace point data
			var data = {
				action: vp_woo_pont_metabox.prefix+'replace_point',
				nonce: vp_woo_pont_metabox.nonce,
				order: vp_woo_pont_metabox.order,
				provider: vp_woo_pont_metabox.selected_replacement.provider,
				point_id: vp_woo_pont_metabox.selected_replacement.id
			};

			//Set selected provider id
			vp_woo_pont_metabox.selected_provider = vp_woo_pont_metabox.selected_replacement.provider;
			vp_woo_pont_metabox.toggle_options();

			$.post(ajaxurl, data, function(response) {

				//Hide loading indicator
				$modal.unblock();

				//Hide modal
				$('.modal-close-link').trigger('click');

				//Show success/error messages
				vp_woo_pont_metabox.show_messages(response);

				//On success and error
				if(response.success) {
					vp_woo_pont_metabox.$pointRow.slideDown();
					vp_woo_pont_metabox.$pointRow.find('strong').text(response.data.provider_label);
					vp_woo_pont_metabox.$pointRow.find('span').text(response.data.point_name);
					vp_woo_pont_metabox.$pointRow.find('i').removeAttr('class').addClass('vp-woo-pont-provider-icon-'+response.data.provider);
				}

			});

			return false;
		},
		add_to_heartbeat: function() {
			$( document ).on( 'heartbeat-send', function ( event, data ) {
				data.vp_woo_pont_label_generate = true;
				data.vp_woo_pont_order_id = vp_woo_pont_metabox.order;
			});
		},
		show_provider_options: function() {
			//Hide the currently selected provider and show a list instead of available options
			vp_woo_pont_metabox.$providerRow.slideUp();
			vp_woo_pont_metabox.$homeDeliveryProviders.slideDown();
			return false;
		},
		on_provider_change: function() {

			//Get selected value
			var label = $('input[name="home_delivery_provider"]:checked').data('label');
			var id = $('input[name="home_delivery_provider"]:checked').val();

			//Save it with ajax
			var data = {
				action: vp_woo_pont_metabox.prefix+'save_provider',
				nonce: vp_woo_pont_metabox.nonce,
				order: vp_woo_pont_metabox.order,
				provider: id
			};

			//Set selected provider id
			vp_woo_pont_metabox.selected_provider = id;
			vp_woo_pont_metabox.toggle_options();

			//Not that important, so just do it in the background without any loading indicator
			$.post(ajaxurl, data);

			//Replace selected provider and show just that
			vp_woo_pont_metabox.$providerRow.find('strong').text(label);
			vp_woo_pont_metabox.$providerRow.find('i').attr('class', 'vp-woo-pont-provider-icon-'+id);
			vp_woo_pont_metabox.$providerRow.slideDown();
			vp_woo_pont_metabox.$homeDeliveryProviders.slideUp();

		},
		update_tracking_info: function() {
			var $this = $(this);

			//Show loading indicator
			vp_woo_pont_metabox.loading_indicator(vp_woo_pont_metabox.$trackingInfoList, '#fff');

			//Create request
			var data = {
				action: vp_woo_pont_metabox.prefix+'update_tracking_info',
				nonce: vp_woo_pont_metabox.nonce,
				order: vp_woo_pont_metabox.order,
			};

			$.post(ajaxurl, data, function(response) {

				//Hide loading indicator
				vp_woo_pont_metabox.$trackingInfoList.unblock();

				//Show success/error messages
				response.container = vp_woo_pont_metabox.$trackingMessages;
				vp_woo_pont_metabox.show_messages(response);

				//On success
				if(!response.data.error) {

					//If we have a new item, append it
					if(response.data.tracking_info.length > vp_woo_pont_metabox.$trackingInfoList.find('li').length-1) {

						//Show the latest item
						var latest = response.data.tracking_info[0];

						//Create new item
						var $event = vp_woo_pont_metabox.$trackingInfoList.find('.note-sample').clone();
						$event.removeClass('note-sample');
						$event.find('.note_content p').text(latest.label);
						$event.find('.exact-date').text(new Date(latest.date* 1e3).toLocaleString());
						$event.show();

						setTimeout(function() {
							$event.removeClass('customer-note');
						}, 3000);

						//Prepend to list
						vp_woo_pont_metabox.$trackingInfoList.prepend($event);

					}
				}

			});

			return false;
		},
	}

	//Metabox
	if($('#vp_woo_pont_metabox').length) {
		vp_woo_pont_metabox.init();
	}

	//Bulk actions
	var vp_wo_pont_bulk_actions = {
		$dpd_button: $('.vp-woo-pont-dpd-start-sync a'),
		$print_button: $('#vp-woo-pont-bulk-print-generate'),
		init: function() {

			//Actions for the download and print bulk action notice
			var printAction = $('#vp-woo-pont-bulk-print');
			var downloadAction = $('#vp-woo-pont-bulk-download');
			printAction.on( 'click', this.printInvoices );
			if(printAction.length) {
				printAction.trigger('click');
			}

			//This will sync labels with DPD
			this.$dpd_button.on('click', this.dpd_sync);

			//Print button after the labels generated notice
			this.$print_button.on('click', this.print_generate);

			//Function related to the tracking info modal
			$('.vp-woo-pont-orders-tracking-event-label').on('click', this.show_tracking_modal);

		},
		printInvoices: function() {
			var pdf_url = $(this).data('pdf');
			if (typeof printJS === 'function') {
				printJS(pdf_url);
				return false;
			}
		},
		dpd_sync: function() {
			var nonce = $(this).data('nonce');
			var $button = $(this);

			//Create request
			var data = {
				action: vp_woo_pont_metabox.prefix+'dpd_run_sync',
				nonce: nonce
			};

			//Only one click
			if($button.hasClass('loading')) return false;

			//Loading indicator
			$button.addClass('loading');

			//Make an ajax call in the background. No error handling, since this usually works just fine
			$.post(ajaxurl, data, function(response) {
				$button.addClass('success');
				setTimeout(function(){
					$button.removeClass('loading');
					$button.removeClass('success');
				}, 3000);
			});

			return false;
		},
		print_generate: function() {
			var nonce = $(this).data('nonce');
			var $button = $(this);
			var orders = $(this).data('orders');
			var $message = $('.vp-woo-pont-bulk-actions');

			//Create request
			var data = {
				action: vp_woo_pont_metabox.prefix+'print_labels',
				nonce: nonce,
				orders: orders
			};

			//Loading indicator
			vp_woo_pont_metabox.loading_indicator($message, '#fff');

			//Make an ajax call in the background. No error handling, since this usually works just fine
			$.post(ajaxurl, data, function(response) {
				$message.unblock();

				if (typeof printJS === 'function') {
					printJS(response.data.url);
				}
				return false;
			});

			return false;
		},
		show_tracking_modal: function() {
			var events = $(this).data('events');
			var order_id = $(this).data('order_id');
			var link = $(this).data('link');
			var parcel_number = $(this).data('parcel_number');

			var ul = $('<ul/>');
			ul.addClass('order_notes');

			$(events).each(function(i) {
				ul.append('<li class="note"><div class="note_content"><p>'+events[i]['label']+'</p></div><p class="meta">'+events[i]['date']+'</p></li>');
			});

			$(this).WCBackboneModal({
				template: 'vp-woo-pont-modal-tracking',
				variable : {events: ul.prop("outerHTML"), has_events: (events), order_id: order_id, link: link, parcel_number: parcel_number}
			});

		}
	}

	//Init bulk actions
	if($('.vp-woo-pont-bulk-actions').length || $('.vp-woo-pont-dpd-start-sync').length || $('#vp-woo-pont-bulk-print-generate').length) {
		vp_wo_pont_bulk_actions.init();
	}

	//Background generate actions
	var vp_woo_pont_background_actions = {
		$menu_bar_item: $('#wp-admin-bar-vp-woo-pont-bg-generate-loading'),
		$link_stop: $('#vp-woo-pont-bg-generate-stop'),
		$link_refresh: $('#vp-woo-pont-bg-generate-refresh'),
		finished: false,
		nonce: '',
		init: function() {
			this.$link_stop.on( 'click', this.stop );
			this.$link_refresh.on( 'click', this.reload_page );

			//Store nonce
			this.nonce = this.$link_stop.data('nonce');

			//Refresh status every 5 second
			var refresh_action = this.refresh;
			setTimeout(refresh_action, 5000);

		},
		reload_page: function() {
			location.reload();
			return false;
		},
		stop: function() {
			var data = {
				action: 'vp_woo_pont_bg_generate_stop',
				nonce: vp_woo_pont_background_actions.nonce,
			}

			$.post(ajaxurl, data, function(response) {
				vp_woo_pont_background_actions.mark_stopped();
			});
			return false;
		},
		refresh: function() {
			var data = {
				action: 'vp_woo_pont_bg_generate_status',
				nonce: vp_woo_pont_background_actions.nonce,
			}

			if(!vp_woo_pont_background_actions.finished) {
				$.post(ajaxurl, data, function(response) {
					if(response.data.finished) {
						vp_woo_pont_background_actions.mark_finished();
					} else {
						//Repeat after 5 seconds
						setTimeout(vp_woo_pont_background_actions.refresh, 5000);
					}

				});
			}
		},
		mark_finished: function() {
			this.finished = true;
			this.$menu_bar_item.addClass('finished');
		},
		mark_stopped: function() {
			this.mark_finished();
			this.$menu_bar_item.addClass('stopped');
		}
	}

	//Init background generate loading indicator
	if($('#wp-admin-bar-vp-woo-pont-bg-generate-loading').length) {
		vp_woo_pont_background_actions.init();
	}

	//Background generate actions
	var vp_woo_pont_mpl_actions = {
		$button: $('#vp_woo_pont_mpl_close_shipments'),
		$button_alt: $('#vp_woo_pont_mpl_close_orders'),
		$table: $('.wp-list-table.csomagok'),
		$error: $('.vp-woo-pont-admin-mpl-notice'),
		nonce: '',
		init: function() {

			//Store nonce
			this.nonce = this.$button.data('nonce');

			//Refresh status every 5 second
			this.$button.on('click', this.close_shipments);
			this.$button_alt.on('click', this.close_orders);

		},
		reload_page: function() {
			location.reload();
			return false;
		},
		close_orders: function() {
			//Setup request data
			var data = {
				action: 'vp_woo_pont_mpl_close_orders',
				nonce: vp_woo_pont_mpl_actions.nonce,
				orders: []
			}

			//Get checked orders
			vp_woo_pont_mpl_actions.$table.find('input[name="selected_packages"]:checked').each(function(){
				var package_number = $(this).data('order');
				data.orders.push(package_number);
			});

			//Show loading indicator
			vp_woo_pont_mpl_actions.$table.block({
				message: null,
				overlayCSS: {
					background: '#F5F5F5 url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			//Make ajax request
			$.post(ajaxurl, data, function(response) {

				//Check for errors
				if(response.success) {
					vp_woo_pont_mpl_actions.reload_page();
				} else {
					vp_woo_pont_mpl_actions.$error.find('p').text(response.data.message);
					vp_woo_pont_mpl_actions.$error.show();
				}

				//Hide loading indicator
				vp_woo_pont_mpl_actions.$table.unblock();

			});
			return false;
		},
		close_shipments: function(action) {

			//Setup request data
			var data = {
				action: 'vp_woo_pont_mpl_close_shipments',
				nonce: vp_woo_pont_mpl_actions.nonce,
				packages: []
			}

			//Get checked orders
			vp_woo_pont_mpl_actions.$table.find('input[name="selected_packages"]:checked').each(function(){
				var package_number = $(this).val();
				data.packages.push(package_number);
			});

			//Show loading indicator
			vp_woo_pont_mpl_actions.$table.block({
				message: null,
				overlayCSS: {
					background: '#F5F5F5 url(' + vp_woo_pont_params.loading + ') no-repeat center',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			//Make ajax request
			$.post(ajaxurl, data, function(response) {

				//Check for errors
				if(response.success) {
					vp_woo_pont_mpl_actions.reload_page();
				} else {
					vp_woo_pont_mpl_actions.$error.find('p').text(response.data.message);
					vp_woo_pont_mpl_actions.$error.show();
				}

				//Hide loading indicator
				vp_woo_pont_mpl_actions.$table.unblock();

			});
			return false;
		},
	}

	//Init background generate loading indicator
	if($('#vp_woo_pont_mpl_close_shipments').length) {
		vp_woo_pont_mpl_actions.init();
	}

});
