<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'VP_Woo_Pont_Tracking', false ) ) :

	class VP_Woo_Pont_Tracking {
		public $supports_tracking_automations = array('gls', 'dpd', 'foxpost', 'packeta', 'posta');

		//Setup triggers
		public function __construct() {

			//Only of order tracking is enabled
			if(VP_Woo_Pont_Helpers::get_option('order_tracking', 'no') == 'yes') {

				//Runs when label created
				add_action( 'vp_woo_pont_label_created', array($this, 'init_scheduled_action'), 10, 3);

				//Create metabox for labels on order edit page
				add_action( 'add_meta_boxes', array( $this, 'add_metabox' ), 10, 2 );

				//Show tracking info in order admin table
				add_filter( 'manage_edit-shop_order_columns', array( $this, 'add_listing_column' ) );
				add_action( 'manage_shop_order_posts_custom_column', array( $this, 'add_listing_actions' ) );
				add_action( 'admin_footer', array( $this, 'tracking_modal' ) );

			}

			//Runs when tracking info needs to be updated for an order(scheduled action)
			add_action( 'vp_woo_pont_update_tracking_info', array($this, 'update_tracking_info'));

			//Ajax function to reload tracking info
			add_action( 'wp_ajax_vp_woo_pont_update_tracking_info', array( $this, 'ajax_update_tracking_info' ) );

			//Include invoices in emails
			if(VP_Woo_Pont_Helpers::get_option('email_tracking_number')) {
				if(VP_Woo_Pont_Helpers::get_option('email_tracking_number_pos', 'beginning') == 'beginning') {
					add_action('woocommerce_email_before_order_table', array( $this, 'email_attachment'), 10, 4);
					add_action('woocommerce_subscriptions_email_order_details', array( $this, 'email_attachment'), 10, 4);
				} else {
					add_action('woocommerce_email_customer_details', array( $this, 'email_attachment'), 30, 4);
				}
			}

			//Display tracking number in my account
			add_filter( 'woocommerce_get_order_item_totals', array( $this, 'woocommerce_get_order_item_totals' ), 10, 3 );

		}

		//Setup scheduled actions to update tracking number info, only if supported by the provider
		public function init_scheduled_action($order, $label, $provider) {
			if(in_array($provider, $this->supports_tracking_automations)) {
				WC()->queue()->schedule_recurring( time()+HOUR_IN_SECONDS*2, HOUR_IN_SECONDS*2, 'vp_woo_pont_update_tracking_info', array('order_id' => $order->get_id()), 'vp_woo_pont' );
			}
		}

		//Helper function to get all of the tracking statuses
		public function get_supported_tracking_statuses() {
			$statuses = array();

			foreach ($this->supports_tracking_automations as $provider_id) {
				$statuses[$provider_id] = VP_Woo_Pont()->providers[$provider_id]->package_statuses;
			}

			return apply_filters('vp_woo_pont_tracking_status_codes', $statuses);
		}

		//Fetch tracking updates from the tracking number
		public function update_tracking_info($order_id) {
			$order = wc_get_order($order_id);

			//If order not found(maybe deleted), cancel and delete scheduled actions too
			//Also if order exists, but the label was deleted, we can cancel the scheculed action
			if(!$order || ($order && !$order->get_meta('_vp_woo_pont_parcel_number'))) {
				WC()->queue()->cancel('vp_woo_pont_update_tracking_info', array('order_id' => $order_id), 'vp_woo_pont' );
				return false;
			}

			//If its been 2 weeks, cancel updating the tracking number
			$order_date = $order->get_date_created()->getTimestamp();
			$todays_date = time();
			if($todays_date-$order_date > (WEEK_IN_SECONDS*2)) {
				WC()->queue()->cancel('vp_woo_pont_update_tracking_info', array('order_id' => $order_id), 'vp_woo_pont' );
				return false;
			}

			//Otherwise, run the API to get fresh tracking info
			$provider_id = $order->get_meta('_vp_woo_pont_provider');
			if(strpos($provider_id, 'postapont') !== false) $provider_id = 'posta';
			$tracking_info = VP_Woo_Pont()->providers[$provider_id]->get_tracking_info($order);

			//Check for errors
			if(!is_wp_error($tracking_info)) {

				//Update order meta
				$order->update_meta_data('_vp_woo_pont_parcel_info', $tracking_info);
				$order->save();

				//Check if order status needs to be updated
				if($tracking_info && count($tracking_info) > 0) {
					$this->run_tracking_automation($order, $provider_id, $tracking_info[0]);
				}

				//Allow plugins to hook in
				do_action('vp_woo_pont_tracking_info_updated', $tracking_info, $order, $provider_id);

			}

			//Just so the scheduled action knows its the end
			return false;
		}

		//This will check if we need to change order status based on the tracking automation settings
		public function run_tracking_automation($order, $provider, $tracking_info) {

			//Get tracking autoamtions
			$automations = get_option('vp_woo_pont_tracking_automations', array());

			//Loop through automations
			foreach ($automations as $automation) {

				//If its an automation set for the provider and the event is set as an option
				if(
					isset($automation[$provider]) &&
					is_array($automation[$provider]) &&
					in_array($tracking_info['event'], $automation[$provider])) {

						//Change order status
						$target_status = $automation['order_status'];
						$tracking_statuses = $this->get_supported_tracking_statuses();
						$order->update_status($target_status, sprintf(__( 'Order status updated, because of the following tracking status event: %s', 'vp_woo_pont' ), $tracking_statuses[$provider][$tracking_info['event']]));
						$order->save();

				}

			}

		}

		//Ajax function to update tracking data
		public function ajax_update_tracking_info() {

			//Validate nonce
			check_ajax_referer( 'vp_woo_pont_manage', 'nonce' );

			//Gather data
			$order_id = intval($_POST['order']);
			$order = wc_get_order($order_id);
			$provider_id = $order->get_meta('_vp_woo_pont_provider');
			if(strpos($provider_id, 'postapont') !== false) $provider_id = 'posta';

			//Create response object
			$response = array();
			$response['error'] = false;
			$response['messages'] = array();

			//Get tracking info
			$tracking_info = VP_Woo_Pont()->providers[$provider_id]->get_tracking_info($order);
			$parcel_statuses = $this->get_supported_tracking_statuses();

			//Check for errors
			if(is_wp_error($tracking_info)) {
				$response['error'] = true;
				$response['messages'][] = $tracking_info->get_error_message();
				return $response;
			}

			//Return response
			$response['tracking_info'] = $tracking_info;
			$response['messages'][] = __('Tracking info updated successfully', 'vp-woo-pont');

			//Update order meta
			$order->update_meta_data('_vp_woo_pont_parcel_info', $tracking_info);
			$order->save();

			//Get label if needed
			if(!$response['tracking_info']['label']) {
				$response['tracking_info']['label'] = $parcel_statuses[$provider_id][$tracking_info['event']];
			}

			//Check if order status needs to be updated
			$this->run_tracking_automation($order, $provider_id, $tracking_info[0]);

			//Allow plugins to hook in
			do_action('vp_woo_pont_tracking_info_updated', $tracking_info, $order, $provider_id);

			wp_send_json_success($response);
		}

		//Meta box on order page
		public function add_metabox( $post_type, $post ) {
			$order = wc_get_order($post->ID);
			if($order) {
				$provider_id = $order->get_meta('_vp_woo_pont_provider');
				if(strpos($provider_id, 'postapont') !== false) $provider_id = 'posta';
				if(in_array($provider_id, $this->supports_tracking_automations) && $order->get_meta('_vp_woo_pont_parcel_number')) {
					add_meta_box('vp_woo_pont_metabox_tracking', __('Tracking informations', 'vp-woo-pont'), array( $this, 'render_tracking_metabox_content' ), 'shop_order', 'side');
				}
			}
		}

		//Render metabox content
		public function render_tracking_metabox_content($post) {
			$order = wc_get_order($post->ID);
			include( dirname( __FILE__ ) . '/views/html-metabox-tracking.php' );
		}

		//Email attachment
		public function email_attachment($order, $sent_to_admin, $plain_text, $email){
			$order_id = $order->get_id();
			$order = wc_get_order($order_id);

			if(isset($email->id) && is_a( $order, 'WC_Order' ) && VP_Woo_Pont()->labels->is_label_generated($order)) {
				$tracking_url = $this->get_tracking_link($order);
				$tracking_number = $order->get_meta('_vp_woo_pont_parcel_number');
				$tracking_link = '<a target="_blank" href="'.esc_url($tracking_url).'">'.esc_html($tracking_number).'</a>';
				$text = VP_Woo_Pont_Helpers::get_option('email_tracking_number_desc', __('You can track the order by clicking on the tracking number: {tracking_number}', 'vp-woo-pont'));
				$params = array( 'order' => $order, 'tracking_url' => $tracking_url, 'tracking_link' => $tracking_link, 'tracking_number' => $tracking_number, 'tracking_text' => $text );

				if($plain_text) {
					wc_get_template( 'emails/plain/email-vp-woo-pont-section.php', $params, '', plugin_dir_path( __FILE__ ) );
				} else {
					wc_get_template( 'emails/email-vp-woo-pont-section.php', $params, '', plugin_dir_path( __FILE__ ) );
				}
			}
		}

		//Function to get a tracking link URL
		public function get_tracking_link($order) {
			$link = '';

			if(VP_Woo_Pont()->labels->is_label_generated($order)) {
				$provider_id = $order->get_meta('_vp_woo_pont_provider');
				$parcel_number = $order->get_meta('_vp_woo_pont_parcel_number');

				//Fix for postaponts... provider id
				if(strpos($provider_id, 'postapont') !== false) {
					$provider_id = 'posta';
				}

				$link = VP_Woo_Pont()->providers[$provider_id]->get_tracking_link($parcel_number, $order);
			}

			return esc_url($link);
		}

		//Display tracking number and link in my account / orders
		public function woocommerce_get_order_item_totals($total_rows, $order, $tax_display) {

			//Only on my account / view order
			if(!is_wc_endpoint_url( 'view-order' )) return $total_rows;

			//Check if tracking link exists
			$tracking_link = $this->get_tracking_link($order);
			if($tracking_link) {
				$total_rows['vp_woo_pont'] = array(
					'label' => esc_html('Tracking number', 'vp-woo-pont'),
					'value' => '<a href="'.$tracking_link.'" target="_blank">'.$order->get_meta('_vp_woo_pont_parcel_number').'</a>'
				);
			}

			return $total_rows;
		}

		//Column on orders page
		public function add_listing_column($columns) {
			$new_columns = array();
			foreach ($columns as $column_name => $column_info ) {
				$new_columns[ $column_name ] = $column_info;
				if ( 'order_total' === $column_name ) {
					$new_columns['vp_woo_pont'] = __( 'Tracking informations', 'vp-woo-pont' );
				}
			}
			return $new_columns;
		}

		//Add icon to order list to show invoice
		public function add_listing_actions( $column ) {
			global $the_order;
			if ( 'vp_woo_pont' === $column ) {

				//Get the parcel info
				$parcel_statuses = $this->get_supported_tracking_statuses();
				$parcel_info = $the_order->get_meta('_vp_woo_pont_parcel_info');
				$parcel_number = $the_order->get_meta('_vp_woo_pont_parcel_number');
				$tracking_link = $this->get_tracking_link($the_order);

				//If theres no parcel number, return empty
				if(!$parcel_number) return '';

				//Get provider
				$provider_id = $the_order->get_meta('_vp_woo_pont_provider');
				if(strpos($provider_id, 'postapont') !== false) $provider_id = 'posta';

				//Latest event
				$event = false;
				$events = '';
				$label = $parcel_number;
				if(!empty($parcel_info) && count($parcel_info) > 0) {
					$event = $parcel_info[0];
					$events = wp_json_encode( $parcel_info );
					$events = function_exists( 'wc_esc_json' ) ? wc_esc_json( $events ) : _wp_specialchars( $events, ENT_QUOTES, 'UTF-8', true );
				}

				if($event) {
					$label = $event['label'];
					if(!$label) $label = $parcel_statuses[$provider_id][$event['event']];
					if(empty($label)) $label = $parcel_number;
				}

				?>
				<div class="order-status vp-woo-pont-orders-tracking-event">
					<a class="vp-woo-pont-orders-tracking-event-external" target="_blank" href="<?php echo $tracking_link; ?>"><i class="vp-woo-pont-provider-icon-<?php echo esc_attr($provider_id); ?>"></i></a>
					<a class="vp-woo-pont-orders-tracking-event-label" href="#" data-link="<?php echo $tracking_link; ?>" data-events="<?php echo $events; ?>" data-parcel_number="<?php echo $parcel_number; ?>" data-order_id="<?php echo $the_order->get_id(); ?>"><span><?php echo esc_html($label); ?></span></a>
				</div>
				<?php

			}
		}

		public function tracking_modal() {
			global $typenow;
			if ( in_array( $typenow, wc_get_order_types( 'order-meta-boxes' ), true ) ) {
				include( dirname( __FILE__ ) . '/views/html-modal-tracking.php' );
			}
		}

	}

endif;
