<?php

class VP_Woo_Pont_MPL_Shipments_Table extends WP_List_Table {

	public function __construct() {

		parent::__construct(
			array(
				'singular' => __( 'Delivery note', 'vp-woo-pont' ),
				'plural'   => __( 'Delivery notes', 'vp-woo-pont' ),
				'ajax'     => false
			)
		);

	}

	//Get data for the table
	public function get_shipments() {
		global $wpdb;
		$sql = "SELECT * FROM {$wpdb->prefix}vp_woo_pont_mpl_shipments";
		$orderby = 'time';
		$order   = 'DESC';
		$sql .= ' ORDER BY ' . esc_sql( $orderby );
		$sql .= ' ' . esc_sql( $order );
		$result = $wpdb->get_results( $sql, 'ARRAY_A' );
		return $result;
	}

	//Shipment number column
	function column_id( $item ) {
		return $item['id'];
	}

	//Payment amount column
	function column_time( $item ) {
		return $item['time'];
	}

  //Upload date column
  function column_packages( $item ) {
		$packages = json_decode($item['packages']);
		?>
		<ul>
			<?php foreach ($packages as $package): ?>
				<li><?php echo esc_html($package->tracking); ?> - <?php echo wc_price($package->cost); ?></li>
			<?php endforeach; ?>
		</ul>
		<?php
  }

  //Order number column
  function column_orders( $item ) {
		$orders = json_decode($item['orders']);
		?>
		<ul>
			<?php foreach ($orders as $order_id): ?>
				<?php $order = wc_get_order($order_id); ?>
				<?php if($order): ?>
					<li>
						<?php $buyer = $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?>
						<?php echo '<a href="' . esc_url( admin_url( 'post.php?post=' . absint( $order->get_id() ) ) . '&action=edit' ) . '" class="order-view"><strong>#' . esc_attr( $order->get_order_number() ) . ' ' . esc_html( $buyer ) . '</strong></a>'; ?>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ul>
		<?php
	}

  //Invoice number column
  function column_pdf( $item ) {
		$paths = VP_Woo_Pont()->labels->get_pdf_file_path();
		$pdf_file_url = $paths['baseurl'].$item['pdf'];
    return '<a href="'.esc_url($pdf_file_url).'">'.__('Download', 'vp-woo-pont').'</a>';
  }

	//Setup columns
	function get_columns() {
		$columns = array(
			'id' => __( 'ID', 'vp-woo-pont' ),
			'time' => __( 'Uploaded', 'vp-woo-pont' ),
			'orders' => __( 'Orders', 'vp-woo-pont' ),
			'packages' => __( 'Packages', 'vp-woo-pont' ),
			'pdf' => __( 'Delivery note', 'vp-woo-pont' )
		);
		return $columns;
	}

	//Setup items to display
	function prepare_items() {
		$columns = $this->get_columns();
		$hidden = array();

		$per_page = 20;
		$current_page = $this->get_pagenum();
		$data = $this->get_shipments();
		$total_items = count( $data );

		$this->set_pagination_args(
			array(
				'total_items' => $total_items,
				'per_page'    => $per_page,
			)
		);

		$this->_column_headers = array( $columns, $hidden, array() );
		$this->items = array_slice( $data, ( ( $current_page - 1 ) * $per_page ), $per_page );
	}

}

class VP_Woo_Pont_MPL_Pending_Table extends WP_List_Table {

	public function __construct() {

		parent::__construct(
			array(
				'singular' => __( 'Package', 'vp-woo-pont' ),
				'plural'   => __( 'Packages', 'vp-woo-pont' ),
				'ajax'     => false
			)
		);

	}

	//Get data for the table
	public function get_packages() {
		$orders = wc_get_orders( array(
			'limit'        => -1,
			'orderby'      => 'date',
			'order'        => 'DESC',
			'meta_key'     => '_vp_woo_pont_mpl_closed',
			'meta_value' 	 => 'no',
		));
		return $orders;
	}

	//Shipment number column
	function column_id( $order ) {
		$buyer = $order->get_billing_first_name(). ' ' .$order->get_billing_last_name();
		echo '<a href="' . esc_url( admin_url( 'post.php?post=' . absint( $order->get_id() ) ) . '&action=edit' ) . '" class="order-view"><strong>#' . esc_attr( $order->get_order_number() ) . ' ' . esc_html( $buyer ) . '</strong></a>';
	}

	//Billing address column
	function column_billing($order) {
		$address = $order->get_formatted_billing_address();
		echo esc_html( preg_replace( '#<br\s*/?>#i', ', ', $address ) );
		echo '<span class="description">' . esc_html( $order->get_payment_method_title() ) . '</span>';
	}

  //Shipping address column
  function column_shipping( $order ) {
		return $order->get_formatted_shipping_address();
  }

  //Package info column
  function column_tracking( $order ) {
		?>
		<div class="vp-woo-pont-order-column">
			<a target="_blank" href="<?php echo VP_Woo_Pont()->labels->generate_download_link($order); ?>" class="vp-woo-pont-order-column-pdf">
				<i></i>
				<span><?php echo esc_html($order->get_meta('_vp_woo_pont_parcel_id')); ?></span>
			</a>
		</div>
		<?php
  }

	//Checkbox column
	function column_cb( $order ) {
		return '<input type="checkbox" value="'.esc_attr($order->get_meta('_vp_woo_pont_parcel_id')).'" data-order="'.esc_attr($order->get_id()).'" checked name="selected_packages" />';
	}

	//Setup columns
	function get_columns() {
		$columns = array(
			'cb'      => '<input type="checkbox" checked />',
			'id' => __( 'Order number', 'vp-woo-pont' ),
			'billing' => __( 'Billing address', 'vp-woo-pont' ),
			'shipping' => __( 'Shipping address', 'vp-woo-pont' ),
			'tracking' => __( 'Shipping label', 'vp-woo-pont' ),
		);
		return $columns;
	}

	//Setup items to display
	function prepare_items() {
		$columns = $this->get_columns();
		$sortable = $this->get_sortable_columns();
		$hidden = array();

		$per_page = 1000;
		$data = $this->get_packages();
		$orders = array();
		$total_items = count( $data );
		$current_page = 0;

		//Filter out items with deleted labels
		foreach ($data as $order) {
			if($order->get_meta('_vp_woo_pont_parcel_id')) {
				$orders[] = $order;
			}
		}

		$this->set_pagination_args(
			array(
				'total_items' => $total_items,
				'per_page'    => $total_items,
			)
		);

		$this->_column_headers = array( $columns, $hidden, $hidden );
		$this->items = $orders;
	}

}
