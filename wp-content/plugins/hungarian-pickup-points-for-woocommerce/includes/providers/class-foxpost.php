<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class VP_Woo_Pont_Foxpost {
	protected $api_url = 'https://webapi.foxpost.hu/api/';
	protected $username = '';
	protected $password = '';
	protected $api_key = '';
	public $package_statuses = array();

	public function __construct() {
		$this->username = VP_Woo_Pont_Helpers::get_option('foxpost_username');
		$this->password = VP_Woo_Pont_Helpers::get_option('foxpost_password');
		$this->api_key = VP_Woo_Pont_Helpers::get_option('foxpost_api_key');

		//Load settings
		add_filter('vp_woo_pont_global_settings', array($this, 'get_settings'));

		//Set supported statuses
		$this->package_statuses = array(
			"BACKLOGINFAIL" => "BACKLOGINFAIL",
			"BACKLOGINFULL" => "BACKLOGINFULL",
			"BACKTOSENDER" => "BACKTOSENDER",
			"C2BIN" => "C2BIN",
			"C2CIN" => "C2CIN",
			"COLLECTED" => "COLLECTED",
			"COLLECTSENT" => "COLLECTSENT",
			"CREATE" => "CREATE",
			"EMPTYSLOT" => "EMPTYSLOT",
			"HDCOURIER" => "HDCOURIER",
			"HDDEPO" => "HDDEPO",
			"HDHUBIN" => "HDHUBIN",
			"HDHUBOUT" => "HDHUBOUT",
			"HDINTRANSIT" => "HDINTRANSIT",
			"HDRECEIVE" => "HDRECEIVE",
			"HDRETURN" => "HDRETURN",
			"HDSENT" => "HDSENT",
			"HDUNDELIVERABLE" => "HDUNDELIVERABLE",
			"INWAREHOUSE" => "INWAREHOUSE",
			"MISSORT" => "MISSORT",
			"MPSIN" => "MPSIN",
			"OPERIN" => "OPERIN",
			"OPEROUT" => "OPEROUT",
			"OVERTIMED" => "OVERTIMED",
			"OVERTIMEOUT" => "OVERTIMEOUT",
			"PREPAREDFORPD" => "PREPAREDFORPD",
			"RECEIVE" => "RECEIVE",
			"REDIRECT" => "REDIRECT",
			"RESENT" => "RESENT",
			"RETURN" => "RETURN",
			"RETURNED" => "RETURNED",
			"SLOTCHANGE" => "SLOTCHANGE",
			"SORTIN" => "SORTIN",
			"SORTOUT" => "SORTOUT",
			"WBXREDIRECT" => "WBXREDIRECT",
		);

	}

	public function get_settings($settings) {
		$foxpost_settings = array(
			'section_foxpost' => array(
				'title' => __( 'Foxpost settings', 'vp-woo-pont' ),
				'type' => 'vp_woo_pont_settings_title',
				'description' => __( 'Settings related to the Foxpost provider.', 'vp-woo-pont' ),
			),
			'foxpost_username' => array(
				'title' => __('Ecomm code', 'vp-woo-pont'),
				'type' => 'text',
			),
			'foxpost_password' => array(
				'title' => __('Ecomm password', 'vp-woo-pont'),
				'type' => 'text',
			),
			'foxpost_api_key' => array(
				'title' => __('Ecomm API key', 'vp-woo-pont'),
				'type' => 'text',
			),
			'foxpost_parcel_size' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'Parcel size', 'vp-woo-pont' ),
				'default' => 'XS',
				'options' => array(
					'XS' => __( 'XS', 'vp-woo-pont' ),
					'S' => __( 'S', 'vp-woo-pont' ),
					'M' => __( 'M', 'vp-woo-pont' ),
					'L' => __( 'L', 'vp-woo-pont' ),
					'XL' => __( 'XL', 'vp-woo-pont' ),
				),
			),
			'foxpost_sticker_size' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'Sticker size', 'vp-woo-pont' ),
				'default' => 'A7',
				'options' => array(
					'a7' => __( 'A7', 'vp-woo-pont' ),
					'a5' => __( 'A5', 'vp-woo-pont' ),
					'a6' => __( 'A6', 'vp-woo-pont' ),
					'_85X85' => __( '_85X85', 'vp-woo-pont' )
				),
			),
			'foxpost_sticker_merge' => array(
				'title'    => __( 'Merge stickers on a single page', 'vp-woo-pont' ),
				'type'     => 'checkbox',
				'description' => __('If you are using the A7, A6 or the A5 format, this option will merge multiple stickers on a single A4 page during bulk printing & printing.', 'vp-woo-pont')
			),
			'foxpost_package_contents' => array(
				'title' => __('Package contents text', 'vp-woo-pont'),
				'type' => 'textarea',
				'description' => __('You can use the following shortcodes: {order_number}, {order_items}, {customer_note}', 'vp-woo-pont')
			),
		);

		return $settings+$foxpost_settings;
	}

	public function create_label($data) {

		//Create item
		$comment = VP_Woo_Pont()->labels->get_package_contents_label($data, 'foxpost');
		$item = array(
			'comment' => substr($comment,0,50), //50 character limit
			'deliveryNote' => substr($data['order']->get_customer_note(),0,50),
			'orderId' => $data['order_id'],
			'recipientName' => $data['customer']['name'],
			'recipientPhone' => $data['customer']['phone'],
			'recipientEmail' => $data['customer']['email'],
			'size' => strtolower(VP_Woo_Pont_Helpers::get_option('foxpost_parcel_size', 'XS')),
			'refCode' => $data['reference_number']
		);

		//If its a point order
		if($data['point_id']) {
			$item['destination'] = $data['point_id'];
			$item['sendType'] = 'APM';
		}

		//If its home delivery
		if(!$data['point_id']) {
			$order = $data['order'];
			$item['sendType'] = 'HD';
			$item['recipientAddress'] = implode(' ', array($order->get_shipping_address_1(), $order->get_shipping_address_2()));
			$item['recipientCity'] = $order->get_shipping_city();
			$item['recipientZip'] = $order->get_shipping_postcode();
		}

		//Check for COD
		if($data['package']['cod']) {
			$item['cod'] = $data['package']['total'];
		}

		//Create request data
		$options = array($item);

		//So developers can modify
		$options = apply_filters('vp_woo_pont_foxpost_label', $options, $data);
		$auth_header = apply_filters('vp_woo_pont_foxpost_auth_header', 'Basic ' . base64_encode( $this->username . ':' . $this->password ), $options);

		//Logging
		VP_Woo_Pont()->log_debug_messages($options, 'foxpost-create-label');

		//Submit request
		$request = wp_remote_post( $this->api_url.'parcel', array(
			'body'    => json_encode($options),
			'headers' => array(
				'Authorization' => $auth_header,
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'api-key' => $this->api_key
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for HTTP errors
		if(wp_remote_retrieve_response_code( $request ) != 201) {
			VP_Woo_Pont()->log_error_messages($response, 'foxpost-create-label');
			if(isset($response['error'])) {
				return new WP_Error( 'foxpost_error_unknown', $response['error'] );
			} else {
				return new WP_Error( 'foxpost_error_unknown', __('Unknown error', 'vp-woo-pont') );
			}
		}

		//Check for errors
		if(!$response['valid']) {
			VP_Woo_Pont()->log_error_messages($response, 'foxpost-create-label');
			$package = $response['parcels'][0];
			$error_messages = array();
			if(isset($package['errors'])) {
				foreach ($package['errors'] as $fault) {
					$error_messages[] = $fault['message'];
					$error_messages[] = $fault['field'];
				}
			}
			return new WP_Error( 'bad_request', implode('; ', $error_messages) );
		}

		//Else, it was successful
		$parcel_number = $response['parcels'][0]['clFoxId'];

		//Next, generate the PDF label
		$label_size = VP_Woo_Pont_Helpers::get_option('foxpost_sticker_size', 'A5');
		$request = wp_remote_post( $this->api_url.'label/'.strtoupper($label_size), array(
			'body'    => json_encode(array($parcel_number)),
			'headers' => array(
				'Authorization' => $auth_header,
				'Content-Type' => 'application/json',
				'api-key' => $this->api_key
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );

		//Check for errors
		if(wp_remote_retrieve_response_code( $request ) != 200) {
			$response = json_decode( $response, true );
			VP_Woo_Pont()->log_error_messages($response, 'foxpost-download-label');
			return new WP_Error( $response['statusCode'], $response['statusCodeValue'] );
		}

		//Now we have the PDF as base64, save it
		$pdf = $response;

		//Try to save PDF file
		$pdf_file = VP_Woo_Pont_Labels::get_pdf_file_path('foxpost', $data['order_id']);
		VP_Woo_Pont_Labels::save_pdf_file($pdf, $pdf_file);

		//Create response
		$label = array();
		$label['id'] = $parcel_number;
		$label['number'] = $parcel_number;
		$label['pdf'] = $pdf_file['name'];

		//Return file name, package ID, tracking number which will be stored in order meta
		return $label;
	}

	public function void_label($data) {

		//Create request data
		VP_Woo_Pont()->log_debug_messages($data, 'foxpost-void-label-request');

		//So developers can modify
		$options = apply_filters('vp_woo_pont_foxpost_void_label', $data, $data);
		$auth_header = apply_filters('vp_woo_pont_foxpost_auth_header', 'Basic ' . base64_encode( $this->username . ':' . $this->password ), $options);

		//Submit request
		$request = wp_remote_request( $this->api_url.'parcel/'.$options['parcel_number'], array(
			'method' => 'DELETE',
			'headers' => array(
				'Authorization' => $auth_header,
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'api-key' => $this->api_key
			),
		));
		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Check for API errors
		if(wp_remote_retrieve_response_code( $request ) != 204) {
			$response = wp_remote_retrieve_body( $request );
			$response = json_decode( $response, true );

			//If INVALID_PARCEL_ID, that means it was deleted on foxpost already
			if($response['error'] == 'INVALID_PARCEL_ID') {
				$label = array();
				$label['success'] = true;
				return $label;
			} else {
				VP_Woo_Pont()->log_error_messages($response, 'foxpost-delete-label');
				if($response && $response['error']) {
					return new WP_Error( 'foxpost_error_'.$response['status'], $response['error'] );
				} else {
					return new WP_Error( 'foxpost_error_unknown', __('Unknown error', 'vp-woo-pont') );
				}
			}
		}

		//Check for success
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );
		$label = array();
		$label['success'] = true;

		VP_Woo_Pont()->log_debug_messages($response, 'foxpost-void-label-response');

		return $label;
	}

	//Return tracking link
	public function get_tracking_link($parcel_number, $order = false) {
		return 'https://foxpost.hu/csomagkovetes/?code='.esc_attr($parcel_number);
	}

	//Function to get tracking informations using API
	public function get_tracking_info($order) {

		//Parcel number
		$parcel_number = $order->get_meta('_vp_woo_pont_parcel_number');

		//So developers can modify
		$auth_header = apply_filters('vp_woo_pont_foxpost_auth_header', 'Basic ' . base64_encode( $this->username . ':' . $this->password ), $order);

		//Submit request
		$request = wp_remote_request( $this->api_url.'tracking/'.$parcel_number, array(
			'method' => 'GET',
			'headers' => array(
				'Authorization' => $auth_header,
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'api-key' => $this->api_key
			),
		));

		VP_Woo_Pont()->log_debug_messages($request, 'foxpost-get-tracking-info');

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Check for API errors
		if(wp_remote_retrieve_response_code( $request ) != 200) {

			//If INVALID_PARCEL_ID, that means it was deleted on foxpost already
			return new WP_Error( 'foxpost_error_unknown', __('Unknown error', 'vp-woo-pont') );

		}

		//Check for success
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );
		$label = array();
		$label['success'] = true;

		//Check if empty response
		if(empty($response)) {
			return new WP_Error( 'foxpost_error_unknown', __('Unknown error', 'vp-woo-pont') );
		}

		//Collect events
		$tracking_info = array();
		foreach ($response['traces'] as $event) {
			$tracking_info[] = array(
				'date' => strtotime($event['statusDate']),
				'event' => $event['status'],
				'label' => $event['shortName']
			);
		}

		return $tracking_info;
	}

}
