<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class VP_Woo_Pont_Posta {
	protected $api_url = 'https://core.api.posta.hu/';
	protected $api_key = '';
	protected $api_password = '';
	public $package_statuses = array();

	public function __construct() {
		$this->api_key = VP_Woo_Pont_Helpers::get_option('posta_api_key');
		$this->api_password = VP_Woo_Pont_Helpers::get_option('posta_api_password');

		//Load settings
		add_filter('vp_woo_pont_global_settings', array($this, 'get_settings'));

		//If dev mode, use a different API url
		if(VP_Woo_Pont_Helpers::get_option('posta_dev_mode', 'no') == 'yes') {
			$this->api_url = 'https://sandbox.api.posta.hu/';
		}

		//Ajax function to handle shipment closing
		add_action( 'wp_ajax_vp_woo_pont_mpl_close_shipments', array( $this, 'ajax_close_shipments' ) );
		add_action( 'wp_ajax_vp_woo_pont_mpl_close_orders', array( $this, 'ajax_close_orders' ) );

		//Set supported statuses
		$this->package_statuses = array(
			1 => "Postára beszállítás folyamatban",
			2 => "A küldeményt a feladótól átvettük",
			3 => "A küldeményt a feladó előrejelezte, az átadást követően megkezdjük a feldolgozást",
			4 => "A küldemény feldolgozás alatt",
			5 => "A küldemény Csomagautomatából postára szállítva (műszaki hiba miatt)",
			6 => "A küldemény Csomagautomatából postára szállítva",
			7 => "A küldemény Csomagautomatából postára szállítva (lejárt őrzési idő miatt)",
			8 => "A küldemény szállítás alatt",
			9 => "A küldemény nem kézbesíthető (megőrzésre továbbítva)",
			10 => "A küldemény nem kézbesíthető (cég megszűnt)",
			11 => "A küldemény nem kézbesíthető (elköltözött)",
			12 => "A küldemény nem kézbesíthető (átvételt megtagadta)",
			13 => "A küldemény nem kézbesíthető (nincs jogosult átvevő)",
			14 => "A küldemény nem kézbesíthető (hibás vagy hiányos címzés)",
			15 => "A küldemény nem kézbesíthető (ismeretlen címzett)",
			16 => "A küldemény nem kézbesíthető (nem kereste)",
			17 => "Utánküldés új címre (megrendelés alapján)",
			18 => "Továbbítás másik kézbesítő postára (címzetti rendelkezés alapján)",
			19 => "A küldemény nem kézbesíthető (sérülés miatt)",
			20 => "A küldemény nem kézbesíthető (a feladó visszakérte)",
			21 => "A küldemény nem kézbesíthető (címzett és feladó ismeretlen), megőrzésre továbbítva",
			22 => "Ismételt kézbesítésre továbbítás új címre",
			23 => "A küldemény szállítás alatt (ismételt kézbesítésre)",
			24 => "A küldemény nem kézbesíthető (kézbesítés akadályozott)",
			25 => "A küldemény nem kézbesíthető (átvételt megtagadta)",
			26 => "A küldemény postán átvehető",
			27 => "Telefonos egyeztetés címzettel",
			28 => "A küldemény Csomagautomatából átvehető (az sms/email-ben kapott kóddal)",
			29 => "Sikertelen kézbesítés Csomagautomatából",
			30 => "A küldemény szállítás alatt",
			31 => "Másnapi kézbesítésre előkészítve (címzett kérésére)",
			32 => "Sikertelen kézbesítés",
			33 => "A küldemény a kézbesítőnél van",
			34 => "A küldemény PostaPonton 12:00 után átvehető",
			35 => "Sikeresen kézbesítve Csomagautomatából",
			36 => "Árufizetési összeg feladónak kifizetve",
			37 => "Feladónak visszakézbesítve",
			38 => "Sikeresen kézbesítve",
			39 => "Sikeresen kézbesítve háznál",
			40 => "Sikeresen kézbesítve Postahelyen",
			41 => "Sikeres kézbesítés rögzítése belső rendszerben",
			42 => "Sikeresen kézbesítve PostaPonton"
		);
	}

	public function get_settings($settings) {
		$posta_settings = array(
			'section_posta' => array(
				'title' => __( 'Posta settings', 'vp-woo-pont' ),
				'type' => 'vp_woo_pont_settings_title',
				'description' => __( 'Settings related to the Posta provider.', 'vp-woo-pont' ),
			),
			'posta_api_key' => array(
				'title' => __('API key', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __('The developer representing the company can access these APIs on the Developer Portal by selecting the Applications menu item after logging in.', 'vp-woo-pont')
			),
			'posta_api_password' => array(
				'title' => __('API password', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __('The developer representing the company can access these APIs on the Developer Portal by selecting the Applications menu item after logging in.', 'vp-woo-pont')
			),
			'posta_customer_code' => array(
				'title' => __('Customer code', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __('The developer representing the company can access these APIs on the Developer Portal by selecting the Applications menu item after logging in.', 'vp-woo-pont')
			),
			'posta_agreement_code' => array(
				'title' => __('Agreement code', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __('The developer representing the company can access these APIs on the Developer Portal by selecting the Applications menu item after logging in.', 'vp-woo-pont')
			),

			'posta_dev_mode' => array(
				'title'    => __( 'Enable DEV mode', 'vp-woo-pont' ),
				'type'     => 'checkbox'
			),
			'posta_sender_name' => array(
				'title' => __('Sender name', 'vp-woo-pont'),
				'type' => 'text',
			),
			'posta_sender_address' => array(
				'title' => __('Sender address', 'vp-woo-pont'),
				'type' => 'text',
			),
			'posta_sender_city' => array(
				'title' => __('Sender city', 'vp-woo-pont'),
				'type' => 'text',
			),
			'posta_sender_postcode' => array(
				'title' => __('Sender postcode', 'vp-woo-pont'),
				'type' => 'text',
			),
			'posta_sender_phone' => array(
				'title' => __('Sender phone number', 'vp-woo-pont'),
				'type' => 'text',
			),
			'posta_sender_email' => array(
				'title' => __('Sender email address', 'vp-woo-pont'),
				'type' => 'text',
				'default' => get_bloginfo('admin_email')
			),
			'posta_package_contents' => array(
				'title' => __('Package contents text', 'vp-woo-pont'),
				'type' => 'textarea',
				'description' => __('You can use the following shortcodes: {order_number}, {order_items}, {customer_note}', 'vp-woo-pont')
			),
			'posta_sticker_size' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'Sticker size', 'vp-woo-pont' ),
				'default' => 'A5',
				'options' => array(
					'A4' => __( 'A4', 'vp-woo-pont' ),
					'A5' => __( 'A5 at the top of an A4 page', 'vp-woo-pont' ),
					'A5inA4' => __( 'Two A5 landscape on an A4 page', 'vp-woo-pont' ),
					'A5E' => __( 'A5 landscape', 'vp-woo-pont' ),
					'A5E_EXTRA' => __( 'A5 landscape(with comments)', 'vp-woo-pont' ),
					'A5E_STAND' => __( 'A5 portrait', 'vp-woo-pont' ),
					'A6' => __( 'A6', 'vp-woo-pont' ),
					'A6inA4' => __( 'Six A6 on an A4 page', 'vp-woo-pont' ),
				),
			),
			'posta_retention' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'Retention period', 'vp-woo-pont' ),
				'default' => '5',
				'options' => array(
					'0' => __( '0 day', 'vp-woo-pont' ),
					'5' => __( '5 days', 'vp-woo-pont' ),
					'10' => __( '10 days', 'vp-woo-pont' ),
				),
			),
			'posta_size' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'Parcel size', 'vp-woo-pont' ),
				'default' => 'M',
				'options' => array(
					'S' => __( 'S', 'vp-woo-pont' ),
					'M' => __( 'M', 'vp-woo-pont' ),
					'L' => __( 'L', 'vp-woo-pont' ),
				),
			),
			'posta_default_weight' => array(
				'type' => 'text',
				'title' => __( 'Default package weight(g)', 'vp-woo-pont' ),
				'default' => '1000',
				'description' => __('The weight is a required parameter. If it is missing, this value will be used instead. Enter a value in gramms.', 'vp-woo-pont')
			),
			'posta_extra_services' => array(
				'type' => 'multiselect',
				'title' => __( 'Enabled services', 'vp-woo-pont' ),
				'class' => 'wc-enhanced-select',
				'default' => array(),
				'options' => array(
					'K_ENY' => __('Value insurance', 'vp-woo-pont'),
					'K_TOR' => __('Fragile handling', 'vp-woo-pont'),
				)
			),
			'posta_payment_type' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'COD payment handling', 'vp-woo-pont' ),
				'default' => 'M',
				'options' => array(
					'UV_AT' => __( 'Wire transfer', 'vp-woo-pont' ),
					'UV_KP' => __( 'Cash', 'vp-woo-pont' ),
				),
			),
			'posta_payment_number' => array(
				'title' => __('Bank account number for payments', 'vp-woo-pont'),
				'type' => 'text',
				'default' => ''
			)
		);
		return $settings+$posta_settings;
	}

	public function create_label($data) {

		//Set delivery mode
		$deliveryMode = 'HA'; //Home delivrey

		//Get point info, in that case we need to change the delivery mode too
		if($data['point_id']) {
			$point = VP_Woo_Pont()->find_point_info('postapont', $data['point_id']);
			$point_type = $point['group'];
			$deliveryModes = array(
				10 => 'PM',
				20 => 'PP',
				30 => 'CS',
				50 => 'PP',
				70 => 'PP'
			);
			$deliveryMode = $deliveryModes[$point_type];
		}

		//Get package weight in gramms
		if(!$data['package']['weight']) {
			$data['package']['weight'] = (int)VP_Woo_Pont_Helpers::get_option('posta_default_weight', 1000);
		} else {
			$data['package']['weight'] = wc_get_weight($data['package']['weight'], 'g');
		}

		//Create packet data
		$parcel = array(
			'sender' => array(
				'agreement' => VP_Woo_Pont_Helpers::get_option('posta_agreement_code'),
				'accountNo' => VP_Woo_Pont_Helpers::get_option('posta_payment_number'),
				'contact' => array(
					'name' => VP_Woo_Pont_Helpers::get_option('posta_sender_name'),
					'email' => VP_Woo_Pont_Helpers::get_option('posta_sender_email'),
					'phone' => VP_Woo_Pont_Helpers::get_option('posta_sender_phone')
				),
				'address' => array(
					'postCode' => VP_Woo_Pont_Helpers::get_option('posta_sender_postcode'),
					'city' => VP_Woo_Pont_Helpers::get_option('posta_sender_city'),
					'address' => VP_Woo_Pont_Helpers::get_option('posta_sender_address'),
					'remark' => ''
				)
			),
			'orderId' => strval($data['order_id']),
			'webshopId' => $data['reference_number'],
			'developer' => 'vp-woo-pont',
			'labelType' => VP_Woo_Pont_Helpers::get_option('posta_sticker_size', 'A5'),
			'item' => array(
				array(
					'customData1' => '',
					'customData2' => '',
					'weight' => array(
						'value' => $data['package']['weight'],
						'unit' => 'g'
					),
					'size' => VP_Woo_Pont_Helpers::get_option('posta_size', 'M'),
					'services' => array(
						'basic' => 'A_175_UZL',
						'extra' => array(),
						'deliveryMode' => $deliveryMode,
						'value' => $data['package']['total']
					)
				)
			),
			'recipient' => array(
				'contact' => array(
					'name' => $data['customer']['name'],
					'email' => $data['customer']['email'],
					'phone' => $data['customer']['phone']
				)
			),
			'paymentMode' => VP_Woo_Pont_Helpers::get_option('posta_payment_type', 'UV_AT'),
			'packageRetention' => VP_Woo_Pont_Helpers::get_option('posta_retention', '5')
		);

		//If its a pont shipping method
		if($data['point_id']) {
			$parcel['recipient']['address'] = array(
				'postCode' => $point['zip'],
				'city' => $point['city'],
				'address' => $point['addr'],
				'remark' => '',
				'parcelPickupSite' => $point['name']
			);
		}

		//If its home delivery
		if(!$data['point_id']) {
			$order = $data['order'];
			$parcel['recipient']['address'] = array(
				'postCode' => $order->get_shipping_postcode(),
				'city' => $order->get_shipping_city(),
				'address' => $order->get_shipping_address_1(),
				'remark' => $order->get_shipping_address_2().VP_Woo_Pont()->labels->get_package_contents_label($data, 'posta'),
				'countryCode' => $order->get_shipping_country()
			);
		}

		//Check for COD
		if($data['package']['cod']) {
			$parcel['item'][0]['services']['cod'] = $data['package']['total'];
			$parcel['item'][0]['services']['extra'][] = 'K_UVT';
		}

		//Check for extra services
		$enabled_services = VP_Woo_Pont_Helpers::get_option('posta_extra_services', array());
		foreach ($enabled_services as $service) {
			$parcel['item'][0]['services']['extra'][] = $service;
		}

		//Get auth token
		$token = $this->get_access_token();

		//If no auth token, wrong api keys or sometihng like that
		if(is_wp_error($token)) {
			return $token;
		}

		//Logging
		VP_Woo_Pont()->log_debug_messages($parcel, 'posta-create-label');

		//Create request data
		$options = array($parcel);

		//So developers can modify
		$options = apply_filters('vp_woo_pont_posta_label', $options, $data);
		$auth_header = apply_filters('vp_woo_pont_posta_auth_header', 'Bearer ' . $token, $options);

		//Submit request
		$request_params = array(
			'body'    => json_encode($options),
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'X-Request-Id' => wp_generate_uuid4(),
				'X-Accounting-Code' => VP_Woo_Pont_Helpers::get_option('posta_customer_code', ''),
				'Authorization' => $auth_header
			),
		);

		$request = wp_remote_post( $this->api_url.'v2/mplapi/shipments', $request_params);

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//If 401 error, possible just the token is expired, so get a new one and try again just in case
		if(wp_remote_retrieve_response_code( $request ) == 401) {
			$token = $this->get_access_token(true);
			$request_params['headers']['Authorization'] = 'Bearer '.$token;
			$request = wp_remote_post( $this->api_url.'v2/mplapi/shipments', $request_params);

			//Check for errors again
			if(is_wp_error($request)) {
				return $request;
			}
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for API errors
		if(isset($response['fault'])) {
			VP_Woo_Pont()->log_error_messages($response, 'posta-create-label');
			$error = $response['fault'];
			return new WP_Error( 'posta_error_'.$error['detail']['errorcode'], $error['faultstring'] );
		}

		//Check for package errors
		$parcel_data = $response[0];
		if($parcel_data && isset($parcel_data['errors']) && count($parcel_data['errors']) > 0) {
			$error = $parcel_data['errors'][0]['text'];
			return new WP_Error( 'posta_error_'.$parcel_data['errors'][0]['code'], $parcel_data['errors'][0]['text'] );
		}

		//Get PDF label
		$attachment = $parcel_data['label'];
		$pdf = base64_decode($attachment);

		//Try to save PDF file
		$pdf_file = VP_Woo_Pont_Labels::get_pdf_file_path('posta', $data['order_id']);
		VP_Woo_Pont_Labels::save_pdf_file($pdf, $pdf_file);

		//Create response
		$label = array();
		$label['id'] = $parcel_data['trackingNumber'];
		$label['number'] = $parcel_data['trackingNumber'];
		$label['pdf'] = $pdf_file['name'];
		$label['mpl'] = true;

		//Return file name, package ID, tracking number which will be stored in order meta
		return $label;
	}

	public function void_label($data) {

		//Get auth token
		$token = $this->get_access_token();

		//If no auth token, wrong api keys or sometihng like that
		if(is_wp_error($token)) {
			return $token;
		}

		//So developers can modify
		$auth_header = apply_filters('vp_woo_pont_posta_auth_header', 'Bearer ' . $token, $data);

		//Submit request
		$request = wp_remote_request( $this->api_url.'v2/mplapi/shipments/'.$data['parcel_number'], array(
			'method' => 'DELETE',
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'X-Request-Id' => wp_generate_uuid4(),
				'X-Accounting-Code' => VP_Woo_Pont_Helpers::get_option('posta_customer_code', ''),
				'Authorization' => $auth_header
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		VP_Woo_Pont()->log_error_messages($request, 'posta-void-label');

		//Check for API errors
		if(wp_remote_retrieve_response_code( $request ) != 200) {
			$response = wp_remote_retrieve_body( $request );
			$response = json_decode( $response, true );
			VP_Woo_Pont()->log_error_messages($response, 'posta-delete-label');

			if(isset($response['fault'])) {
				$error = $response['fault'];
				return new WP_Error( 'posta_error_'.$error['detail']['errorcode'], $error['faultstring'] );
			} else {
				return new WP_Error( 'posta_error_unknown', __('Unknown error', 'vp-woo-pont') );
			}
		}

		//Check for success
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );
		$label = array();
		$label['success'] = true;

		VP_Woo_Pont()->log_debug_messages($response, 'posta-void-label-response');

		return $label;
	}

	public function close_shipments($packages = array()) {
		global $wpdb;

		//Get auth token
		$token = $this->get_access_token();

		//If no auth token, wrong api keys or sometihng like that
		if(is_wp_error($token)) {
			return $token;
		}

		//So developers can modify
		$auth_header = apply_filters('vp_woo_pont_posta_auth_header', 'Bearer ' . $token, $data);

		//Set package numbers if needed
		$options = [];
		if(!empty($packages)) {
			$options['trackingNumbers'] = $packages;
		}

		//Close shipments
		$request = wp_remote_post( $this->api_url.'v2/mplapi/shipments/close', array(
			'body'    => json_encode($options),
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'X-Request-Id' => wp_generate_uuid4(),
				'X-Accounting-Code' => VP_Woo_Pont_Helpers::get_option('posta_customer_code', ''),
				'Authorization' => $auth_header
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Log if development mode enabled
		VP_Woo_Pont()->log_debug_messages($request, 'posta-close-shipments');

		//Check for API errors
		if(wp_remote_retrieve_response_code( $request ) != 200) {
			$response = wp_remote_retrieve_body( $request );
			$response = json_decode( $response, true );
			VP_Woo_Pont()->log_error_messages($response, 'posta-close-shipments');

			if(isset($response['fault'])) {
				$error = $response['fault'];
				return new WP_Error( 'posta_error_'.$error['detail']['errorcode'], $error['faultstring'] );
			} else {
				return new WP_Error( 'posta_error_unknown', __('Unknown error', 'vp-woo-pont') );
			}
		}

		//Check for success
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );
		$response = $response[0];

		//Check for more errors
		if(!isset($response['manifest']) || !isset($response['trackingNrPrices'])) {
			return new WP_Error( 'posta_error_unknown', __('Unknown error', 'vp-woo-pont') );
		}

		if((isset($response['manifest']) && empty($response['manifest'])) || (isset($response['trackingNrPrices']) && empty($response['trackingNrPrices']))) {
			return new WP_Error( 'posta_error_unknown', __('Unknown error', 'vp-woo-pont') );
		}

		//Save manifest PDF
		$attachment = $response['manifest'];
		$pdf = base64_decode($attachment);

		//Try to save PDF file
		$pdf_file = VP_Woo_Pont_Labels::get_pdf_file_path('mpl-manifest', 0);
		VP_Woo_Pont_Labels::save_pdf_file($pdf, $pdf_file);

		//Mark the closing on the orders
		$order_ids = array();
		$orders = array();
		$shipments = array();
		foreach ($response['trackingNrPrices'] as $package) {
			$shipments[] = array(
				'tracking' => $package['trackingNumber'],
				'cost' => $package['price']
			);

			$order = wc_get_orders( array(
				'limit'        => 1,
				'orderby'      => 'date',
				'order'        => 'DESC',
				'meta_key'     => '_vp_woo_pont_parcel_number',
				'meta_value' 	 => substr($package['trackingNumber'], 0, 13),
			));

			if(!empty($order)) {
				$order = $order[0];
				$orders[] = $order;
				$order_ids[] = $order->get_id();
			}

		}

		//Save stuff to custom database
		$mpl_shipment = array(
			'packages' => json_encode($shipments),
			'orders' => json_encode($order_ids),
			'pdf' => $pdf_file['name'],
			'time' => current_time( 'mysql' )
		);

		//Save to db
		$table_name = $wpdb->prefix . 'vp_woo_pont_mpl_shipments';
		$wpdb->insert($table_name, $mpl_shipment);
		$shipment_id = $wpdb->insert_id;

		//Update orders
		foreach ($orders as $order) {
			$order->update_meta_data('_vp_woo_pont_mpl_closed', $shipment_id);
			$order->add_order_note(sprintf(esc_html__('MPL shipment closed. Generated delivery note ID: %s', 'vp-woo-pont'), $shipment_id));
			$order->save();
		}

		//Success
		return true;
	}

	public function get_access_token($refresh = false) {
		$access_token = get_transient( '_vp_woo_pont_posta_access_token' );
		if(!$access_token || $refresh) {
			$access_token = false; //returns nothing on error
			$key = base64_encode( $this->api_key.':'.$this->api_password );
			$request = wp_remote_post($this->api_url.'oauth2/token', array(
				'headers' => array(
					'Authorization' => 'Basic ' . $key,
					'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8'
				),
				'body' => 'grant_type=client_credentials',
				'httpversion' => '1.1'
			));

			if(is_wp_error($request)) {
				$this->log_error_messages($request, 'posta-auth');
				return $request;
			} else {
				$response = json_decode( wp_remote_retrieve_body( $request ) );
				$access_token = $response->access_token;
				set_transient( '_vp_woo_pont_posta_access_token', $access_token, $response->expires_in );
			}
		}

		return $access_token;
	}

	//Return tracking link
	public function get_tracking_link($parcel_number, $order = false) {
		return 'https://www.posta.hu/nyomkovetes/nyitooldal?searchvalue='.esc_attr($parcel_number);
	}

	//Ajax function to close shipments
	public function ajax_close_shipments() {

		//Security check
		check_ajax_referer( 'vp-woo-pont-mpl-close-shipments', 'nonce' );

		//Get selected packages
		$selected = array();
		if(isset($_POST['packages'])) {
			foreach ($_POST['packages'] as $package) {
				$selected[] = sanitize_text_field($package);
			}
		}

		//Run closing
		$results = $this->close_shipments($selected);

		//Check for errors
		if(is_wp_error($results)) {
			wp_send_json_error(array('message' => $results->get_error_message()));
		}

		//Success
		wp_send_json_success();

	}

	//Ajax function to close shipments
	public function ajax_close_orders() {

		//Security check
		check_ajax_referer( 'vp-woo-pont-mpl-close-shipments', 'nonce' );

		//Get selected packages
		$selected = array();
		if(isset($_POST['orders'])) {
			foreach ($_POST['orders'] as $order_id) {
				$order_id = sanitize_text_field($order_id);
				$order = wc_get_order($order_id);
				$order->update_meta_data('_vp_woo_pont_mpl_closed', 'yes');
				$order->save();
			}
		}

		//Success
		wp_send_json_success();

	}

	//Function to get tracking informations using API
	public function get_tracking_info($order) {

		//Parcel number
		$parcel_id = $order->get_meta('_vp_woo_pont_parcel_id');

		//Get auth token
		$token = $this->get_access_token();

		//If no auth token, wrong api keys or sometihng like that
		if(is_wp_error($token)) {
			return $token;
		}

		//So developers can modify
		$auth_header = apply_filters('vp_woo_pont_posta_auth_header', 'Bearer ' . $token, $order);

		//Create parameters
		$options = array(
			'language' => 'hu',
			'ids' => $parcel_id,
			'state' => 'all'
		);

		//Setup request
		$request_params = array(
			'body'    => json_encode($options),
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'X-Request-Id' => wp_generate_uuid4(),
				'X-Accounting-Code' => VP_Woo_Pont_Helpers::get_option('posta_customer_code', ''),
				'Authorization' => $auth_header
			),
		);

		//Run request
		$request = wp_remote_post( $this->api_url.'v2/nyomkovetes/registered', $request_params);

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Check for API errors
		if(wp_remote_retrieve_response_code( $request ) != 200) {
			return new WP_Error( 'posta_error_unknown', __('Unknown error', 'vp-woo-pont') );
		}

		//Check for success
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		$events = array();
		foreach ($response['trackAndTrace'] as $event) {
			$date = strtotime($event['c11'].' '.$event['c12']);
			$label = $event['c9'];
			$events[$date] = array(
				'date' => $date,
				'label' => $label
			);
		}

		//Sort by keys
		krsort($events);

		//Create new array for status info
		$tracking_info = array();
		foreach ($events as $event) {
			$event_id = array_search($event['label'], $this->package_statuses);
			if($event_id) {
				$tracking_info[] = array(
					'date' => $event['date'],
					'event' => array_search($event['label'], $this->package_statuses),
				);
			}
		}

		return $tracking_info;
	}

}
