<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class VP_Woo_Pont_GLS {
	protected $api_url = 'https://api.mygls.hu/ParcelService.svc/json/';
	protected $api_username = '';
	protected $api_password = '';
	protected $api_client_number = '';
	public $package_statuses = array();

	public function __construct() {
		$this->api_username = VP_Woo_Pont_Helpers::get_option('gls_username');
		$this->api_password = VP_Woo_Pont_Helpers::get_option('gls_password');
		$this->api_client_number = VP_Woo_Pont_Helpers::get_option('gls_client_id');

		if(VP_Woo_Pont_Helpers::get_option('gls_dev_mode', 'no') == 'yes') {
			$this->api_url = 'https://api.test.mygls.hu/ParcelService.svc/json/';
		}

		//Set country for api url
		$country = VP_Woo_Pont_Helpers::get_option('gls_sender_country', 'HU');
		$domain = strtolower($country);
		$this->api_url = str_replace('.hu', '.'.$domain, $this->api_url);

		//Load settings
		add_filter('vp_woo_pont_global_settings', array($this, 'get_settings'));

		//Set supported statuses
		$this->package_statuses = array(
			"1" => "The parcel was handed over to GLS.",
			"2" => "The parcel has left the parcel center.",
			"3" => "The parcel has reached the parcel center.",
			"4" => "The parcel is expected to be delivered during the day.",
			"5" => "The parcel has been delivered.",
			"6" => "The parcel is stored in the parcel center.",
			"7" => "The parcel is stored in the parcel center.",
			"8" => "The parcel is stored in the GLS parcel center. The consignee has agreed to collect the goods himself.",
			"9" => "The parcel is stored in the parcel center to be delivered at a new delivery date.",
			"10" => "Check scan normal",
			"11" => "The parcel could not be delivered as the consignee is on holidays.",
			"12" => "The parcel could not be delivered as the consignee was absent.",
			"13" => "Sorting error at the depot.",
			"14" => "The parcel could not be delivered as the reception was closed.",
			"15" => "Not delivered lack of time",
			"16" => "The parcel could not be delivered as the consignee had no cash available/suitable.",
			"17" => "The parcel could not be delivered as the recipient refused acceptance.",
			"18" => "The parcel could not be delivered as further address information is needed.",
			"19" => "The parcel could not be delivered due to the weather condition.",
			"20" => "The parcel could not be delivered due to wrong or incomplete address.",
			"21" => "Forwarded sorting error",
			"22" => "Parcel is sent from the depot to sorting center.",
			"23" => "The parcel has been returned to sender.",
			"24" => "The changed delivery option has been saved in the GLS system and will be implemented as requested.",
			"25" => "Forwarded misrouted",
			"26" => "The parcel has reached the parcel center.",
			"27" => "The parcel has reached the parcel center.",
			"28" => "Disposed",
			"29" => "Parcel is under investigation.",
			"30" => "Inbound damaged",
			"31" => "Parcel was completely damaged.",
			"32" => "The parcel will be delivered in the evening.",
			"33" => "The parcel could not be delivered due to exceeded time frame.",
			"34" => "The parcel could not be delivered as acceptance has been refused due to delayed delivery.",
			"35" => "Parcel was refused because the goods was not ordered.",
			"36" => "Consignee was not in, contact card couldn't be left.",
			"37" => "Change delivery for shipper's request.",
			"38" => "The parcel could not be delivered due to missing delivery note.",
			"39" => "Delivery note not signed",
			"40" => "The parcel has been returned to sender.",
			"41" => "Forwarded normal",
			"42" => "The parcel was disposed upon shipper's request.",
			"43" => "Parcel is not to locate.",
			"44" => "Parcel is excluded from General Terms and Conditions.",
			"46" => "Change completed for Delivery address",
			"47" => "The parcel has left the parcel center.",
			"51" => "The parcel data was entered into the GLS IT system; the parcel was not yet handed over to GLS.",
			"52" => "The COD data was entered into the GLS IT system.",
			"54" => "The parcel has been delivered to the parcel box.",
			"55" => "The parcel has been delivered at the ParcelShop (see ParcelShop information).",
			"56" => "Parcel is stored in GLS ParcelShop.",
			"57" => "The parcel has reached the maximum storage time in the ParcelShop.",
			"58" => "The parcel has been delivered at the neighbour’s (see signature)",
			"60" => "Customs clearance is delayed due to a missing invoice.",
			"61" => "The customs documents are being prepared.",
			"62" => "Customs clearance is delayed as the consignee's phone number is not available.",
			"64" => "The parcel was released by customs.",
			"65" => "The parcel was released by customs. Customs clearance is carried out by the consignee.",
			"66" => "Customs clearance is delayed until the consignee's approval is available.",
			"67" => "The customs documents are being prepared.",
			"68" => "The parcel could not be delivered as the consignee refused to pay charges.",
			"69" => "The parcel is stored in the parcel center. It cannot be delivered as the consignment is not complete.",
			"70" => "Customs clearance is delayed due to incomplete documents.",
			"71" => "Customs clearance is delayed due to missing or inaccurate customs documents.",
			"72" => "Customs data must be recorded.",
			"73" => "Customs parcel locked in origin country.",
			"74" => "Customs clearance is delayed due to a customs inspection.",
			"75" => "Parcel was confiscated by the Customs authorities.",
			"76" => "Customs data recorded, parcel can be sent do final location.",
			"80" => "The parcel has been forwarded to the desired address to be delivered there.",
			"83" => "The parcel data for Pickup-Service was entered into the GLS system.",
			"84" => "The parcel label for the pickup has been produced.",
			"85" => "The driver has received the order to pick up the parcel during the day.",
			"86" => "The parcel has reached the parcel center.",
			"87" => "The pickup request has been cancelled as there were no goods to be picked up.",
			"88" => "The parcel could not be picked up as the goods to be picked up were not packed.",
			"89" => "The parcel could not be picked up as the customer was not informed about the pickup.",
			"90" => "The pickup request has been cancelled as the goods were sent by other means.",
			"91" => "Pick and Ship/Return cancelled",
			"92" => "The parcel has been delivered.",
			"93" => "Signature confirmed",
			"99" => "Consignee contacted Email delivery notification"
		);

	}

	public function get_settings($settings) {
		$gls_settings = array(
			'section_gls' => array(
				'title' => __( 'GLS settings', 'vp-woo-pont' ),
				'type' => 'vp_woo_pont_settings_title',
				'description' => __( 'Settings related to the GLS provider.', 'vp-woo-pont' ),
			),
			'gls_username' => array(
				'title' => __('GLS Username', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __("Enter your GLS account's e-mail address.", 'vp-woo-pont')
			),
			'gls_password' => array(
				'title' => __('GLS Password', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __("Enter your GLS account's password.", 'vp-woo-pont')
			),
			'gls_client_id' => array(
				'title' => __('GLS Customer Number', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __("Enter your GLS customer number.", 'vp-woo-pont')
			),
			'gls_dev_mode' => array(
				'title'    => __( 'Enable DEV mode', 'vp-woo-pont' ),
				'type'     => 'checkbox'
			),
			'gls_sender_name' => array(
				'title' => __('Sender name', 'vp-woo-pont'),
				'type' => 'text',
			),
			'gls_sender_street' => array(
				'title' => __('Sender address(street)', 'vp-woo-pont'),
				'type' => 'text',
			),
			'gls_sender_address' => array(
				'title' => __('Sender address(just the number)', 'vp-woo-pont'),
				'type' => 'text',
			),
			'gls_sender_address_2' => array(
				'title' => __('Sender address info(building, stairway, etc...)', 'vp-woo-pont'),
				'type' => 'text',
			),
			'gls_sender_city' => array(
				'title' => __('Sender city', 'vp-woo-pont'),
				'type' => 'text',
			),
			'gls_sender_postcode' => array(
				'title' => __('Sender postcode', 'vp-woo-pont'),
				'type' => 'text',
			),
			'gls_sender_phone' => array(
				'title' => __('Sender phone number', 'vp-woo-pont'),
				'type' => 'text',
			),
			'gls_sender_email' => array(
				'title' => __('Sender email address', 'vp-woo-pont'),
				'type' => 'text',
				'default' => get_bloginfo('admin_email')
			),
			'gls_sender_country' => array(
				'title' => __('Sender country', 'vp-woo-pont'),
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'default' => 'HU',
				'options' => array(
					'HU' => __( 'Hungary', 'vp-woo-pont' ),
					'RO' => __( 'Romania', 'vp-woo-pont' ),
					'HR' => __( 'Croatia', 'vp-woo-pont' ),
					'CZ' => __( 'Czechia', 'vp-woo-pont' ),
					'SI' => __( 'Slovenia', 'vp-woo-pont' ),
					'SK' => __( 'Slovakia', 'vp-woo-pont' ),
				),
			),
			'gls_package_contents' => array(
				'title' => __('Package contents text', 'vp-woo-pont'),
				'type' => 'textarea',
				'description' => __('You can use the following shortcodes: {order_number}, {order_items}, {customer_note}', 'vp-woo-pont')
			),
			'gls_sticker_size' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'Sticker size', 'vp-woo-pont' ),
				'default' => 'A4_2x2',
				'options' => array(
					'A4_2x2' => __( 'A4_2x2', 'vp-woo-pont' ),
					'A4_4x1' => __( 'A4_4x1', 'vp-woo-pont' ),
					'Connect' => __( 'Connect', 'vp-woo-pont' ),
					'Thermo' => __( 'Thermo', 'vp-woo-pont' ),
				),
			),
			'gls_sticker_merge' => array(
				'title'    => __( 'Merge stickers on a single page', 'vp-woo-pont' ),
				'type'     => 'checkbox',
				'description' => __('If you are using the A4_2x2 or the A4_4x1 format, this option will merge multiple stickers on a single A4 page during bulk printing & printing.', 'vp-woo-pont')
			),
			'gls_extra_services' => array(
				'type' => 'multiselect',
				'title' => __( 'Enabled services for home delivery', 'vp-woo-pont' ),
				'class' => 'wc-enhanced-select',
				'default' => array(),
				'options' => array(
					'CS1' => __('Contact Service (CS1)', 'vp-woo-pont'),
					'FDS' => __('FlexDelivery Service (FDS)', 'vp-woo-pont'),
					'FSS' => __('FlexDelivery SMS Service (FSS)', 'vp-woo-pont'),
					'SM2' => __('Preadvice Service (SM2)', 'vp-woo-pont'),
					'SM1' => __('SMS Service (SM1)', 'vp-woo-pont'),
				)
			),
			'gls_extra_services_points' => array(
				'type' => 'multiselect',
				'title' => __( 'Enabled services for point delivery', 'vp-woo-pont' ),
				'class' => 'wc-enhanced-select',
				'default' => array(),
				'options' => array(
					'CS1' => __('Contact Service (CS1)', 'vp-woo-pont'),
					'FDS' => __('FlexDelivery Service (FDS)', 'vp-woo-pont'),
					'FSS' => __('FlexDelivery SMS Service (FSS)', 'vp-woo-pont'),
					'SM2' => __('Preadvice Service (SM2)', 'vp-woo-pont'),
					'SM1' => __('SMS Service (SM1)', 'vp-woo-pont'),
				)
			),
			'gls_sm1_text' => array(
				'title' => __('SMS text for the SMS Service (SM1)', 'vp-woo-pont'),
				'type' => 'textarea',
				'description' => __('You can use the following shortcodes: #ParcelNr#, #COD#, #PickupDate#, #From_Name#, #ClientRef#.', 'vp-woo-pont')
			),
			'gls_hide_parcellocker' => array(
				'title' => __('Hide parcel lockers', 'vp-woo-pont'),
				'type'     => 'checkbox',
				'description' => __('Hide GLS ParcelLockers, so customers can only order to pickup points.', 'vp-woo-pont')
			)
		);

		return $settings+$gls_settings;
	}

	public function create_label($data) {

		//Create packet data
		$parcel = array(
			'ClientNumber' => intval($this->api_client_number),
			'ClientReference' => $data['reference_number'],
			'Content' => VP_Woo_Pont()->labels->get_package_contents_label($data, 'gls'),
			'PickupAddress' => array(
				'Name' => VP_Woo_Pont_Helpers::get_option('gls_sender_name'),
				'Street' => VP_Woo_Pont_Helpers::get_option('gls_sender_street'),
				'HouseNumber' => VP_Woo_Pont_Helpers::get_option('gls_sender_address'),
				'HouseNumberInfo' => VP_Woo_Pont_Helpers::get_option('gls_sender_address_2'),
				'City' => VP_Woo_Pont_Helpers::get_option('gls_sender_city'),
				'ZipCode' => VP_Woo_Pont_Helpers::get_option('gls_sender_postcode'),
				'CountryIsoCode' => VP_Woo_Pont_Helpers::get_option('gls_sender_country', 'HU'),
				'ContactName' => VP_Woo_Pont_Helpers::get_option('gls_sender_name'),
				'ContactPhone' => VP_Woo_Pont_Helpers::get_option('gls_sender_phone'),
				'ContactEmail' => VP_Woo_Pont_Helpers::get_option('gls_sender_email')
			),
			'ServiceList' => array(),
			'DeliveryAddress' => array(
				'ContactName' => $data['customer']['name'],
				'ContactPhone' => $data['customer']['phone'],
				'ContactEmail' => $data['customer']['email']
			)
		);

		//If its a pont shipping method
		if($data['point_id']) {
			$parcel['ServiceList'][] = array(
				'Code' => 'PSD',
				'PSDParameter' => array(
					'StringValue' => $data['point_id']
				)
			);
		}

		//If package count set
		if($data['options']['package_count'] && $data['options']['package_count'] > 1) {
			$parcel['Count'] = $data['options']['package_count'];
		}

		//If pickup date set
		if($data['options']['pickup_date'] && $data['options']['pickup_date'] != '') {
			$datetime = DateTime::createFromFormat('Y-m-d', $data['options']['pickup_date']);
			$parcel['PickupDate'] = '/Date(' . $datetime->format('UvO') . ')/';
		}

		//If its home delivery, define shipping address too
		if(!$data['point_id']) {
			$order = wc_get_order($data['order_id']);
			$parcel['DeliveryAddress']['Name'] = $data['customer']['name_with_company'];
			$parcel['DeliveryAddress']['Street'] = $order->get_shipping_address_1();
			$parcel['DeliveryAddress']['HouseNumberInfo'] = $order->get_shipping_address_2();
			$parcel['DeliveryAddress']['City'] = $order->get_shipping_city();
			$parcel['DeliveryAddress']['ZipCode'] = $order->get_shipping_postcode();
			$parcel['DeliveryAddress']['CountryIsoCode'] = $order->get_shipping_country();
		}

		//Check for COD
		if($data['package']['cod']) {
			$parcel['CODAmount'] = $data['package']['total'];
			$parcel['CODReference'] = $data['order_number'];
		}

		//Check for extra services
		$enabled_services = VP_Woo_Pont_Helpers::get_option('gls_extra_services', array());
		if($data['point_id']) {
			$enabled_services = VP_Woo_Pont_Helpers::get_option('gls_extra_services_points', array());
		}

		foreach ($enabled_services as $service_id) {
			$value = false;

			if($service_id == 'CS1') {
				$value = $data['customer']['phone'];
			}

			if($service_id == 'FDS') {
				$value = $data['customer']['email'];
			}

			if($service_id == 'FSS') {
				$value = $data['customer']['phone'];
			}

			if($service_id == 'SM1') {
				$value = $data['customer']['phone'].'|'.VP_Woo_Pont_Helpers::get_option('gls_sm1_text', '');
			}

			if($service_id == 'SM2') {
				$value = $data['customer']['phone'];
			}

			if($value) {
				$parcel['ServiceList'][] = array(
					'Code' => $service_id,
					$service_id.'Parameter' => array(
						'Value' => $value
					)
				);
			}
		}

		//Create request data
		$options = array(
			'Username' => $this->api_username,
			'Password' => array_values(unpack('C*', hash('sha512', $this->api_password, true))),
			'ParcelList' => array($parcel),
			'PrintPosition' => 1,
			'ShowPrintDialog' => 0,
			'TypeOfPrinter' => VP_Woo_Pont_Helpers::get_option('gls_sticker_size', 'A4_2x2')
		);

		//Logging
		VP_Woo_Pont()->log_debug_messages($options, 'gls-create-label');

		//Submit request
		$request = wp_remote_post( $this->api_url.'PrintLabels', array(
			'body'    => json_encode($options),
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for API errors
		if(count($response['PrintLabelsErrorList']) > 0) {
			VP_Woo_Pont()->log_error_messages($response, 'gls-create-label');
			$error = $response['PrintLabelsErrorList'][0];
			return new WP_Error( 'gls_error_'.$error['ErrorCode'], $error['ErrorDescription'] );
		}

		//Get PDF file
		$pdf = implode(array_map('chr', $response['Labels']));

		//Try to save PDF file
		$pdf_file = VP_Woo_Pont_Labels::get_pdf_file_path('gls', $data['order_id']);
		VP_Woo_Pont_Labels::save_pdf_file($pdf, $pdf_file);

		//Create response
		$parcel_data = $response['PrintLabelsInfoList'][0];
		$label = array();
		$label['id'] = $parcel_data['ParcelId'];
		$label['number'] = $parcel_data['ParcelNumber'];
		$label['pdf'] = $pdf_file['name'];

		//Return file name, package ID, tracking number which will be stored in order meta
		return $label;
	}

	public function void_label($data) {

		//Create request data
		$options = array(
			'Username' => $this->api_username,
			'Password' => array_values(unpack('C*', hash('sha512', $this->api_password, true))),
			'ParcelIdList' => array($data['parcel_id'])
		);

		//Submit request
		$request = wp_remote_post( $this->api_url.'DeleteLabels', array(
			'body'    => json_encode($options),
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for API errors
		if(count($response['DeleteLabelsErrorList']) > 0) {
			$error = $response['DeleteLabelsErrorList'][0];

			//If the label does not exist in GLS system, we can delete it from WC too
			if($error['ErrorCode'] == 4) {
				return array('success' => true);
			} else {
				VP_Woo_Pont()->log_error_messages($response, 'gls-delete-label');
				return new WP_Error( 'gls_error_'.$error['ErrorCode'], $error['ErrorDescription'] );
			}

		}

		//Check for success
		$label = array();
		$label['success'] = true;

		return $label;
	}

	public function download_labels($data) {

		//Create request data
		$options = array(
			'Username' => $this->api_username,
			'Password' => array_values(unpack('C*', hash('sha512', $this->api_password, true))),
			'ParcelIdList' => $data['parcel_ids'],
			'PrintPosition' => 1,
			'ShowPrintDialog' => 0,
			'TypeOfPrinter' => VP_Woo_Pont_Helpers::get_option('gls_sticker_size', 'A4_2x2')
		);

		//Logging
		VP_Woo_Pont()->log_debug_messages($options, 'gls-download-label');

		//Submit request
		$request = wp_remote_post( $this->api_url.'GetPrintedLabels', array(
			'body'    => json_encode($options),
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for API errors
		if(count($response['GetPrintedLabelsErrorList']) > 0) {
			VP_Woo_Pont()->log_error_messages($response, 'gls-download-label');
			$error = $response['PrintLabelsErrorList'][0];
			return new WP_Error( 'gls_error_'.$error['ErrorCode'], $error['ErrorDescription'] );
		}

		//Get PDF file
		$pdf = implode(array_map('chr', $response['Labels']));

		//Try to save PDF file
		$pdf_file = VP_Woo_Pont_Labels::get_pdf_file_path('gls', 0);
		VP_Woo_Pont_Labels::save_pdf_file($pdf, $pdf_file);

		//Create response
		$label = array();
		$label['pdf'] = $pdf_file['path'];

		//Return file name
		return $label;

	}

	//Return tracking link
	public function get_tracking_link($parcel_number, $order = false) {
		$country = VP_Woo_Pont_Helpers::get_option('gls_sender_country', 'HU');
		if($country == 'HU') {
			return 'https://gls-group.eu/HU/hu/csomagkovetes.html?match='.esc_attr($parcel_number);
		} else {
			return 'https://gls-group.eu/'.$country.'/en/parcel-tracking?match='.esc_attr($parcel_number);
		}
	}

	//Replace placeholder in shipping label conents string
	public function get_package_contents_label($data, $provider) {

		//Get order
		$order = $data['order'];
		$note = VP_Woo_Pont_Helpers::get_option('gls_package_contents', '');
		$order_items = $order->get_items();
		$order_items_strings = array();

		//Setup order items
		foreach( $order_items as $order_item ) {
			$order_item_string = $order_item->get_quantity().'x '.$order_item->get_name();
			if($order_item->get_product()) {
				$product = $order_item->get_product();
				if($product->get_sku()) {
					$order_item_string .= ' ('.$product->get_sku().')';
				}
			}
			$order_items_strings[] = $order_item_string;
		}

		//Setup replacements
		$note_replacements = apply_filters('vp_woo_pont_gls_label_placeholders', array(
			'{order_number}' => $order->get_order_number(),
			'{customer_note}' => $order->get_customer_note(),
			'{order_items}' => implode(', ', $order_items_strings)
		), $order, $data);

		//Replace stuff:
		$note = str_replace( array_keys( $note_replacements ), array_values( $note_replacements ), $note);

		return $note;
	}

	//Function to get tracking informations using API
	public function get_tracking_info($order) {

		//Parcel number
		$parcel_number = $order->get_meta('_vp_woo_pont_parcel_number');

		//Create request data
		$options = array(
			'Username' => $this->api_username,
			'Password' => array_values(unpack('C*', hash('sha512', $this->api_password, true))),
			'ParcelNumber' => $parcel_number,
			'ReturnPOD' => 0,
			'LanguageIsoCode' => 'HU'
		);

		//Logging
		VP_Woo_Pont()->log_debug_messages($options, 'gls-get-tracking-info');

		//Submit request
		$request = wp_remote_post( $this->api_url.'GetParcelStatuses', array(
			'body'    => json_encode($options),
			'headers' => array(
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Get tracking info
		if($response && isset($response['ParcelStatusList']) && count($response['ParcelStatusList']) > 0) {
			$tracking_info = array();

			//Loop through events
			foreach ($response['ParcelStatusList'] as $event) {

				//First, convert the dotnet formatted date to normal datetime and setup the event
				if (preg_match('#^(/Date\()([-]?[0-9]+)([0-9]{3})([+-][0-9]{4})?(\)/)$#', $event['StatusDate'], $matches)) {
					$datetime = \DateTime::createFromFormat('U u', $matches[2] . ' ' . $matches[3] . '000')->setTimezone(new \DateTimeZone($matches[4] ?: '+0000'));
					$tracking_info[] = array(
						'date' => $datetime->getTimestamp(),
						'event' => strval(intval($event['StatusCode'])),
						'label' => $event['StatusDescription']
					);
				}

			}

			//And return tracking info to store
			return $tracking_info;

		} else {
			return new WP_Error( 'gls_tracking_info_error', '' );
		}

	}

}
