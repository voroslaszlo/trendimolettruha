<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class VP_Woo_Pont_Packeta {
	protected $api_url = 'https://www.zasilkovna.cz/api/rest/';
	protected $api_key = '';
	protected $api_password = '';
	protected $sender_id = '';
	public $package_statuses = array();

	public function __construct() {
		$this->api_key = VP_Woo_Pont_Helpers::get_option('packeta_api_key');
		$this->api_password = VP_Woo_Pont_Helpers::get_option('packeta_api_password');
		$this->sender_id = VP_Woo_Pont_Helpers::get_option('packeta_sender');

		//Load settings
		add_filter('vp_woo_pont_global_settings', array($this, 'get_settings'));

		//Set supported statuses
		$this->package_statuses = array(
			1 => "We have received the packet data. Freshly created packet.",
			2 => "Packet has been accepted at our branch.",
			3 => "Packet is waiting to be dispatched.",
			4 => "Packet is on the way.",
			5 => "Packet has been delivered to its destination, the customer has been informed via SMS.",
			6 => "Packet has been handed over to an external carrier for delivery.",
			7 => "Packet was picked up by the customer.",
			9 => "Packet is on the way back to the sender.",
			10 => "Packet has been returned to the sender.",
			11 => "Packet has been cancelled.",
			12 => "Packet has been collected and is on its way.",
			14 => "Customs declaration process.",
			15 => "Reverse packet has been accepted at our branch.",
			999 => "Unknown packet status."
		);

	}

	public function get_settings($settings) {
		$supported_countries = array(
			'HU' => __('Hungary', 'vp-woo-pont'),
			'RO' => __('Romania', 'vp-woo-pont'),
			'SK' => __('Slovakia', 'vp-woo-pont'),
			'CZ' => __('Czech Republic', 'vp-woo-pont'),
			'PL' => __('Poland(InPost)', 'vp-woo-pont')
		);

		$packeta_settings = array(
			'section_packeta' => array(
				'title' => __( 'Packeta settings', 'vp-woo-pont' ),
				'type' => 'vp_woo_pont_settings_title',
				'description' => __( 'Settings related to the Packeta provider.', 'vp-woo-pont' ),
			),
			'packeta_api_key' => array(
				'title' => __('Packeta API key', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __('Please enter your Packeta API key if you plan to use it. This is required to get an up to date list of pickup points. Sign in into your Packeta account, click on your name top right and you can find the API key there.', 'vp-woo-pont')
			),
			'packeta_api_password' => array(
				'title' => __('Packeta API password', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __('Please enter your Packeta API password. This is required to generate labels for packages. Sign in into your Packeta account, click on your name top right and you can find the API key password there.', 'vp-woo-pont')
			),
			'packeta_sender' => array(
				'title' => __('Sender ID', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __('Please enter your SenderID. Sign in into your Packeta account and click User Informations / Senders.', 'vp-woo-pont')
			),
			'packeta_countries' => array(
				'type' => 'vp_woo_pont_settings_checkboxes',
				'title' => __( 'Enabled countries', 'vp-woo-pont' ),
				'options' => $supported_countries,
				'default' => array('HU'),
				'description' => __('The map will show pickup points from these countries.', 'vp-woo-pont')
			),
			'packeta_carriers' => array(
				'type' => 'vp_woo_pont_settings_packeta_carriers',
				'title' => __( 'Supported carriers', 'vp-woo-pont' ),
				'options' => $this->get_supported_carriers(),
				'description' => __('If you are planning to ship worldwide with home delivery, select the provider you want to use in each country.', 'vp-woo-pont')
			),
			'packeta_sticker_size' => array(
				'type' => 'select',
				'class' => 'wc-enhanced-select',
				'title' => __( 'Label format', 'vp-woo-pont' ),
				'default' => VP_Woo_Pont_Helpers::get_option('packeta_label_format', 'A7 on A4'),
				'options' => array(
					'A6 on A6' => __( 'A6 on A6', 'vp-woo-pont' ),
					'A7 on A7' => __( 'A7 on A7', 'vp-woo-pont' ),
					'A6 on A4' => __( 'A6 on A4', 'vp-woo-pont' ),
					'A7 on A4' => __( 'A7 on A4', 'vp-woo-pont' ),
					'105x35mm on A4' => __( '105x35mm on A4', 'vp-woo-pont' ),
					'A8 on A8' => __( 'A8', 'vp-woo-pont' ),
				),
			),
			'packeta_sticker_merge' => array(
				'title'    => __( 'Merge stickers on a single page', 'vp-woo-pont' ),
				'type'     => 'checkbox',
				'description' => __('If you are using the A6 on A4, A7 on A4 or the 105x35mm on A4 format, this option will merge multiple stickers on a single A4 page during bulk printing & printing.', 'vp-woo-pont')
			),
			'packeta_hide_zbox' => array(
				'title'    => __( 'Hide Z-Boxes', 'vp-woo-pont' ),
				'type'     => 'checkbox',
				'description' => __('If you are only shipping large packages, you can hide the Z-Box stations on the map.', 'vp-woo-pont')
			),
		);

		return $settings+$packeta_settings;
	}

	public function create_label($data) {

		//Create a new XML object
		$packet = new SimpleXMLElement('<createPacket></createPacket>');

		//Set password
		$packet->addChild('apiPassword', $this->api_password);

		//Set packat attributes
		$attributes = $packet->addChild('packetAttributes');
		$attributes->addChild('number', $data['reference_number']);
		$attributes->addChild('name', $data['customer']['first_name']);
		$attributes->addChild('surname', $data['customer']['last_name']);
		if($data['customer']['company']) {
			$attributes->addChild('company', $data['customer']['company']);
		}
		$attributes->addChild('email', $data['customer']['email']);
		$attributes->addChild('phone', $data['customer']['phone']);
		$attributes->addChild('addressId', $data['point_id']);
		$attributes->addChild('value', $data['package']['total']);
		$attributes->addChild('eshop_id', $this->sender_id);
		$attributes->addChild('currency', $this->get_packeta_package_currency($data['package'], false));

		//Set default weight if not exists
		if($data['package']['weight'] == 0) {
			$attributes->addChild('weight', 1);
		} else {
			$attributes->addChild('weight', wc_get_weight($data['package']['weight'], 'kg'));
		}

		//Check for COD
		if($data['package']['cod']) {
			//Rounded for HUF, normal for rest of the currencies
			if($data['package']['currency'] == 'HUF') {
				$attributes->addChild('cod', $data['package']['total_rounded']);
			} else {
				$attributes->addChild('cod', $data['package']['total']);
			}
		}

		//Use a different point id, if its home delivery and define shipping address too
		//ID in this case is the carrier ID set in settings
		if(!$data['point_id']) {
			$order = $data['order'];
			$shipping_address = $this->get_shipping_address($order);
			$carrier_id = $this->get_packeta_carrier_from_order($order);
			$attributes->addressId = $carrier_id;
			$attributes->addChild('note', implode(', ', $shipping_address['comment']));
			$attributes->addChild('street', $shipping_address['street']);
			$attributes->addChild('houseNumber', $shipping_address['number']);
			$attributes->addChild('city', $order->get_shipping_city());
			$attributes->addChild('zip', $order->get_shipping_postcode());
			$attributes->value = $this->get_packeta_package_value($data['package'], $carrier_id);
			$attributes->currency = $this->get_packeta_package_currency($data['package'], $carrier_id);
		}

		//Convert to xml string
		$packet = apply_filters('vp_woo_pont_packeta_label', $packet, $data);
		$xml = $packet->asXML();

		//Logging
		VP_Woo_Pont()->log_debug_messages($packet, 'packeta-create-label');

		//Create APi request
		$request = wp_remote_post( $this->api_url, array(
			'body' => $xml
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_encode(simplexml_load_string($response));
		$response = json_decode($response,true);

		//Check for errors
		if($response['status'] == 'fault') {
			VP_Woo_Pont()->log_error_messages($response, 'packeta-create-label');
			$error_messages[] = $response['string'];
			if(isset($response['fault']) && $response['fault'] == 'PacketAttributesFault') {
				$error_messages[] = json_encode($response['detail']['attributes']);
			}
			return new WP_Error( $response['fault'], implode('; ', $error_messages) );
		}

		//Else, it was successful
		$parcel_number = $response['result']['id'];

		//Next, generate the PDF label
		$packet_label = new SimpleXMLElement('<packetLabelPdf></packetLabelPdf>');

		//Set password and packet id
		$packet_label->addChild('apiPassword', $this->api_password);
		$packet_label->addChild('packetId', $parcel_number);
		$packet_label->addChild('format', VP_Woo_Pont_Helpers::get_option('packeta_sticker_size', VP_Woo_Pont_Helpers::get_option('packeta_label_format', 'A7 on A4')));
		$packet_label->addChild('offset', '0');

		//Convert to xml string
		$xml = $packet_label->asXML();

		//Create APi request
		$request = wp_remote_post( $this->api_url, array(
			'body' => $xml
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_encode(simplexml_load_string($response));
		$response = json_decode($response,true);

		//Check for errors
		if($response['status'] == 'fault') {
			VP_Woo_Pont()->log_error_messages($response, 'packeta-download-label');
			return new WP_Error( $response['fault'], $response['string'] );
		}

		//Now we have the PDF as base64, save it
		$attachment = $response['result'];
		$pdf = base64_decode($attachment);

		//Try to save PDF file
		$pdf_file = VP_Woo_Pont_Labels::get_pdf_file_path('packeta', $data['order_id']);
		VP_Woo_Pont_Labels::save_pdf_file($pdf, $pdf_file);

		//Create response
		$label = array();
		$label['id'] = $parcel_number;
		$label['number'] = $parcel_number;
		$label['pdf'] = $pdf_file['name'];

		//Return file name, package ID, tracking number which will be stored in order meta
		return $label;

	}

	public function void_label($data) {

		//Create a new XML object
		$packet = new SimpleXMLElement('<cancelPacket></cancelPacket>');

		//Set password
		$packet->addChild('apiPassword', $this->api_password);

		//Set package ID
		$packet->addChild('packetId', $data['parcel_id']);

		//Convert to xml string
		$packet = apply_filters('vp_woo_pont_packeta_void_label', $packet, $data);
		$xml = $packet->asXML();

		//Create APi request
		$request = wp_remote_post( $this->api_url, array(
			'body' => $xml
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_encode(simplexml_load_string($response));
		$response = json_decode($response,true);

		//Check for errors
		if($response['status'] == 'fault') {
			VP_Woo_Pont()->log_error_messages($response, 'packeta-delete-label');
			return new WP_Error( $response['fault'], $response['string'] );
		}

		//Check for success
		$label = array();
		$label['success'] = true;

		return $label;
	}

	//Return tracking link
	public function get_tracking_link($parcel_number, $order = false) {
		return 'https://tracking.packeta.com/hu/?id='.esc_attr($parcel_number);
	}

	public function get_supported_carriers($raw = false) {

		//Get the JSON file based on the provider type
		$download_folders = VP_Woo_Pont_Helpers::get_download_folder();
		$filename = get_option('_vp_woo_pont_file_packeta_carriers');
		if(!$filename) {
			return array();
		}

		$filepath = $download_folders['dir'].$filename;
		$json_file = file_get_contents($filepath);

		//Check if file exists
		if($json_file === false) {
			return array();
		}

		//Check if its a valid json file
		$json = json_decode($json_file, true);
		if ($json === null) {
			return array();
		}

		//Group by countries
		$carriers = array();
		foreach ($json as $carrier) {
			if(!isset($carriers[$carrier['country']])) {
				$carriers[$carrier['country']] = array();
			}
			$carriers[$carrier['country']][$carrier['id']] = $carrier['name'];
		}

		if($raw) {
			return $json;
		} else {
			return $carriers;
		}
	}

	public function get_packeta_carrier_from_order($order) {
		$supported_carriers = get_option('vp_woo_pont_packeta_carriers', array());
		$shipping_country = $order->get_shipping_country();
		$carrier = '';
		if(isset($supported_carriers[$shipping_country]) && $supported_carriers[$shipping_country] != '') {
			$carrier = $supported_carriers[$shipping_country];
		}

		return $carrier;
	}

	public function get_packeta_package_currency($package, $carrier_id) {
		$carriers = $this->get_supported_carriers(true);
		$carrier_currency = $package['currency'];
		foreach ($carriers as $carrier) {
			if($carrier['id'] == $carrier_id) {
				$carrier_currency = $carrier['currency'];
			}
		}
		return $carrier_currency;
	}

	public function get_packeta_package_value($package, $carrier_id) {
		$carrier_currency = $this->get_packeta_package_currency($package, $carrier_id);

		//Check if we need to convert value
		if($carrier_currency != $package['currency']) {
			return VP_Woo_Pont_Helpers::convert_currency($package['currency'], $carrier_currency, $package['total']);
		} else {
			return $package['total'];
		}

	}

	public function get_shipping_address($order) {
		$shipping_addres_1 = $order->get_shipping_address_1();
		$shipping_addres_2 = $order->get_shipping_address_2();
		$shipping_address = array(
			'street' => $shipping_addres_1,
			'number' => '-',
			'comment' => array()
		);

		//Try to split the shipping address at the first number
		if (preg_match('/\d/', $shipping_addres_1)) {
			$components = preg_split('/(?=\d)/', $shipping_addres_1, 2);
			if(!empty($components[0])) $shipping_address['street'] = $components[0];
			if(!empty($components[1])) $shipping_address['number'] = $components[1];
		}

		//Add customer note too
		if(!empty($shipping_addres_2)) {
			$shipping_address['comment'][] = $shipping_addres_2;
		}

		if(!empty($order->get_customer_note())) {
			$shipping_address['comment'][] = $order->get_customer_note();
		}

		return $shipping_address;
	}

	//Function to get tracking informations using API
	public function get_tracking_info($order) {

		//Parcel number
		$parcel_id = $order->get_meta('_vp_woo_pont_parcel_id');

		//Create a new XML object
		$packet = new SimpleXMLElement('<packetTracking></packetTracking>');

		//Set password
		$packet->addChild('apiPassword', $this->api_password);

		//Set package ID
		$packet->addChild('packetId', $parcel_id);

		//Convert to xml string
		$packet = apply_filters('vp_woo_pont_packeta_get_tracking_info', $packet, $order);
		$xml = $packet->asXML();

		//Create APi request
		$request = wp_remote_post( $this->api_url, array(
			'body' => $xml
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_encode(simplexml_load_string($response));
		$response = json_decode($response,true);

		//Check for errors
		if($response['status'] == 'fault') {
			VP_Woo_Pont()->log_error_messages($response, 'packeta-delete-label');
			return new WP_Error( $response['fault'], $response['string'] );
		}

		//Collect events
		$tracking_info = array();
		foreach ($response['result']['record'] as $event) {
			$tracking_info[] = array(
				'date' => strtotime($event['dateTime']),
				'event' => $event['statusCode'],
				'label' => ''
			);
		}

		//Reverse array, so newest is the first
		$tracking_info = array_reverse($tracking_info);

		return $tracking_info;
	}

}
