<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class VP_Woo_Pont_DPD {
	protected $api_url = 'https://weblabel.dpd.hu/dpd_wow/';
	protected $username = '';
	protected $password = '';
	public $package_statuses = array();

	public function __construct() {
		$this->username = VP_Woo_Pont_Helpers::get_option('dpd_username');
		$this->password = VP_Woo_Pont_Helpers::get_option('dpd_password');

		//Load settings
		add_filter('vp_woo_pont_global_settings', array($this, 'get_settings'));

		//Set supported statuses
		$this->package_statuses = array(
			"Shipped" => "Shipped",
			"Delivered" => "Delivered",
			"Out for delivery" => "Out for delivery",
			"Driver's return" => "Driver's return",
			"Pickup scan" => "Pickup scan",
			"System return" => "System return",
			"Outbound" => "Outbound",
			"HUB-scan" => "HUB-scan",
		);

		//Display a button in the orders admin page to manually run sync
		add_action( 'manage_posts_extra_tablenav', array( $this, 'display_sync_button' ), 9 );
		add_action( 'wp_ajax_vp_woo_pont_dpd_run_sync', array( $this, 'run_sync_with_ajax' ) );

	}

	public function get_settings($settings) {
		$dpd_settings = array(
			'section_dpd' => array(
				'title' => __( 'DPD settings', 'vp-woo-pont' ),
				'type' => 'vp_woo_pont_settings_title',
				'description' => __( 'Settings related to the DPD provider.', 'vp-woo-pont' ),
			),
			'dpd_username' => array(
				'title' => __('Weblabel Username', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __("Enter your DPD account's Weblabel username.", 'vp-woo-pont')
			),
			'dpd_password' => array(
				'title' => __('Weblabel Password', 'vp-woo-pont'),
				'type' => 'text',
				'description' => __("Enter your DPD account's Weblabel password.", 'vp-woo-pont')
			),
			'dpd_package_contents' => array(
				'title' => __('Package contents text', 'vp-woo-pont'),
				'type' => 'textarea',
				'description' => __('You can use the following shortcodes: {order_number}, {order_items}, {customer_note}', 'vp-woo-pont')
			),
			'dpd_sticker_merge' => array(
				'title'    => __( 'Merge stickers on a single page', 'vp-woo-pont' ),
				'type'     => 'checkbox',
				'description' => __('This option will merge multiple stickers on a single A4 page during bulk printing & printing.', 'vp-woo-pont')
			),
		);

		return $settings+$dpd_settings;
	}

	public function create_label($data) {

		//Create item
		$comment = VP_Woo_Pont()->labels->get_package_contents_label($data, 'dpd');
		$order = $data['order'];
		$item = array(
			'username' => $this->username,
			'password' => $this->password,
			'name1' => $data['customer']['name'],
			'street' => implode(' ', array($order->get_shipping_address_1(), $order->get_shipping_address_2())),
			'city' => $order->get_shipping_city(),
			'country' => $order->get_shipping_country(),
			'pcode' => $order->get_shipping_postcode(),
			'phone' => $data['customer']['phone'],
			'email' => $data['customer']['email'],
			'remark' => $comment,
			'weight' => wc_get_weight($data['package']['weight'], 'kg'),
			'num_of_parcel' => 1,
			'order_number' => $data['reference_number'],
			'parcel_type' => 'D'
		);

		//If its a point order
		if($data['point_id']) {
			$item['parcelshop_id'] = $data['point_id'];
			$item['parcel_type'] = 'PS';
		}

		//Check for COD
		if($data['package']['cod']) {
			$item['parcel_type'] = 'D-COD';
			$item['parcel_cod_type'] = 'all';
			$item['cod_amount'] = $data['package']['total'];
			if($data['point_id']) {
				$item['parcel_type'] = 'PSCOD';
			}
		}

		//So developers can modify
		$item = apply_filters('vp_woo_pont_dpd_label', $item, $data);

		//Logging
		VP_Woo_Pont()->log_debug_messages($options, 'dpd-create-label');

		//Submit request
		$request = wp_remote_post( $this->api_url.'parcel_import.php', array(
			'body'    => $item,
			'headers'  => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for errors
		if($response['status'] == 'err') {
			VP_Woo_Pont()->log_error_messages($response, 'dpd-create-label');
			return new WP_Error( 'dpd_error', $response['errlog'] );
		}

		//Else, it was successful
		$parcel_number = $response['pl_number'][0];

		//Next, generate the PDF label
		$request = wp_remote_post( $this->api_url.'parcel_print.php', array(
			'body'    => array(
				'username' => $this->username,
				'password' => $this->password,
				'parcels' => $parcel_number
			),
			'headers' => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );

		//Check if its json, if so, thats an error
		$json = json_decode( $response );
		if($json !== null) {
			return new WP_Error( 'dpd_error', $json['errlog'] );
		}

		//Now we have the PDF as base64, save it
		$pdf = $response;

		//Try to save PDF file
		$pdf_file = VP_Woo_Pont_Labels::get_pdf_file_path('dpd', $data['order_id']);
		VP_Woo_Pont_Labels::save_pdf_file($pdf, $pdf_file);

		//Create response
		$label = array();
		$label['id'] = $parcel_number;
		$label['number'] = $parcel_number;
		$label['pdf'] = $pdf_file['name'];

		//Return file name, package ID, tracking number which will be stored in order meta
		return $label;
	}

	public function void_label($data) {

		//Create request data
		VP_Woo_Pont()->log_debug_messages($data, 'dpd-void-label-request');

		//Submit request
		$request = wp_remote_post( $this->api_url.'parcel_delete.php', array(
			'body'    => array(
				'username' => $this->username,
				'password' => $this->password,
				'parcels' => $options['parcel_number']
			),
			'headers' => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for errors
		if($response['status'] == 'err') {
			VP_Woo_Pont()->log_error_messages($response, 'dpd-create-label');
			return new WP_Error( 'dpd_error', $response['errlog'] );
		}

		//Check for success
		$label = array();
		$label['success'] = true;

		VP_Woo_Pont()->log_debug_messages($response, 'dpd-void-label-response');

		return $label;
	}

	//Return tracking link
	public function get_tracking_link($parcel_number, $order = false) {
		return 'https://www.dpdgroup.com/hu/mydpd/my-parcels/track?lang=hu&parcelNumber='.esc_attr($parcel_number);
	}

	//Function to get tracking informations using API
	public function get_tracking_info($order) {

		//Parcel number
		$parcel_number = $order->get_meta('_vp_woo_pont_parcel_number');

		//Submit request
		$request = wp_remote_post( $this->api_url.'parcel_status.php', array(
			'body'    => array(
				'secret' => 'FcJyN7vU7WKPtUh7m1bx', //Fixed secret based on the Weblabel documentation
				'parcel_number' => $parcel_number
			),
			'headers' => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for errors
		if($response['status'] == 'err') {
			VP_Woo_Pont()->log_error_messages($response, 'dpd-create-label');
			return new WP_Error( 'dpd_error', $response['errlog'] );
		}

		//Get existing status updates, since this API only returns the current status of the package, not all
		$tracking_info = $order->get_meta('_vp_woo_pont_parcel_info');
		if(!$tracking_info) $tracking_info = array();

		//Check if requested status already saved
		$status_saved = false;
		foreach ($tracking_info as $event) {
			if($event['event'] == $response['parcel_status']) {
				$status_saved = true;
			}
		}

		//If already saved, just return the current tracking info
		if($status_saved) {
			return $tracking_info;
		}

		//Prepend the new event at the beginning
		$event = array(
			'date' => strtotime($response['event_date']),
			'event' => $response['parcel_status'],
			'label' => ''
		);
		array_unshift($tracking_info, $event);

		return $tracking_info;
	}

	public function display_sync_button() {
		global $pagenow, $post_type;
		if( 'shop_order' === $post_type && 'edit.php' === $pagenow && VP_Woo_Pont_Helpers::is_provider_configured('dpd') && VP_Woo_Pont_Pro::is_pro_enabled()) {
			?>
			<div class="alignleft actions vp-woo-pont-dpd-start-sync">
				<a href="#" class="button tips" data-nonce="<?php echo wp_create_nonce( "vp_woo_pont_dpd_start_sync" ); ?>" data-tip="<?php esc_attr_e('Send parcel data to DPD','vp-woo-pont'); ?>">
					<i class="vp-woo-pont-provider-icon-dpd"></i>
					<span><?php esc_html_e('DPD Datasend', 'vp-woo-pont'); ?></span>
					<span class="dashicons dashicons-yes"></span>
				</a>
			</div>
			<?php
		}
	}

	public function run_sync_with_ajax() {
		check_ajax_referer( 'vp_woo_pont_dpd_start_sync', 'nonce' );
		$send = $this->parcel_data_send();
		if(is_wp_error($send)) {
			wp_send_json_error();
		} else {
			wp_send_json_success();
		}
	}

	public function parcel_data_send() {

		//Submit request
		$request = wp_remote_post( $this->api_url.'parceldatasend.php', array(
			'body'    => array(
				'username' => $this->username,
				'password' => $this->password
			),
			'headers' => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		//Check for errors
		if(is_wp_error($request)) {
			return $request;
		}

		//Parse response
		$response = wp_remote_retrieve_body( $request );
		$response = json_decode( $response, true );

		//Check for errors
		if($response['status'] == 'err') {
			VP_Woo_Pont()->log_error_messages($response, 'dpd-create-label');
			return new WP_Error( 'dpd_error', $response['errlog'] );
		}

		return true;
	}

}
