<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Webshippy Compatibility
class Vp_Woo_Pont_Woo_Webshippy_Compatibility {

	public static function init() {

		//Add condition for pickup point for order notes
		add_filter( 'http_request_args', array( __CLASS__, 'add_point_data'), 10, 2 );

	}

	public static function add_point_data($parsed_args, $url) {

		//Continue if not webshippy request
		if($url != webshippy_url) return $parsed_args;

		//Get body parameters
		$body = json_decode($parsed_args['body'], true);

		//Get order
		$order = wc_get_order($body['order']['order_id']);
		if($order && $order->get_meta('_vp_woo_pont_point_id')) {
			$provider = $order->get_meta('_vp_woo_pont_provider');
			$point_id = $order->get_meta('_vp_woo_pont_point_id');
			if($provider == 'foxpost') $provider = 'FoxPost';
			if($provider == 'gls') $provider = 'GLS CsomagPont';
			$body['order']['wc_selected_pont'] = $order->get_meta('_vp_woo_pont_point_name').'|'.$provider.'|'.$point_id;

			//Hide shipping company name if it was a point order
			$body['order']['shipping_address_company'] = '';

			//Use the billing name as the shipping name if it was a point order
			$body['order']['shipping_address_name'] = $order->get_formatted_billing_full_name();

		}

		//Set body again
		$parsed_args['body'] = json_encode($body);

		return $parsed_args;
	}

}

Vp_Woo_Pont_Woo_Webshippy_Compatibility::init();
