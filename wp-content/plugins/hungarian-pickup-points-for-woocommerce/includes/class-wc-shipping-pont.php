<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WC_Shipping_Pont extends WC_Shipping_Method {

	//Create instance
	public function __construct( $instance_id = 0 ) {
		$this->id                 = 'vp_pont';
		$this->instance_id        = absint( $instance_id );
		$this->method_title       = _x( 'Pickup points & labels', 'admin', 'vp-woo-pont' );
		$this->method_description = __( 'You can find some extra options under Appearance / WooCommerce / Pickup Points. ', 'vp-woo-pont' );
		$this->supports           = array(
			'shipping-zones',
			'settings',
		);
		$this->init();

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'save_custom_options' ) );
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

	}

	//Initialize settings.
	private function init() {
		$this->title                    = $this->get_option( 'title', $this->method_title );
		$this->tax_status               = $this->get_option( 'tax_status' );
		$this->instance_form_fields     = include __DIR__ . '/settings-instance.php';
		$this->form_fields              = include __DIR__ . '/settings-global.php';
	}

	//Calculate cost
	public function calculate_shipping( $package = array() ) {
		$rate = array(
			'id'      => $this->get_rate_id(),
			'label'   => $this->title,
			'cost'    => 0,
			'package' => $package,
		);

		//Find out shipping cost
		$shipping_cost = VP_Woo_Pont_Helpers::get_shipping_cost();

		//If a cost is found, set it
		if($shipping_cost && isset($shipping_cost['net'])) {
			$rate['cost'] = $shipping_cost['net'];
		}

		//Set rate
		$this->add_rate( $rate );
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_title_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_webhook_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_pricing_table_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_points_table_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_checkboxes_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_automations_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_home_delivery_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_packeta_carriers_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_auto_status_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function generate_vp_woo_pont_settings_tracking_automations_html( $key, $data) {
		return $this->render_custom_setting_html($key, $data);
	}

	//Generate html for custom settings fields
	public function render_custom_setting_html($key, $data) {
		$field_key = $this->get_field_key( $key );
		$defaults = array(
			'title' => '',
			'disabled' => false,
			'class' => '',
			'css' => '',
			'placeholder' => '',
			'type' => 'text',
			'desc_tip' => false,
			'description' => '',
			'custom_attributes' => array(),
			'options' => array()
		);
		$data = wp_parse_args( $data, $defaults );
		$template_name = str_replace('vp_woo_pont_settings_', '', $data['type']);
		ob_start();
		include( dirname( __FILE__ ) . '/views/html-admin-'.str_replace('_', '-', $template_name).'.php' );
		return ob_get_clean();
	}

	//To save extra fields
	public function save_custom_options() {

		//Save pricing options
		$prices = array();
		if ( isset( $_POST['vp_woo_pont_pricing'] ) ) {
			foreach ($_POST['vp_woo_pont_pricing'] as $pricing_id => $pricing) {

				$cost = wc_clean($pricing['cost']);
				$prices[$pricing_id] = array(
					'cost' => (float)$cost,
					'conditional' => false,
					'providers' => array()
				);

				//If theres conditions to setup
				$condition_enabled = isset($pricing['condition_enabled']) ? true : false;
				$conditions = (isset($pricing['conditions']) && count($pricing['conditions']) > 0);
				if($condition_enabled && $conditions) {
					$prices[$pricing_id]['conditional'] = true;
					$prices[$pricing_id]['conditions'] = array();
					$prices[$pricing_id]['logic'] = wc_clean($pricing['logic']);

					foreach ($pricing['conditions'] as $condition) {
						$condition_details = array(
							'category' => wc_clean($condition['category']),
							'comparison' => wc_clean($condition['comparison']),
							'value' => $condition[$condition['category']]
						);

						$prices[$pricing_id]['conditions'][] = $condition_details;
					}
				}

				//Save providers
				$providers = (isset($pricing['providers']) && count($pricing['providers']) > 0);
				if($providers) {
					foreach ($pricing['providers'] as $provider) {
						$prices[$pricing_id]['providers'][] = $provider;
					}
				}

			}
		}

		update_option( 'vp_woo_pont_pricing', $prices );

		//Save custom points
		$points = array();
		if ( isset( $_POST['vp_woo_pont_points'] ) ) {
			foreach ($_POST['vp_woo_pont_points'] as $point_id => $point) {

				$name = wc_clean($point['name']);
				$id = wc_clean($point['id']);
				$provider = wc_clean($point['provider']);
				$coordinates = wc_clean($point['coordinates']);
				$zip = wc_clean($point['zip']);
				$addr = wc_clean($point['addr']);
				$city = wc_clean($point['city']);
				$comment = wp_kses_post( trim( wp_unslash($point['comment']) ) );
				$hidden = isset($point['hidden']) ? true : false;

				//Convert coordinates
				$coordinates = explode(';', $coordinates);

				//Create new point
				$points[$point_id] = array(
					'name' => $name,
					'id' => $id,
					'provider' => $provider,
					'lat' => $coordinates[0],
					'lon' => $coordinates[1],
					'zip' => $zip,
					'addr' => $addr,
					'city' => $city,
					'comment' => $comment,
					'hidden' => $hidden,
				);
			}
		}

		update_option( 'vp_woo_pont_points', $points );

		//Save automations options
		$automations = array();
		if ( isset( $_POST['vp_woo_pont_automation'] ) ) {
			foreach ($_POST['vp_woo_pont_automation'] as $automation_id => $automation) {

				$trigger = sanitize_text_field($automation['trigger']);
				$automations[$automation_id] = array(
					'trigger' => $trigger,
					'conditional' => false
				);

				//If theres conditions to setup
				$condition_enabled = isset($automation['condition_enabled']) ? true : false;
				$conditions = (isset($automation['conditions']) && count($automation['conditions']) > 0);
				if($condition_enabled && $conditions) {
					$automations[$automation_id]['conditional'] = true;
					$automations[$automation_id]['conditions'] = array();
					$automations[$automation_id]['logic'] = wc_clean($automation['logic']);

					foreach ($automation['conditions'] as $condition) {
						$condition_details = array(
							'category' => wc_clean($condition['category']),
							'comparison' => wc_clean($condition['comparison']),
							'value' => $condition[$condition['category']]
						);

						$automations[$automation_id]['conditions'][] = $condition_details;
					}
				}

			}
		}

		update_option( 'vp_woo_pont_automations', $automations );

		//Save checkbox groups
		$checkbox_groups = array('enabled_providers', 'packeta_countries', 'free_shipping');
		foreach ($checkbox_groups as $checkbox_group) {
			$checkbox_values = array();
			if ( isset( $_POST['vp_woo_pont_'.$checkbox_group] ) ) {
				foreach ($_POST['vp_woo_pont_'.$checkbox_group] as $checkbox_value) {
					$checkbox_values[] = wc_clean($checkbox_value);
				}
			}
			update_option('vp_woo_pont_'.$checkbox_group, $checkbox_values);
		}

		//If the Packeta API key was updated, run the importer
		if(VP_Woo_Pont_Helpers::get_option('packeta_api_key') != $_POST['woocommerce_vp_pont_packeta_api_key']) {
			$new_api_key = sanitize_text_field($_POST['woocommerce_vp_pont_packeta_api_key']);
			VP_Woo_Pont_Import_Database::get_packeta_json($new_api_key);
		}

		//Save home delivery options
		$home_delivery = array();
		if ( isset( $_POST['vp_woo_pont_home_delivery'] ) ) {
			foreach ($_POST['vp_woo_pont_home_delivery'] as $shipping_method_id => $provider_id) {
				$home_delivery[sanitize_text_field($shipping_method_id)] = sanitize_text_field($provider_id);
			}
			update_option('vp_woo_pont_home_delivery', $home_delivery);
		}

		//Save packeta home delivery carriers
		$packeta_carriers = array();
		if ( isset( $_POST['wc_billingo_plus_packeta_carriers'] ) ) {
			foreach ($_POST['wc_billingo_plus_packeta_carriers'] as $country_group => $info) {
				$packeta_carriers[sanitize_text_field($info['country'])] = sanitize_text_field($info['carrier']);
			}
			update_option('vp_woo_pont_packeta_carriers', $packeta_carriers);
		}

		//Save tracking automations
		$tracking_automations = array();
		if ( isset( $_POST['vp_woo_pont_tracking_automation'] ) ) {
			foreach ($_POST['vp_woo_pont_tracking_automation'] as $automation_id => $automation) {

				$trigger = sanitize_text_field($automation['order_status']);
				$tracking_automations[$automation_id] = array(
					'order_status' => $trigger
				);

				$package_statuses = (isset($automation['package_status']) && count($automation['package_status']) > 0);

				if($package_statuses) {
					foreach ($automation['package_status'] as $provider_id => $tracking_statuses) {
						$provider_id = sanitize_text_field($provider_id);
						$tracking_automations[$automation_id][$provider_id] = array();

						foreach ($tracking_statuses as $tracking_status) {
							$tracking_status = sanitize_text_field($tracking_status);
							$tracking_automations[$automation_id][$provider_id][] = $tracking_status;
						}
					}
				}
			}
		}

		update_option( 'vp_woo_pont_tracking_automations', $tracking_automations );

	}

	public function get_available_providers() {
		$providers = VP_Woo_Pont_Helpers::get_supported_providers();
		return $providers;
	}

	public function get_bulk_zip_error() {
		$message = '';
		if(	!class_exists('ZipArchive')) {
			$message = '<span class="vp-woo-pont-settings-error"><span class="dashicons dashicons-warning"></span> '.__('This feature requires the ZipArchive function and by the looks of it, this is not enabled on your website. You can ask your hosting provider for help.', 'vp-woo-pont').'</span>';
		}
		return $message;
	}

	//Get order statues
	public function get_order_statuses($empty = false) {
		$statuses = array();
		if(function_exists('wc_order_status_manager_get_order_status_posts')) {
			$filtered_statuses = array();
			$custom_statuses = wc_order_status_manager_get_order_status_posts();
			foreach ($custom_statuses as $status ) {
				$filtered_statuses[ 'wc-' . $status->post_name ] = $status->post_title;
			}
			$statuses = $filtered_statuses;
		} else {
			$statuses = wc_get_order_statuses();
		}

		if($empty) {
			$statuses = array('' => $empty) + $statuses;
		}

		return $statuses;
	}

	public function get_supported_packeta_carriers() {

		//Get the JSON file based on the provider type
		$download_folders = VP_Woo_Pont_Helpers::get_download_folder();
		$filename = get_option('_vp_woo_pont_file_packeta_carriers');
		$filepath = $download_folders['dir'].$filename;
		$json_file = file_get_contents($filepath);

		//Check if file exists
		if($json_file === false) {
			return array();
		}

		//Check if its a valid json file
		$json = json_decode($json_file, true);
		if ($json === null) {
			return array();
		}

		//Group by countries
		$carriers = array();
		foreach ($json as $carrier) {
			if(!isset($carriers[$carrier['country']])) {
				$carriers[$carrier['country']] = array();
			}
			$carriers[$carrier['country']][$carrier['id']] = $carrier['name'];
		}

		return array();
	}

}
