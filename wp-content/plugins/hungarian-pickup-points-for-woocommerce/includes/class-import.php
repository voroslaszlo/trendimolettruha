<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'VP_Woo_Pont_Import_Database', false ) ) :

	class VP_Woo_Pont_Import_Database {
	private static $pont_types = array('foxpost', 'postapont', 'packeta', 'sprinter', 'expressone', 'gls', 'dpd', 'sameday');

		public static function init() {

			//Setup daily scheduled action to refresh databases
			foreach (self::$pont_types as $pont_type) {
				add_action( 'vp_woo_pont_update_'.$pont_type.'_list', array( __CLASS__, 'get_'.$pont_type.'_json' ) );
			}

			//Ajax function to trigger import manually
			add_action( 'wp_ajax_vp_woo_pont_import_json_manually', array( __CLASS__, 'import_manually' ) );

		}

		public static function schedule_actions() {

			foreach (self::$pont_types as $pont_type) {
				WC()->queue()->cancel_all( 'vp_woo_pont_update_'.$pont_type.'_list' );
			}

			foreach (self::$pont_types as $pont_type) {

				//Download postapont list on activation
				WC()->queue()->add( 'vp_woo_pont_update_'.$pont_type.'_list', array(), 'vp_woo_pont' );

				//Setup scheduled actions
				WC()->queue()->schedule_recurring( time()+DAY_IN_SECONDS, DAY_IN_SECONDS, 'vp_woo_pont_update_'.$pont_type.'_list', array(), 'vp_woo_pont' );

			}
		}

		public static function get_postapont_json() {
			$need_sync = self::check_if_sync_needed('postapont');
			if(!$need_sync) return false;

			$request = wp_remote_get('https://www.posta.hu/szolgaltatasok/posta-srv-postoffice/rest/postoffice/listPPMarkers?callback');

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'postapont-import-points');
				return false;
			}

			//Get body
			$body = wp_remote_retrieve_body( $request );

			//Remove first and last character, because it was a jsonp request
			$body = substr($body, 1, -1);

			//Try to convert into json
			$json = json_decode( $body );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Create a new json
			$results = array();
			$valid_groups = array(10,20,30,50,70); //60 and 80 category is available, but nto documented, so lets skip those for now

			//Simplify json, so its smaller to store, faster to load
			foreach ($json as $postapont) {
				$group = intval(substr($postapont->group, 0, 2));
				if(in_array($group, $valid_groups)) {
					$result = array(
						'id' => $postapont->id,
						'group' => $group,
						'lat' => number_format($postapont->lat, 5, '.', ''),
						'lon' => number_format($postapont->lon, 5, '.', ''),
						'name' => $postapont->name,
						'zip' => $postapont->zip,
						'addr' => $postapont->address,
						'city' => $postapont->county,
						'comment' => $postapont->phone
					);

					//If there is a separate zip code, use it for search
					if($postapont->zip != $postapont->kzip) {
						$result['keywords'] = $postapont->kzip;
					}

					$results[] = $result;
				}
			}

			//Save stuff
			self::save_json_file('postapont', $results);
			return true;

		}

		public static function get_foxpost_json() {
			$need_sync = self::check_if_sync_needed('foxpost');
			if(!$need_sync) return false;

			$request = wp_remote_get('https://cdn.foxpost.hu/foxpost_terminals_extended_v3.json');

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'foxpost-import-points');
				return false;
			}

			//Get body
			$body = wp_remote_retrieve_body( $request );

			//Try to convert into json
			$json = json_decode( $body );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Create a new json
			$results = array();

			//Simplify json, so its smaller to store, faster to load
			foreach ($json as $foxpost) {
				$result = array(
					'id' => $foxpost->place_id,
					'lat' => number_format($foxpost->geolat, 5, '.', ''),
					'lon' => number_format($foxpost->geolng, 5, '.', ''),
					'name' => $foxpost->name,
					'zip' => $foxpost->zip,
					'addr' => $foxpost->street,
					'city' => $foxpost->city,
					'comment' => $foxpost->findme
				);
				$results[] = $result;
			}

			//Save stuff
			self::save_json_file('foxpost', $results);
			return true;

		}

		public static function get_packeta_json($api_key = false) {
			$need_sync = self::check_if_sync_needed('packeta');
			if(!$need_sync) return false;

			//Check API key
			if(!$api_key) {
				$api_key = VP_Woo_Pont_Helpers::get_option('packeta_api_key');
			}

			//If theres no api key, return error
			if(!$api_key) {
				return false;
			}

			//Get supported countries
			$enabled_countries = get_option('vp_woo_pont_packeta_countries', array('HU'));
			$skip_zbox = (VP_Woo_Pont_Helpers::get_option('packeta_hide_zbox', 'no') == 'yes');

			//Download address delivery list too
			$download_folder = VP_Woo_Pont_Helpers::get_download_folder('packeta');
			$file_branch = $download_folder['dir'].'packeta-source-file.json';
			$file_branch_polish = $download_folder['dir'].'packeta-source-file-pl.json';
			$file_address_delivery = $download_folder['dir'].'packeta-source-address-delivery.json';
			$request = wp_remote_get('https://www.zasilkovna.cz/api/v4/'.$api_key.'/branch.json?address-delivery&lang=hu', array(
				'timeout' => 100,
				'stream' => true,
				'filename' => $file_address_delivery
			));

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'packeta-import-points');
				return false;
			}

			//Create a new json
			$results = array();

			//For
			if(in_array('HU', $enabled_countries) || in_array('RO', $enabled_countries) || in_array('SK', $enabled_countries) || in_array('CZ', $enabled_countries)) {

				//Make request for branches json file
				$request = wp_remote_get('https://www.zasilkovna.cz/api/v4/'.$api_key.'/branch.json?lang=hu', array(
					'timeout' => 100,
					'stream' => true,
					'filename' => $file_branch
				));

				//Get body
				$body = file_get_contents($file_branch);

				//Try to convert into json
				$json = json_decode( $body );

				//Check if json exists
				if($json === null) {
					return false;
				}

				//Simplify json, so its smaller to store, faster to load
				foreach ($json->data as $place) {

					//For now, only hungarian points
					if(in_array(strtoupper($place->country), $enabled_countries) && $place->status->statusId == '1') {

						//Check if we need to skip Z-Box
						if($skip_zbox && strpos($place->place, 'Z-BOX') !== false) {
							continue;
						}

						$result = array(
							'id' => $place->id,
							'lat' => number_format($place->latitude, 5, '.', ''),
							'lon' => number_format($place->longitude, 5, '.', ''),
							'name' => $place->place,
							'zip' => $place->zip,
							'addr' => $place->street,
							'city' => $place->city,
							'country' => strtoupper($place->country)
						);

						if(isset($place->directions) && $place->directions != new stdClass()) {
							$result['comment'] = wp_strip_all_tags($place->directions);
						}

						$results[] = $result;
					}

				}

			}

			//Download polish pickup points
			if(in_array('PL', $enabled_countries)) {
				$request = wp_remote_get('https://www.zasilkovna.cz/api/'.$api_key.'/point.json?ids=3060', array(
					'timeout' => 100,
					'stream' => true,
					'filename' => $file_branch_polish
				));

				//Check for errors
				if( is_wp_error( $request ) ) {
					VP_Woo_Pont()->log_error_messages($request, 'packeta-import-points');
					return false;
				}

				//Get body
				$points = file_get_contents($file_branch_polish);

				//Try to convert into json
				$json = json_decode( $points );

				//Check if json exists
				if($json === null) {
					return false;
				}

				foreach ($json->carriers[0]->points as $place) {

					//For now, only hungarian points
					if($place->displayFrontend == '1') {
						$result = array(
							'id' => $place->code,
							'lat' => number_format($place->coordinates->latitude, 5, '.', ''),
							'lon' => number_format($place->coordinates->longitude, 5, '.', ''),
							'name' => $place->city.' '.$place->street,
							'zip' => $place->zip,
							'addr' => $place->street.' '.$place->streetNumber,
							'city' => $place->city,
							'country' => strtoupper($place->country)
						);
						$results[] = $result;
					}

				}

			}

			//Save stuff
			self::save_json_file('packeta', $results);

			//Parse home delivery stuff too
			$body = file_get_contents($file_address_delivery);

			//Try to convert into json
			$json = json_decode( $body );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Create a new json
			$results = array();

			//Simplify json, so its smaller to store, faster to load
			foreach ($json->carriers as $carrier) {

				//For now, only hungarian points
				if($carrier->pickupPoints == 'false') {
					$result = array(
						'id' => $carrier->id,
						'name' => $carrier->name,
						'country' => strtoupper($carrier->country),
						'currency' => $carrier->currency
					);
					$results[] = $result;
				}

			}

			//Save stuff
			self::save_json_file('packeta_carriers', $results);

			//Delete source file
			unlink($file_branch);
			unlink($file_branch_polish);
			unlink($file_address_delivery);

			return true;

		}

		public static function get_sprinter_json() {
			$need_sync = self::check_if_sync_needed('sprinter');
			if(!$need_sync) return false;

			//Make request for json file
			$request = wp_remote_get('https://partner.pickpackpont.hu/stores/ShopList.json');

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'sprinter-import-points');
				return false;
			}

			//Get body
			$body = wp_remote_retrieve_body( $request );

			//Remove BOM, so it sa valid json
			$bom = pack('H*','EFBBBF');
	    $body = preg_replace("/^$bom/", '', $body);

			//Try to convert into json
			$json = json_decode( $body, true );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Create a new json
			$results = array();

			//Simplify json, so its smaller to store, faster to load
			foreach ($json as $place) {

				//For now, only hungarian points
				$result = array(
					'id' => $place['shopCode'],
					'lat' => number_format($place['lat'], 5, '.', ''),
					'lon' => number_format($place['lng'], 5, '.', ''),
					'name' => $place['shopName'],
					'zip' => $place['zipCode'],
					'addr' => $place['address'],
					'city' => $place['city']
				);

				if($place['description']) {
					$result['comment'] = $place['description'];
				}

				$results[] = $result;
			}

			//Save stuff
			self::save_json_file('sprinter', $results);
			return true;

		}

		public static function get_expressone_json() {
			$need_sync = self::check_if_sync_needed('expressone');
			if(!$need_sync) return false;

			//Make request for json file
			$request = wp_remote_post('https://webcas.expressone.hu/eoneshops/eoneshop.php?method=getTofShopPointsAll&lang=hu');

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'expressone-import-points');
				return false;
			}

			//Get body
			$body = wp_remote_retrieve_body( $request );

			//Try to convert into json
			$json = json_decode( $body, true );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Create a new json
			$results = array();

			//Simplify json, so its smaller to store, faster to load
			foreach ($json as $place) {

				//For now, only hungarian points
				$result = array(
					'id' => $place['tof_shop_id'],
					'lat' => number_format(str_replace(',', '.', $place['gis_y']), 5, '.', ''),
					'lon' => number_format(str_replace(',', '.', $place['gis_x']), 5, '.', ''),
					'name' => $place['name'],
					'zip' => $place['zip_code'],
					'addr' => $place['street'],
					'city' => $place['city']
				);

				$results[] = $result;
			}

			//Save stuff
			self::save_json_file('expressone', $results);
			return true;

		}

		public static function get_gls_json() {
			$need_sync = self::check_if_sync_needed('gls');
			if(!$need_sync) return false;

			//Make request for json file
			$request = wp_remote_get('https://csomag.hu/api/parcel-shops');

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'gls-import-points');
				return false;
			}

			//Get body
			$body = wp_remote_retrieve_body( $request );

			//Try to convert into json
			$json = json_decode( $body, true );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Create a new json
			$results = array();

			//Should we hide ParcelLockers?
			$hide_parcel_lockers = (VP_Woo_Pont_Helpers::get_option('gls_hide_parcellocker', 'no') == 'yes');

			//Simplify json, so its smaller to store, faster to load
			foreach ($json['data'] as $place) {

				//Skip parcel lockers
				if($hide_parcel_lockers && $place['features']['isParcelLocker']) continue;

				//For now, only hungarian points
				$result = array(
					'id' => $place['id'],
					'lat' => number_format($place['location'][0], 5, '.', ''),
					'lon' => number_format($place['location'][1], 5, '.', ''),
					'name' => $place['name'],
					'zip' => $place['contact']['zipCode'],
					'addr' => $place['contact']['address'],
					'city' => $place['contact']['city']
				);

				if($place['description']) {
					$result['comment'] = $place['description'];
				}

				$results[] = $result;
			}

			//Save stuff
			self::save_json_file('gls', $results);
			return true;

		}

		public static function get_dpd_json($username = false, $password = false) {
			$need_sync = self::check_if_sync_needed('dpd');
			if(!$need_sync) return false;

			//Check API key
			if(!$username || !$password) {
				$username = VP_Woo_Pont_Helpers::get_option('dpd_username');
				$password = VP_Woo_Pont_Helpers::get_option('dpd_password');
			}

			//If theres no api key, return error
			if(!$username || !$password) {
				return false;
			}

			$request = wp_remote_post('https://weblabel.dpd.hu/dpd_wow/parcelshop_info.php', array(
				'headers' => array(
					'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8'
				),
				'body' => 'username='.$username.'&password='.$password,
				'httpversion' => '1.1'
			));

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'dpd-import-points');
				return false;
			}

			//Get body
			$body = wp_remote_retrieve_body( $request );

			//Try to convert into json
			$json = json_decode( $body, true );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Check if we have data
			if(!isset($json['parcelshops'])) {
				return false;
			}

			//Create a new json
			$results = array();

			//Simplify json, so its smaller to store, faster to load
			foreach ($json['parcelshops'] as $place) {

				//For now, only hungarian points
				$result = array(
					'id' => $place['parcelshop_id'],
					'lat' => number_format($place['gpslat'], 5, '.', ''),
					'lon' => number_format($place['gpslong'], 5, '.', ''),
					'name' => $place['company'],
					'zip' => $place['pcode'],
					'addr' => $place['street'],
					'city' => $place['city']
				);

				$results[] = $result;
			}

			//Save stuff
			self::save_json_file('dpd', $results);
			return true;

		}

		public static function get_sameday_json() {
			$need_sync = self::check_if_sync_needed('sameday');
			if(!$need_sync) return false;

			//If theres no password and username set, return error
			if(!VP_Woo_Pont()->providers['sameday']->username || !VP_Woo_Pont()->providers['sameday']->password) {
				return false;
			}

			//Get auth token
			$token = VP_Woo_Pont()->providers['sameday']->get_access_token();

			//If no auth token, wrong username/password or sometihng like that
			if(is_wp_error($token)) {
				return false;
			}

			//Get zip code ids - sameday stores the poscodes in a different format, so we need to convert that later into actual postcodes
			$api_url = VP_Woo_Pont()->providers['sameday']->api_url;
			$request = wp_remote_get($api_url.'locker/lockers', array(
				'headers' => array(
					'Content-Type' => 'application/x-www-form-urlencoded',
					'X-AUTH-TOKEN' => $token
				)
			));

			//Check for errors
			if( is_wp_error( $request ) ) {
				VP_Woo_Pont()->log_error_messages($request, 'sameday-import-points');
				return false;
			}

			//Get body
			$body = wp_remote_retrieve_body( $request );

			//Try to convert into json
			$json = json_decode( $body, true );

			//Check if json exists
			if($json === null) {
				return false;
			}

			//Create a new json
			$results = array();

			//Simplify json, so its smaller to store, faster to load
			foreach ($json as $easybox) {

				//Only hungarian points
				if($easybox['countryId'] != 237) continue;

				$result = array(
					'id' => $easybox['lockerId'],
					'lat' => number_format($easybox['lat'], 5, '.', ''),
					'lon' => number_format($easybox['lng'], 5, '.', ''),
					'name' => $easybox['name'],
					'zip' => $easybox['postalCode'],
					'addr' => $easybox['address'],
					'city' => $easybox['city']
				);
				$results[] = $result;
			}

			//Save stuff
			self::save_json_file('sameday', $results);
			return true;

		}

		public static function save_json_file($provider, $json) {

			//Create smaller json
			$smaller_json = json_encode($json, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

			//Get filename and download folder
			$paths = VP_Woo_Pont_Helpers::get_download_folder($provider);
			$filename = $paths['name'];

			//Remove existing file
			$existing_filename = get_option('_vp_woo_pont_file_'.$provider);
			if($existing_filename) {
				$existing_file = $paths['dir'].$existing_filename;
				if(file_exists($existing_file)){
					unlink($existing_file);
				}
			}

			//Save the file
			file_put_contents($paths['path'], $smaller_json);

			//Store current file name in db
			update_option('_vp_woo_pont_file_'.$provider, $filename);

		}

		public static function import_manually() {
			check_ajax_referer( 'vp-woo-pont-import-json-manually', 'nonce' );

			//Get provider id
			$pont_type = sanitize_text_field($_POST['provider']);

			//Try to run the import
			$import = call_user_func(array(__CLASS__, 'get_'.$pont_type.'_json'));

			//Return response
			if($import) {
				$download_folders = VP_Woo_Pont_Helpers::get_download_folder();
				$updated_file = get_option('_vp_woo_pont_file_'.$pont_type);
				wp_send_json_success(array(
					'url' => $download_folders['url'].$updated_file,
					'message' => __('Import run successfully.', 'vp-woo-pont')
				));
			} else {
				wp_send_json_error(array(
					'message' => __('Unable to run the import tool for some reason.', 'vp-woo-pont')
				));
			}

		}

		public static function check_if_sync_needed($provider) {

			//Get saved value
			$saved_file = get_option('_vp_woo_pont_file_'.$provider);

			//Get enabled providers
			$enabled_providers = get_option('vp_woo_pont_enabled_providers');

			//Check enabled providers
			$enabled = false;
			foreach ($enabled_providers as $enabled_provider) {
				if($provider == 'postapont') {
					if (strpos($enabled_provider, 'postapont') !== false) {
						$enabled = true;
					}
				} else {
					if($provider == $enabled_provider) {
						$enabled = true;
					}
				}
			}

			if($saved_file && !$enabled) {
				return false;
			} else {
				return true;
			}

		}

	}

 VP_Woo_Pont_Import_Database::init();

endif;
