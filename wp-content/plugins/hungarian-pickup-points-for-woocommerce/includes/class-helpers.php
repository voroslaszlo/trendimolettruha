<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'VP_Woo_Pont_Helpers', false ) ) :

	class VP_Woo_Pont_Helpers {

		//Get options stored
		public static function get_option($key, $default = '') {
			$settings = get_option( 'woocommerce_vp_pont_settings', null );
			$value = $default;

			if($settings && isset($settings[$key]) && !empty($settings[$key])) {
				$value = $settings[$key];
			} else if(get_option($key)) {
				$value = get_option($key);
			}

			return apply_filters('vp_woo_pont_get_option', $value, $key);
		}

		//Get IPN url
		public function get_webhook_url($provider) {
			$ipn_id = add_option( '_vp_woo_pont_'.$provider.'_webhook_url', substr(md5(rand()),5)); //this will only store it if doesn't exists yet
			$url = get_admin_url().'admin-post.php?action=vp_woo_pont_webhook_'.$provider.'&id='.get_option('_vp_woo_pont_'.$provider.'_webhook_url').'&provider='.$provider;
			return $url;
		}

		//Helper function to query supported providers
		public static function get_external_provider_groups() {
			return array(
				'foxpost' => __('Foxpost', 'vp-woo-pont'),
				'postapont' => __('Postapont', 'vp-woo-pont'),
				'packeta' => __('Packeta', 'vp-woo-pont'),
				'sprinter' => __('Pick Pack Pont', 'vp-woo-pont'),
				'expressone' => __('Express One', 'vp-woo-pont'),
				'gls' => __('GLS ParcelShop', 'vp-woo-pont'),
				'dpd' => __('DPD', 'vp-woo-pont'),
				'sameday' => __('Easybox', 'vp-woo-pont'),
			);
		}

		//Helper function to query supported providers
		public static function get_supported_providers() {
			return array(
				'foxpost' => __('Foxpost', 'vp-woo-pont'),
				'postapont_10' => __('Posta', 'vp-woo-pont'),
				'postapont_20' => __('Mol', 'vp-woo-pont'),
				'postapont_30' => __('Csomagautomata', 'vp-woo-pont'),
				'postapont_50' => __('Coop', 'vp-woo-pont'),
				'postapont_70' => __('Mediamarkt', 'vp-woo-pont'),
				'packeta' => __('Packeta', 'vp-woo-pont'),
				'sprinter' => __('Pick Pack Pont', 'vp-woo-pont'),
				'expressone' => __('Express One', 'vp-woo-pont'),
				'gls' => __('GLS ParcelShop', 'vp-woo-pont'),
				'dpd' => __('DPD', 'vp-woo-pont'),
				'sameday' => __('Easybox', 'vp-woo-pont'),
				'custom' => self::get_option('custom_title', __( 'Store Pickup', 'vp-woo-pont' ))
			);
		}

		//Returns providers that supports home delivery
		public static function get_supported_providers_for_home_delivery() {
			return array(
				'gls' => __('GLS', 'vp-woo-pont'),
				'posta' => __('MPL', 'vp-woo-pont'),
				'packeta' => __('Packeta', 'vp-woo-pont'),
				'foxpost' => __('Foxpost', 'vp-woo-pont'),
				'dpd' => __('DPD', 'vp-woo-pont'),
			);
		}

		//Helper function to get download folder paths and filenames
		public static function get_download_folder($type = 'postapont') {
			$upload_dir = wp_upload_dir( null, false );
			$basedir = $upload_dir['basedir'] . '/vp-woo-pont-db/';
			$baseurl = set_url_scheme($upload_dir['baseurl']).'/vp-woo-pont-db/';
			$random_file_name = substr(md5(rand()),5);
			$json_file_name = implode( '-', array( $type, $random_file_name ) ).'.json';
			return array('name' => $json_file_name, 'dir' => $basedir, 'path' => $basedir.$json_file_name, 'url' => $baseurl);
		}

		//Returns all json file url-s
		public static function get_json_files() {
			$paths = array();

			//Get a download folder for the urls
			$download_folders = self::get_download_folder();

			//Only load enabled providers
			$enabled_providers = self::get_option('vp_woo_pont_enabled_providers', array());

			//Loop through each pont types
			foreach (self::get_external_provider_groups() as $pont_type => $name) {

				//Check if a file name is stored
				$filename = get_option('_vp_woo_pont_file_'.$pont_type);

				//If file name exists, append to results
				if($filename && (strpos($pont_type, 'postapont') !== false || in_array($pont_type, $enabled_providers))) {
					$paths[] = array(
						'type' => $pont_type,
						'url' => $download_folders['url'].$filename,
						'filename' => $filename
					);
				}

			}

			return $paths;
		}

		public static function get_cart_volume() {
			// Initializing variables
			$volume = $rate = 0;

			// Get the dimetion unit set in Woocommerce
			$dimension_unit = get_option( 'woocommerce_dimension_unit' );

			// Calculate the rate to be applied for volume in m3
			if ( $dimension_unit == 'mm' ) {
				$rate = pow(10, 9);
			} elseif ( $dimension_unit == 'cm' ) {
				$rate = pow(10, 6);
			} elseif ( $dimension_unit == 'm' ) {
				$rate = 1;
			}

			if( $rate == 0 ) return false; // Exit

			// Loop through cart items
			foreach(WC()->cart->get_cart() as $cart_item) {
				// Get an instance of the WC_Product object and cart quantity
				$product = $cart_item['data'];
				$qty     = $cart_item['quantity'];

				// Get product dimensions
				$length = $product->get_length();
				$width  = $product->get_width();
				$height = $product->get_height();

				// Calculations a item level
				if($length && $width && $height) {
					$volume += $length * $width * $height * $qty;
				}
			}

			return $volume / $rate;
		}

		//Calculate shipping costs based on settings
		public static function calculate_shipping_costs() {

			//Get weight
			$cart_details = VP_Woo_Pont_Conditions::get_cart_details('pricings');

			//Get default cost
			$default_cost = self::get_option('cost', 0);

			//Get available providers
			$enabled_providers = get_option('vp_woo_pont_enabled_providers');

			//Get supported providers for labels
			$supported_providers = self::get_supported_providers();

			//Get custom costs table
			$costs = get_option('vp_woo_pont_pricing');
			if(!$costs) $costs = array();

			//Loop through each cost setup and see if theres a match
			$matched_provider_prices = array();
			foreach ($costs as $cost_id => $cost) {

				//Get the price
				$price = $cost['cost'];

				//Check for conditions if needed
				if($cost['conditional']) {

					//Loop through each condition and see if its a match
					$condition_is_a_match = VP_Woo_Pont_Conditions::match_conditions($costs, $cost_id, $cart_details);

					//If no match, skip to the next one
					if(!$condition_is_a_match) continue;

				}

				//If its not conditional, or there is a conditional match, just simply append provider prices
				foreach ($cost['providers'] as $provider) {

					//Make it an array, so if multiple prices are matched, we can later decide which one to use
					if(!isset($matched_provider_prices[$provider])) {
						$matched_provider_prices[$provider] = array();
					}

					//Append to matched prices array grouped by provider
					$matched_provider_prices[$provider][] = $price;

				}

			}

			//Get available providers, and if one is missing from the matched prices, use the default price instead.
			//If theres no default price, that means the specific shipping provider is unavailable
			foreach ($enabled_providers as $enabled_provider) {
				if(!isset($matched_provider_prices[$enabled_provider])) {
					$matched_provider_prices[$enabled_provider] = array($default_cost);
				}
			}

			//Get info if we need lowest or highest price if theres multiple matches
			$cost_logic = self::get_option('cost_logic', 'low');

			//Loop through options and create a new array with single values, including taxes
			$provider_costs = array();
			foreach ($matched_provider_prices as $provider_id => $costs) {
				$cost = min($costs);
				if($cost_logic != 'low') {
					$cost = max($costs);
				}

				//Allow plugins to customize
				$cost = apply_filters( 'vp_woo_pont_shipping_cost', $cost, $matched_provider_prices, $provider_id);

				//Calculate taxes, if enabled
				$tax = array();
				if(wc_tax_enabled()) {
					$tax = WC_Tax::calc_shipping_tax( $cost, WC_Tax::get_shipping_tax_rates() );
				}
				if(!$tax) $tax = array();

				//Check if a free coupon is used and can overwrite the cost
				if(self::order_has_free_shipping_coupon() && in_array($provider_id, self::get_option('vp_woo_pont_free_shipping', array()))) {
					$cost = 0;
					$tax = array(0);
				}

				//If lower than 0, hide the provider
				if($cost<0) continue;

				//Calculate taxes too
				$provider_costs[$provider_id] = array(
					'formatted_net' => wc_price($cost),
					'formatted_gross' => wc_price($cost+array_sum($tax)),
					'net' => $cost,
					'tax' => array_sum($tax),
					'label' => $supported_providers[$provider_id]
				);
			}

			return apply_filters( 'vp_woo_pont_provider_costs', $provider_costs);
		}

		public static function get_shipping_cost() {
			$shipping_cost = array();

			//Get selected point
			$selected_pont = WC()->session->get( 'selected_vp_pont' );

			//If a point is selected, query the prices
			if($selected_pont) {

				//Find the prices
				$shipping_costs = self::calculate_shipping_costs();

				//Find the related provider to the selected point
				if(isset($shipping_costs[$selected_pont['provider']])) {
					$shipping_cost = $shipping_costs[$selected_pont['provider']];
				}

			} else {

				//If point is not selected yet, and only one price set, use that
				$shipping_costs = self::calculate_shipping_costs();
				if(count($shipping_costs) == 1) {
					$shipping_cost = current($shipping_costs);
				}

			}

			return $shipping_cost;
		}

		public static function get_payment_methods() {
			$available_gateways = WC()->payment_gateways->payment_gateways();
			$payment_methods = array();
			foreach ($available_gateways as $available_gateway) {
				if($available_gateway->enabled == 'yes') {
					$payment_methods[$available_gateway->id] = $available_gateway->title;
				}
			}
			return $payment_methods;
		}

		public static function pricing_has_payment_method_condition() {
			$costs = get_option('vp_woo_pont_pricing');
			$has_payment_method_condition = false;
			foreach ($costs as $cost_id => $cost) {

				//Check for conditions if needed
				if($cost['conditional']) {
					foreach ($cost['conditions'] as $condition_id => $condition) {
						if($condition['category'] == 'payment_method') {
							$has_payment_method_condition = true;
							break;
						}
					}
				}
			}

			return $has_payment_method_condition;
		}

		//Get shipping methods, except pont methods
		public static function get_available_shipping_methods() {
			$active_methods = array();
			$custom_zones = WC_Shipping_Zones::get_zones();
			$worldwide_zone = new WC_Shipping_Zone( 0 );
			$worldwide_methods = $worldwide_zone->get_shipping_methods();

			foreach ( $custom_zones as $zone ) {
				$shipping_methods = $zone['shipping_methods'];
				foreach ($shipping_methods as $shipping_method) {
					if ( isset( $shipping_method->enabled ) && 'yes' === $shipping_method->enabled ) {
						$method_title = $shipping_method->title;
						if(strpos($shipping_method->id, 'vp_pont') !== 0) {
							$active_methods[$shipping_method->id.':'.$shipping_method->instance_id] = $method_title.' ('.$zone['zone_name'].' zóna)';
						}
					}
				}
			}

			foreach ($worldwide_methods as $shipping_method_id => $shipping_method) {
				if ( isset( $shipping_method->enabled ) && 'yes' === $shipping_method->enabled ) {
					$method_title = $shipping_method->title;
					if(strpos($shipping_method->id, 'vp_pont') !== 0) {
						$active_methods[$shipping_method->id.':'.$shipping_method->instance_id] = $method_title.' (Worldwide)';
					}
				}
			}

			return $active_methods;
		}

		//Currency converter
		//Query MNB for new rates
		public static function convert_currency($from, $to, $amount) {
			$transient_name = 'vp_woo_pont_currency_rate_'.strtolower($to);
			$exchange_rate = get_transient( $transient_name );
			if(!$exchange_rate) {
				$client = new SoapClient("http://www.mnb.hu/arfolyamok.asmx?wsdl");
				$soap_response = $client->GetCurrentExchangeRates()->GetCurrentExchangeRatesResult;
				$xml = simplexml_load_string($soap_response);
				$compare = $to;
				if($from != 'HUF') $compare = $from;

				foreach($xml->Day->Rate as $rate) {
					$attributes = $rate->attributes();
					if((string)$attributes->curr == $compare) {
						$exchange_rate = (string) $rate;
						$exchange_rate = str_replace(',','.',$exchange_rate);
						set_transient( $transient_name, $exchange_rate, 60*60*12 );
					}
				}
			}

			//Just to revent possible errors
			if(empty($exchange_rate)) $exchange_rate = 1;

			if($from != 'HUF') {
				return $amount*$exchange_rate;
			}

			return $amount/$exchange_rate;
		}

		//Get provider from order
		public static function get_provider_from_order($order) {

			//Check if an order id was submitted instead of an order
			if(is_int($order)) $order = wc_get_order($order);

			//Check for the meta first, this is stored on checkout when the user selects a pickup point provider
			$provider = $order->get_meta('_vp_woo_pont_provider');

			//If no provider set, use default one set based on shipping method
			if(!$provider) $provider = self::get_paired_provider($order, false);

			//And return it. This is false if no provider has been set
			return $provider;
		}

		//Helper function to get paired provider for shipping method
		public static function get_paired_provider($order, $default = 'gls') {

			//Get saved settings
			$home_delivery_pairs = get_option('vp_woo_pont_home_delivery', array());

			//Get shipping method id
			$shipping_method = '';
			$shipping_methods = $order->get_shipping_methods();
			if($shipping_methods) {
				foreach( $shipping_methods as $shipping_method_obj ){
					$shipping_method = $shipping_method_obj->get_method_id().':'.$shipping_method_obj->get_instance_id();
				}
			}

			//Check if we have a provider set for the shipping method
			if($shipping_method != '' && isset($home_delivery_pairs[$shipping_method]) && $home_delivery_pairs[$shipping_method] != '') {
				return $home_delivery_pairs[$shipping_method];
			} else {
				return $default;
			}

		}

		public static function order_has_free_shipping_coupon() {
			$has_free_shipping = false;
			$applied_coupons = WC()->cart->get_applied_coupons();
			foreach( $applied_coupons as $coupon_code ){
				$coupon = new WC_Coupon($coupon_code);
				if($coupon->get_free_shipping()){
					$has_free_shipping = true;
					break;
				}
			}
			return $has_free_shipping;
		}

		public static function is_provider_configured($provider) {
			$configured = false;
			$field_to_check = '';

			if($provider == 'dpd') $field_to_check = 'dpd_password';
			if($provider == 'foxpost') $field_to_check = 'foxpost_password';
			if($provider == 'gls') $field_to_check = 'gls_password';
			if($provider == 'packeta') $field_to_check = 'packeta_api_password';
			if($provider == 'posta') $field_to_check = 'posta_api_password';

			if(self::get_option($field_to_check)) {
				$configured = true;
			}

			return $configured;
		}

		public static function get_pdf_label_positions($provider) {

			//Setup PDF details
			$positions = array(
				'format' => false,
				'sections' => 0,
				'x' => array(),
				'y' => array()
			);

			//Get label size from settings
			$label_size = self::get_option($provider.'_sticker_size');

			//For GLS
			if($provider == 'gls') {
				$positions['sections'] = 4;

				//Portrait with 4 labels below each other
				if($label_size == 'A4_4x1') {
					$positions['format'] = 'A4';
					$positions['x'] = array(0, 0, 0, 0);
					$positions['y'] = array(0, 74, 148, 222);
				}

				//Landscape with 4 labels in a grid
				if($label_size == 'A4_2x2') {
					$positions['format'] = 'A4-L';
					$positions['x'] = array(0, 0, 148, 148);
					$positions['y'] = array(0, 105, 0, 105);
				}

			}

			//For Foxpost
			if($provider == 'foxpost') {

				//Portrait with 8 labels on a grid
				if($label_size == 'a7') {
					$positions['sections'] = 8;
					$positions['format'] = 'A4';
					$positions['x'] = array(0, 105, 0, 105, 0, 105, 0, 105);
					$positions['y'] = array(0, 0, 74, 74, 148, 148, 222, 222);
				}

				//Portrait with 4 labels in a grid
				if($label_size == 'a6') {
					$positions['sections'] = 4;
					$positions['format'] = 'A4-L';
					$positions['x'] = array(0, 0, 148, 148);
					$positions['y'] = array(0, 105, 0, 105);
				}

				//Portrait with 2 labels below each other
				if($label_size == 'a5') {
					$positions['sections'] = 2;
					$positions['format'] = 'A4';
					$positions['x'] = array(0, 0);
					$positions['y'] = array(0, 148);
				}

			}

			//For Foxpost
			if($provider == 'packeta') {

				//Portrait with 8 labels on a grid
				if($label_size == 'A7 on A4') {
					$positions['sections'] = 8;
					$positions['format'] = 'A4';
					$positions['x'] = array(0, 105, 0, 105, 0, 105, 0, 105);
					$positions['y'] = array(0, 0, 74, 74, 148, 148, 222, 222);
				}

				//Portrait with 4 labels on a grid
				if($label_size == 'A6 on A4') {
					$positions['sections'] = 4;
					$positions['format'] = 'A4';
					$positions['x'] = array(0, 105, 0, 105);
					$positions['y'] = array(0, 0, 148, 148);
				}

				//Portrait with 8 labels on a grid
				if($label_size == '105x35mm on A4') {
					$positions['sections'] = 16;
					$positions['format'] = 'A4';
					$positions['x'] = array(0, 105, 0, 105, 0, 105, 0, 105, 0, 105, 0, 105, 0, 105, 0, 105);
					$positions['y'] = array(0, 0, 37, 37, 74, 74, 111, 111, 148, 148, 185, 185, 222, 222, 259, 259);
				}

			}

			//For Sameday
			if($provider == 'sameday') {
				//Portrait with 4 labels on a grid
				if($label_size == 'A6 on A4') {
					$positions['sections'] = 4;
					$positions['format'] = 'A4';
					$positions['x'] = array(0, 105, 0, 105);
					$positions['y'] = array(0, 0, 148, 148);
				}
			}

			//For Foxpost
			if($provider == 'dpd') {

				//Portrait with 4 labels on a grid
				$positions['sections'] = 4;
				$positions['format'] = 'A4';
				$positions['x'] = array(0, 105, 0, 105);
				$positions['y'] = array(0, 0, 148, 148);

			}

			return apply_filters('vp_woo_pont_merged_pdf_parameters', $positions, $provider, $label_size);

		}

	}

endif;
