<?php
defined( 'ABSPATH' ) || exit;

$pro_required = false;
$pro_icon = false;
if(!VP_Woo_Pont_Pro::is_pro_enabled()) {
	$pro_required = true;
	$pro_icon = '<i class="vp_woo_pont_pro_label">PRO</i>';
}

$settings = array(
	'section_general' => array(
		'title' => __( 'General settings', 'vp-woo-pont' ),
		'type' => 'vp_woo_pont_settings_title',
		'description' => __( 'You need to take a look at these option at least to get started.', 'vp-woo-pont' ),
	),
	'title'      => array(
		'title'       => __( 'Method title', 'vp-woo-pont' ),
		'type'        => 'text',
		'description' => __( 'This controls the title which the user sees during checkout.', 'vp-woo-pont' ),
		'default'     => _x( 'Pickup point', 'frontend', 'vp-woo-pont' ),
		'desc_tip'    => true,
	),
	'tax_status' => array(
		'title'   => __( 'Tax status', 'vp-woo-pont' ),
		'type'    => 'select',
		'class'   => 'wc-enhanced-select',
		'default' => 'taxable',
		'options' => array(
			'taxable' => __( 'Taxable', 'vp-woo-pont' ),
			'none'    => _x( 'None', 'Tax status', 'vp-woo-pont' ),
		),
	),
	'enabled_providers' => array(
		'type' => 'vp_woo_pont_settings_checkboxes',
		'title' => __( 'Enabled providers', 'vp-woo-pont' ),
		'options' => $this->get_available_providers(),
		'default' => array('custom', 'foxpost', 'postapont')
	),
	'custom_title'      => array(
		'title'       => __( 'Custom provider name', 'vp-woo-pont' ),
		'type'        => 'text',
		'description' => __( 'If you setup your own pickup points, this will be the name of that category.', 'vp-woo-pont' ),
		'default'     => __( 'Store Pickup', 'vp-woo-pont' ),
		'desc_tip'    => true,
	),
	'cost'       => array(
		'title'             => __( 'Default cost', 'vp-woo-pont' ),
		'type'              => 'text',
		'placeholder'       => '',
		'description'       => __('Enter a default price for this shipping option. You can overwrite this later based on conditional logic.', 'vp-woo-pont'),
		'default'           => '0',
		'desc_tip'          => true,
		'sanitize_callback' => array( $this, 'sanitize_cost' ),
	),
	'detailed_cost' => array(
		'title' => __('Detailed cost', 'vp-woo-pont'),
		'type' => 'vp_woo_pont_settings_pricing_table'
	),
	'cost_logic' => array(
		'title'   => __( 'Multiple cost logic', 'vp-woo-pont' ),
		'type'    => 'select',
		'class'   => 'wc-enhanced-select',
		'default' => 'low',
		'description'       => __('If theres multiple matches for the shipping cost, use the lowest or the highest cost.', 'vp-woo-pont'),
		'options' => array(
			'low' => __( 'Lowest', 'vp-woo-pont' ),
			'high'    => _x( 'Highest', 'Tax status', 'vp-woo-pont' ),
		),
	),
	'free_shipping' => array(
		'type' => 'vp_woo_pont_settings_checkboxes',
		'title' => __( 'Free shipping coupon applies to the following providers', 'vp-woo-pont' ),
		'options' => $this->get_available_providers(),
		'default' => array()
	),
	'points_manager' => array(
		'title' => _x('Pickup points', 'admin', 'vp-woo-pont'),
		'type' => 'vp_woo_pont_settings_points_table',
		'description' => __('You can setup your own pickup points too. Make sure you enter a unique ID(used internally only). These points will show up under the "Custom Provider" name.', 'vp-woo-pont')
	),
	'debug' => array(
		'title' => __( 'Developer mode', 'vp-woo-pont' ),
		'type' => 'checkbox',
		'desc_tip' => __( 'If turned on, the data sent to the provider during when createing a label will be logged in WooCommerce / Status / Logs. Can be used to debug issues.', 'vp-woo-pont' ),
	),
	'uninstall' => array(
		'title' => __( 'Delete settings on uninstall', 'vp-woo-pont' ),
		'type' => 'checkbox',
		'desc_tip' => __( 'If turned on, during plugin uninstall it will also delete all settings from the database', 'vp-woo-pont' ),
	),
	'section_automation' => array(
		'title' => __( 'Label generator', 'vp-woo-pont' ).$pro_icon,
		'type' => 'vp_woo_pont_settings_title',
		'description' => __( 'Settings related to automations and labels. Every function in this section will only work if you have the PRO version activated.', 'vp-woo-pont' ),
	),
	'home_delivery_providers' => array(
		'type' => 'vp_woo_pont_settings_home_delivery',
		'title' => __('Home delivery providers', 'vp-woo-pont'),
		'description' => __( 'Pair your existing shipping methods with home delivery providers. If you are using the automated label generation, it will use this provider to generate the label if the shipping method is a match.', 'vp-woo-pont' ),
	),
	'automations' => array(
		'type' => 'vp_woo_pont_settings_automations',
		'title' => __('Automations', 'vp-woo-pont')
	),
	'bulk_download_zip' => array(
		'title'    => __( 'Create a ZIP file during bulk download', 'vp-woo-pont' ),
		'type'     => 'checkbox',
		'disabled' => (!class_exists('ZipArchive')),
		'desc_tip' => __( 'If you want to download multiple shipping labels at once, this option will create a ZIP file with separate PDF files(the default option will merge all invoices into a single PDF).', 'vp-woo-pont' ),
		'description' => $this->get_bulk_zip_error()
	),
	'label_reference_number' => array(
		'type' => 'select',
		'class' => 'wc-enhanced-select',
		'title' => __( 'Reference number', 'vp-woo-pont' ),
		'desc_tip' => __( 'With most providers, theres an extra field where you can store a reference number to connect the label with an order. You can select what value to use in this field.', 'vp-woo-pont' ),
		'default' => 'order_number',
		'options' => array(
			'order_number' => __( 'Order number', 'vp-woo-pont' ),
			'order_id' => __( 'Order ID', 'vp-woo-pont' ),
			'invoice_number' => __( 'Invoice number', 'vp-woo-pont' ),
		),
	),
	'auto_order_status' => array(
		'type' => 'select',
		'class' => 'wc-enhanced-select',
		'title' => __( 'Change order status after label generated', 'vp-woo-pont' ),
		'options' => $this->get_order_statuses(__('None', 'Order status after label generated', 'vp-woo-pont')),
		'description' => __( 'If a label was generated for the order, change the order status automatically.', 'vp-woo-pont' ),
	),

	'section_tracking' => array(
		'title' => __( 'Shipment tracking', 'vp-woo-pont' ).$pro_icon,
		'type' => 'vp_woo_pont_settings_title',
		'description' => __( 'Settings related to shipment tracking. Every function in this section will only work if you have the PRO version activated.', 'vp-woo-pont' ),
	),
	'email_tracking_number' => array(
		'type' => 'multiselect',
		'title' => __( 'Tracking number in e-mails', 'vp-woo-pont' ),
		'class' => 'wc-enhanced-select',
		'default' => array('customer_completed_order'),
		'options' => array(),
		'description' => __('Selected the e-mails where you want to include the tracking number and tracking link.', 'vp-woo-pont').'<span id="vp_woo_pont_load_email_ids_nonce" data-nonce="'.wp_create_nonce("vp_woo_pont_load_email_ids").'"></span>'
	),
	'email_tracking_number_pos' => array(
		'type' => 'select',
		'class' => 'wc-enhanced-select',
		'title' => __( 'E-mail text position', 'vp-woo-pont' ),
		'desc_tip' => __( 'Where should the tracking numbers be included in the emails?', 'vp-woo-pont' ),
		'default' => 'beginning',
		'options' => array(
			'beginning' => __( 'At the beginning', 'vp-woo-pont' ),
			'end' => __( 'At the end', 'vp-woo-pont' ),
		),
	),
	'email_tracking_number_desc' => array(
		'title' => __( 'E-mail text', 'vp-woo-pont' ),
		'type' => 'textarea',
		'placeholder' => __('You can track the order by clicking on the tracking number: {tracking_number}', 'vp-woo-pont'),
		'desc_tip' => __('This text will be included in the selected e-mails. Use the {tracking_number} replacement code for the actual tracking link. This will be replaced with the package number as a link, redirecting to the providers tracking page.', 'vp-woo-pont')
	),
	'tracking_my_account' => array(
		'type'     => 'checkbox',
		'title' => __( 'Tracking numbers in My Orders', 'vp-woo-pont' ),
		'description' => __( 'If turned on, the tracking number and the tracking link will be visible in My Account / Order Details.', 'vp-woo-pont' ),
	),
	'order_tracking' => array(
		'type'     => 'checkbox',
		'title' => __( 'Fetch tracking information automatically', 'vp-woo-pont' ),
		'description' => __( 'If turned on, the tracking informations will be downloaded hourly from supported providers in the background. The tracking info is visible on the orders table(custom cell) and on the order details page.', 'vp-woo-pont' ),
	),
	'tracking_automations' => array(
		'type' => 'vp_woo_pont_settings_tracking_automations',
		'title' => __( 'Tracking automations', 'vp-woo-pont' )
	)
);

return apply_filters('vp_woo_pont_global_settings', $settings);
