<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Load PDF API
use iio\libmergepdf\Merger;
use iio\libmergepdf\Driver\TcpdiDriver;

if ( ! class_exists( 'VP_Woo_Pont_Labels', false ) ) :

	class VP_Woo_Pont_Labels {
		public $bulk_actions = array();
		public $supported_providers = array('foxpost', 'packeta', 'gls', 'postapont_10', 'postapont_20', 'postapont_30', 'postapont_50', 'postapont_70', 'posta', 'dpd', 'sameday');
		public $supports_bulk_printing = array();
		public $supports_merged_printing = array('foxpost', 'packeta', 'gls', 'dpd');

		public function __construct() {

			$is_pro = VP_Woo_Pont_Pro::is_pro_enabled();

			//Set bulk action values
			$this->bulk_actions = array(
				'vp_woo_pont_generate_labels' => __( 'Generate shipping labels', 'vp-woo-pont' ),
				'vp_woo_pont_print_labels' => __( 'Print shipping labels', 'vp-woo-pont' ),
				'vp_woo_pont_download_labels' => __( 'Download shipping labels', 'vp-woo-pont' ),
			);

			//Add bulk action to print labels
			if($is_pro) {
				add_filter( 'bulk_actions-edit-shop_order', array( $this, 'add_bulk_options'), 20, 1);
				add_filter( 'handle_bulk_actions-edit-shop_order', array( $this, 'handle_bulk_actions'), 10, 3 );
				add_action( 'admin_notices', array( $this, 'bulk_actions_results') );
			}

			//Create metabox for labels on order edit page
			add_action( 'add_meta_boxes', array( $this, 'add_metabox' ), 10, 2 );

			//Add label and package number to orders table
			add_filter( 'manage_edit-shop_order_columns', array( $this, 'add_listing_column' ) );
			add_action( 'manage_shop_order_posts_custom_column', array( $this, 'add_listing_actions' ), 20 );
			add_filter( 'default_hidden_columns', array( $this, 'default_hidden_columns' ), 10, 2 );

			//Ajax function to generate and remove labels
			add_action( 'wp_ajax_vp_woo_pont_generate_label', array( $this, 'ajax_generate_label' ) );
			add_action( 'wp_ajax_vp_woo_pont_void_label', array( $this, 'ajax_void_label' ) );
			add_action( 'wp_ajax_vp_woo_pont_print_labels', array( $this, 'ajax_print_labels' ) );

		}

		public function add_bulk_options( $actions ) {
			$actions['vp_woo_pont_generate_labels'] = __( 'Generate shipping labels', 'vp-woo-pont' );
			$actions['vp_woo_pont_print_labels'] = __( 'Print shipping labels', 'vp-woo-pont' );
			$actions['vp_woo_pont_download_labels'] = __( 'Download shipping labels', 'vp-woo-pont' );
			return $actions;
		}

		public function handle_bulk_actions( $redirect_to, $action, $post_ids ) {

			//Check if we are processing a vp_woo_pont bulk request
			if( strpos($action, 'vp_woo_pont') !== false ) {
				$query_args = array_keys($this->bulk_actions);
				$action_params = explode('_', $action);
				$action = $action_params[3];

				//If we are downloading or printing
				if ( in_array($action, array('print', 'download'))) {

					//Remove existing params from url
					$redirect_to = remove_query_arg(array('vp_woo_pont_bulk_action', 'vp_woo_pont_results_bulk_count', 'vp_woo_pont_results_bulk_pdf'), $redirect_to);

					//Create bulk pdf file
					$bulk_pdf_file = $this->get_pdf_file_path('bulk', 0);

					if($action == 'download' && VP_Woo_Pont_Helpers::get_option('bulk_download_zip', 'no') == 'yes') {

						//Create an object from the ZipArchive class.
						$zipArchive = new ZipArchive();

						//The full path to where we want to save the zip file.
						$bulk_pdf_file['path'] = str_replace('.pdf', '.zip', $bulk_pdf_file['path']);
						$bulk_pdf_file['name'] = str_replace('.pdf', '.zip', $bulk_pdf_file['name']);

						//Call the open function.
						$status = $zipArchive->open($bulk_pdf_file['path'], ZipArchive::CREATE | ZipArchive::OVERWRITE);

						//An array of files that we want to add to our zip archive.
						foreach ( $post_ids as $order_id ) {
							$order = wc_get_order($order_id);
							$pdf_file = $this->generate_download_link($order, true);
							if($pdf_file && file_exists($pdf_file)) {
								$new_filename = substr($pdf_file, strrpos($pdf_file,'/') + 1);
								$zipArchive->addFile($pdf_file, $new_filename);
								$processed[] = $order_id;
							}
						}

						//Bail if theres no $documents
						if(!$processed) {
							return $redirect_to;
						}

						//Finally, close the active archive.
						$zipArchive->close();

					} else {

						//Create merged pdf
						$bulk_results = $this->create_bulk_pdf($post_ids);

						//Bail if theres no $documents
						if(!$bulk_results) {
							return $redirect_to;
						}

						//Setup the necessary variables
						$processed = $bulk_results['processed'];
						$bulk_pdf_file = $bulk_results['bulk_pdf_file'];

					}

					//Set redirect url that will show the download message notice
					$redirect_to = add_query_arg( array('vp_woo_pont_bulk_action' => $action, 'vp_woo_pont_results_bulk_count' => $processed, 'vp_woo_pont_results_bulk_pdf' => urlencode($bulk_pdf_file['name'])), $redirect_to );
					return $redirect_to;

				} else if ($action == 'generate') {

					//Remove existing params from url
					$redirect_to = remove_query_arg($query_args, $redirect_to);
					$redirect_to = remove_query_arg(array('vp_woo_pont_bulk_action', 'vp_woo_pont_results_bulk_count', 'vp_woo_pont_results_bulk_pdf'), $redirect_to);

					//Grouped orders
					$needs_processing = array();
					$processed = array();

					//Check if we need to defer
					$defer_limit = apply_filters('vp_woo_pont_generate_defer_limit', 5);

					//Loop throguh order ids, check if its vp pont and group by providers and prepare data for generating labels
					foreach ($post_ids as $order_id) {

						//Check if its a vp_pont order
						$provider = VP_Woo_Pont_Helpers::get_provider_from_order($order_id);

						//If it is, check if a label is generated already or not
						if($provider && !$this->is_label_generated($order_id) && in_array($provider, $this->supported_providers)) {

							//For postapont orders, just use the postapont part from the provider name, its the same for all
							if(strpos($provider, 'postapont') !== false) {
								$provider = 'posta';
							}

							//Append to future processing
							$needs_processing[] = array('order_id' => $order_id, 'provider' => $provider);

						}

					}

					//If we need to process more than the defer limit, do it in the backgorund
					if(count($needs_processing) > $defer_limit) {
						foreach ( $needs_processing as $label_data ) {
							WC()->queue()->add( 'vp_woo_pont_generate_label_async', array( 'order_id' => $label_data['order_id'], 'provider' => $label_data['provider'] ), 'vp-woo-pont' );
							$processed[] = $label_data['order_id'];
						}
					} else {
						foreach ( $needs_processing as $label_data ) {
							$this->generate_label($label_data['order_id'], $label_data['provider']);
							$processed[] = $label_data['order_id'];
						}
					}

					//Set redirect url that will show the download message notice
					$redirect_to = add_query_arg( array('vp_woo_pont_bulk_action' => $action, 'vp_woo_pont_bulk_action_results' => implode('|', $processed)), $redirect_to );
					return $redirect_to;

				} else {
					return $redirect_to;
				}

			} else {
				return $redirect_to;
			}

		}

		public function bulk_actions_results() {
			if(isset($_REQUEST['vp_woo_pont_bulk_action'])) {

				//If its a print or download request
				$action = sanitize_text_field($_REQUEST['vp_woo_pont_bulk_action']);

				if ( in_array($action, array('print', 'download')) ) {
					$print_count = intval( $_REQUEST['vp_woo_pont_results_bulk_count'] );

					$paths = $this->get_pdf_file_path('bulk', 0);
					$pdf_file_name = esc_attr( $_REQUEST['vp_woo_pont_results_bulk_pdf'] );
					$pdf_file_url = $paths['baseurl'].$pdf_file_name;

					include( dirname( __FILE__ ) . '/views/html-notice-bulk.php' );
				}

				if ( $action == 'generate') {
					$documents = explode('|', $_REQUEST['vp_woo_pont_bulk_action_results']);
					include( dirname( __FILE__ ) . '/views/html-notice-bulk.php' );
				}

			}
		}

		public function generate_label($order_id, $provider = false) {

			//Create response object
			$response = array();
			$response['error'] = false;
			$response['messages'] = array();

			//Get order data
			$order = wc_get_order($order_id);

			//If provider not set, find it
			if(!$provider) {
				$provider = VP_Woo_Pont_Helpers::get_provider_from_order($order);
			}

			//If still not set, check for paramter
			if(isset($_POST['provider'])) {
				$provider = sanitize_text_field($_POST['provider']);
			}

			//Fix for postaponts... provider id
			if(strpos($provider, 'postapont') !== false) {
				$provider = 'posta';
			}

			//Set label data
			$data = $this->prepare_order_data($order);
			$data = apply_filters('vp_woo_pont_prepare_order_data_for_label', $data, $order, $provider);

			//Try to generate the label with the provider class
			$label = VP_Woo_Pont()->providers[$provider]->create_label($data);

			//Check for errors
			if(is_wp_error($label)) {
				$response['error'] = true;
				$response['messages'][] = $label->get_error_message();

				//Log error message
				$order->add_order_note(sprintf(esc_html__('Unable to create shipping label. Error code: %s', 'vp-woo-pont'), urldecode($label->get_error_code())));

				return $response;
			}

			//Save provider if not saved yet
			if(!$order->get_meta('_vp_woo_pont_provider')) {
				$order->update_meta_data('_vp_woo_pont_provider', $provider);
			}

			//Save MPL closing info
			if(isset($label['mpl'])) {
				$order->update_meta_data('_vp_woo_pont_mpl_closed', 'no');
			}

			//If generation is still pending(foxpost for example only works async and takes a few minutes)
			//If all went good, store the PDF file and shipment info
			$order->update_meta_data('_vp_woo_pont_parcel_id', $label['id']);
			$order->update_meta_data('_vp_woo_pont_parcel_pdf', $label['pdf']);
			$order->update_meta_data('_vp_woo_pont_parcel_number', $label['number']);
			$order->save();

			//Save order note
			$order->add_order_note(sprintf(esc_html__('Shipping label generated successfully. ID number: %s', 'vp-woo-pont'), $label['number']));

			//Create response
			$response['number'] = $label['number'];
			$response['pdf'] = $this->generate_download_link($order);
			$response['messages'][] = esc_html__('Shipping label generated successfully.','vp-woo-pont');

			//So developers can hook in here
			do_action('vp_woo_pont_label_created', $order, $label, $provider);

			//Change order status if needed based on settings
			if(!$response['error']) {
				$target_status = VP_Woo_Pont_Helpers::get_option('auto_order_status', 'no');
				if($target_status != 'no') {
					$order->update_status($target_status, __( 'Order status updated, because a shipping label was generated.', 'vp_woo_pont' ));
					$order->save();
				}
			}

			//Return response
			return $response;

		}

		public function void_label($order_id, $provider = false) {

			//Create response object
			$response = array();
			$response['error'] = false;
			$response['messages'] = array();

			//Get order data
			$order = wc_get_order($order_id);

			//If provider not set, find it
			if(!$provider) $provider = VP_Woo_Pont_Helpers::get_provider_from_order($order);

			//Fix for postaponts... provider id
			if(strpos($provider, 'postapont') !== false) {
				$provider = 'posta';
			}

			//Set label data
			$data = $this->prepare_order_data($order);
			$data['parcel_number'] = $order->get_meta('_vp_woo_pont_parcel_number');
			$data['parcel_id'] = $order->get_meta('_vp_woo_pont_parcel_id');
			$data = apply_filters('vp_woo_pont_prepare_order_data_for_void_label', $data, $order, $provider);

			//Try to generate the label with the provider class
			$label = VP_Woo_Pont()->providers[$provider]->void_label($data);

			//Check for errors
			if(is_wp_error($label)) {
				$response['error'] = true;
				$response['messages'][] = $label->get_error_message();

				//Log error message
				$order->add_order_note(sprintf(esc_html__('Unable to void the shipping label. Error code: %s', 'vp-woo-pont'), urldecode($label->get_error_code())));

				return $response;
			}

			//If all went good, store the PDF file and shipment info
			$order->delete_meta_data('_vp_woo_pont_parcel_id');
			$order->delete_meta_data('_vp_woo_pont_parcel_pdf');
			$order->delete_meta_data('_vp_woo_pont_parcel_number');
			$order->delete_meta_data('_vp_woo_pont_parcel_pending');
			$order->delete_meta_data('_vp_woo_pont_parcel_info');
			$order->save();

			if(isset($label['deleted_locally'])) {

				//Save order note
				$order->add_order_note(sprintf(esc_html__("Shipping label removed from the order only. You still need to remove it in the provider's system. ID number: %s", 'vp-woo-pont'), $data['parcel_id']));

				//Create response
				$response['messages'][] = esc_html__("Shipping label removed from WooCommerce. You still need to remove it in the provider's system.",'vp-woo-pont');

			} else {

				//Save order note
				$order->add_order_note(sprintf(esc_html__('Shipping label removed successfully. ID number: %s', 'vp-woo-pont'), $data['parcel_id']));

				//Create response
				$response['messages'][] = esc_html__('Shipping label removed successfully.','vp-woo-pont');

			}

			//So developers can hook in here
			do_action('vp_woo_pont_label_removed', $order, $data, $provider);

			//Return response
			return $response;

		}

		//Function to download labels in bulk straight from the provider(not all supported yet, set in $supports_bulk_printing)
		public function download_labels($order_ids, $provider) {

			//Create response object
			$response = array();
			$response['error'] = false;
			$response['messages'] = array();

			//Setup data, both for ids and numbers, different data might be required by different providers
			$data = array(
				'parcel_ids' => array(),
				'parcel_numbers' => array()
			);

			//Get package ids and numbers
			foreach ($order_ids as $order_id) {
				$order = wc_get_order($order_id);
				$data['parcel_ids'][] = $order->get_meta('_vp_woo_pont_parcel_id');
				$data['parcel_numbers'][] = $order->get_meta('_vp_woo_pont_parcel_number');
			}

			//Try to download the labels with the provider class
			$labels = VP_Woo_Pont()->providers[$provider]->download_labels($data);

			//Validate
			if(is_wp_error($labels)) {
				$response['error'] = true;
				$response['messages'][] = $labels->get_error_message();
				return $response;
			}

			//Otherwise, return the PDF file created
			return $response['labels'] = $labels['pdf'];

		}

		//Function to create an array that can be used by all providers to generate labels
		public function prepare_order_data($order) {

			//Calculate weight
			$total_weight = 0;
			$order_items = $order->get_items();
			foreach ( $order_items as $item_id => $product_item ) {
				$product = $product_item->get_product();
				if($product) {
					$product_weight = $product->get_weight();
					$quantity = $product_item->get_quantity();
					if($product_weight) {
						$total_weight += floatval( $product_weight * $quantity );
					}
				}
			}

			//Compile data required for labels
			$data = array(
				'order_id' => $order->get_id(),
				'order_number' => $order->get_order_number(),
				'reference_number' => $this->get_reference_number($order),
				'order' => $order,
				'customer' => array(
					'name' => $order->get_formatted_shipping_full_name(),
					'phone' => $this->format_phone_number($order),
					'email' => $order->get_billing_email(),
					'first_name' => $order->get_shipping_first_name(),
					'last_name' => $order->get_shipping_last_name(),
					'company' => $order->get_shipping_company(),
					'name_with_company' => $order->get_formatted_shipping_full_name()
				),
				'package' => array(
					'total' => $order->get_total(),
					'total_rounded' => 5 * round($order->get_total() / 5),
					'weight' => $total_weight,
					'cod' => ($order->get_payment_method() == 'cod'),
					'currency' => $order->get_currency()
				),
				'point_id' => $order->get_meta('_vp_woo_pont_point_id')
			);

			//If empty, try to set it to billing
			if(!$data['customer']['name'] || $data['customer']['name'] == ' ') $data['customer']['name'] = $order->get_formatted_billing_full_name();
			if(!$data['customer']['first_name']) $data['customer']['first_name'] = $order->get_billing_first_name();
			if(!$data['customer']['last_name']) $data['customer']['last_name'] = $order->get_billing_last_name();
			if(!$data['customer']['company']) $data['customer']['company'] = $order->get_billing_company();
			if(!$data['customer']['name_with_company']) $data['customer']['name_with_company'] = $order->get_formatted_billing_full_name();

			if($data['customer']['company']) {
				$data['customer']['name_with_company'] .= ' ('.$data['customer']['company'].')';
			}

			//If custom options set(manual label generate)
			$package_count = 1;
			$pickup_date = '';
			$package_contents = '';
			if(isset($_POST['package_count'])) $package_count = sanitize_text_field($_POST['package_count']);
			if(isset($_POST['pickup_date'])) $pickup_date = sanitize_text_field($_POST['pickup_date']);
			if(isset($_POST['package_contents'])) $package_contents = sanitize_text_field($_POST['package_contents']);
			$data['options']['package_count'] = intval($package_count);
			$data['options']['pickup_date'] = $pickup_date;
			$data['options']['package_contents'] = $package_contents;

			return apply_filters('vp_woo_pont_prepare_label_data', $data, $order);
		}

		//Create download link for the PDF file
		public function generate_download_link( $order, $absolute = false) {
			if($order) {
				$pdf_name = '';
				$pdf_name = $order->get_meta('_vp_woo_pont_parcel_pdf');
				if($pdf_name) {
					$paths = $this->get_pdf_file_path();
					if($absolute) {
						$pdf_file_url = $paths['basedir'].$pdf_name;
					} else {
						$pdf_file_url = $paths['baseurl'].$pdf_name;
					}
					return $pdf_file_url;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		//Get file path for pdf files
		public static function get_pdf_file_path($provider = 'postapont', $order_id = 0) {
			$upload_dir = wp_upload_dir( null, false );
			$basedir = $upload_dir['basedir'] . '/vp-woo-pont-labels/';
			$baseurl = $upload_dir['baseurl'] . '/vp-woo-pont-labels/';
			$random_file_name = substr(md5(rand()),5);
			$pdf_file_name = implode( '-', array( $provider, $order_id, $random_file_name ) ).'.pdf';
			$file_dir = $basedir;

			//Group by year and month if needed
			if (get_option('uploads_use_yearmonth_folders') ) {
				$time = current_time( 'mysql' );
				$y = substr( $time, 0, 4 );
				$m = substr( $time, 5, 2 );
				$subdir = "/$y/$m";
				$pdf_file_name = $y.'/'.$m.'/'.$pdf_file_name;
				$file_dir = $basedir.$y.'/'.$m.'/';
			}

			return array('name' => $pdf_file_name, 'filedir' => $file_dir, 'path' => $basedir.$pdf_file_name, 'baseurl' => $baseurl, 'basedir' => $basedir);
		}

		//Meta box on order page
		public function add_metabox( $post_type, $post ) {
			$order = wc_get_order($post->ID);
			if($order) {
				add_meta_box('vp_woo_pont_metabox', __('Shipping informations', 'vp-woo-pont'), array( $this, 'render_metabox_content' ), 'shop_order', 'side');
			}
		}

		//Render metabox content
		public function render_metabox_content($post) {
			$order = wc_get_order($post->ID);
			include( dirname( __FILE__ ) . '/views/html-metabox.php' );
		}

		//Ajax function to create label
		public function ajax_generate_label() {
			check_ajax_referer( 'vp_woo_pont_manage', 'nonce' );
			$order_id = intval($_POST['order']);
			$response = $this->generate_label($order_id);
			wp_send_json_success($response);
		}

		//Ajax function to create label
		public function ajax_void_label() {
			check_ajax_referer( 'vp_woo_pont_manage', 'nonce' );
			$order_id = intval($_POST['order']);
			$response = $this->void_label($order_id);
			wp_send_json_success($response);
		}

		//Save PDF file
		public static function save_pdf_file($pdf, $pdf_file) {

			//If upload folder doesn't exists, create it with an empty index.html file
			$file = array(
				'base' 		=> $pdf_file['filedir'],
				'file' 		=> 'index.html',
				'content' 	=> ''
			);

			if ( wp_mkdir_p( $file['base'] ) && ! file_exists( trailingslashit( $file['base'] ) . $file['file'] ) ) {
				if ( $file_handle = @fopen( trailingslashit( $file['base'] ) . $file['file'], 'w' ) ) {
					fwrite( $file_handle, $file['content'] );
					fclose( $file_handle );
				}
			}

			//Make a GET request and store the pdf file
			file_put_contents($pdf_file['path'], $pdf);

		}

		//Helper function to force correct phone number format for FoxPost API
		public function format_phone_number($order) {
			$number = $order->get_billing_phone();
			$country = $order->get_shipping_country();
			$number = preg_replace( '/[^0-9]/', '', $number );

			//Fix hungarian format
			if(!$country || $country == 'HU') {
				$number = str_replace('+36', '', $number);
				$prefixes = array('+36', '36', '06');
				foreach ($prefixes as $prefix) {
					if (substr($number, 0, strlen($prefix)) == $prefix && strlen($number) > 7) {
						$number = substr($number, strlen($prefix));
					}
				}
				$number = '+36'.$number;
			}

			return $number;
		}

		//Check if it was already generated or not
		public function is_label_generated( $order) {
			if(is_int($order)) $order = wc_get_order($order);
			return ($order->get_meta('_vp_woo_pont_parcel_pdf'));
		}

		//Hide columns by default
		public function default_hidden_columns($hidden, $screen) {
			$hide_columns = array('vp_woo_pont', 'vp_woo_pont_shipment');
			if ( isset( $screen->id ) && 'edit-shop_order' === $screen->id ) {
				$hidden = array_merge( $hidden, $hide_columns );
			}
			return $hidden;
		}

		//Column on orders page
		public function add_listing_column($columns) {
			$new_columns = array();
			foreach ($columns as $column_name => $column_info ) {
				$new_columns[ $column_name ] = $column_info;
				if ( 'order_total' === $column_name ) {
					$new_columns['vp_woo_pont_shipment'] = __( 'Shipment', 'vp-woo-pont' );
				}
			}
			return $new_columns;
		}

		//Add icon to order list to show invoice
		public function add_listing_actions( $column ) {
			global $the_order;

			$hidden_columns = get_hidden_columns('edit-shop_order');
			if ( (in_array('vp_woo_pont_shipment', $hidden_columns) && 'shipping_address' === $column) || $column === 'vp_woo_pont_shipment') {
				if($provider_id = $the_order->get_meta('_vp_woo_pont_provider')) {
					$providers = VP_Woo_Pont_Helpers::get_supported_providers();
					$provider_name = '';
					if(isset($providers[$provider_id])) {
						$provider_name = $providers[$provider_id];
					}

					if(!$the_order->get_meta('_vp_woo_pont_point_id')) {
						$providers = VP_Woo_Pont_Helpers::get_supported_providers_for_home_delivery();
						$provider_name = $providers[$provider_id];
						if($provider_id == 'posta') {
							$provider_name = __('MPL', 'vo-woo-pont');
						}
					}

					?>
					<div class="vp-woo-pont-order-column">
						<?php if($this->is_label_generated($the_order)): ?>
							<a target="_blank" href="<?php echo VP_Woo_Pont()->tracking->get_tracking_link($the_order); ?>" class="vp-woo-pont-order-column-tracking help_tip" data-tip="<?php esc_attr_e('Tracking number', 'vp-woo-pont'); ?>">
								<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($provider_id); ?>"></i>
								<span><?php echo esc_html($the_order->get_meta('_vp_woo_pont_parcel_number')); ?></span>
							</a>
							<a target="_blank" href="<?php echo $this->generate_download_link($the_order); ?>" class="vp-woo-pont-order-column-pdf help_tip" data-tip="<?php esc_attr_e('Shipping label', 'vp-woo-pont'); ?>">
								<i></i>
								<span><?php echo esc_html($the_order->get_meta('_vp_woo_pont_parcel_id')); ?></span>
							</a>
						<?php else: ?>
							<span class="vp-woo-pont-order-column-label">
								<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($provider_id); ?>"></i>
								<span><?php echo esc_html($provider_name); ?></span>

								<?php if($provider_id == 'posta'): ?>
									<?php $mpl_closed = $the_order->get_meta('_vp_woo_pont_mpl_closed'); ?>
									<?php if($mpl_closed == 'no'): ?><em class="vp-woo-pont-mpl-status pending">Lezárásra vár</em><?php endif; ?>
									<?php if($mpl_closed && $mpl_closed != 'no'): ?><em class="vp-woo-pont-mpl-status closed">Feladás lezárva</em><?php endif; ?>
								<?php endif; ?>
							</span>
						<?php endif; ?>
					</div>
				<?php
				}
			}
		}

		//Replace placeholder in shipping label conents string
		public function get_package_contents_label($data, $provider) {

			//Get order
			$order = $data['order'];
			$note = VP_Woo_Pont_Helpers::get_option($provider.'_package_contents', '');
			$order_items = $order->get_items();
			$order_items_strings = array();

			//Setup order items
			foreach( $order_items as $order_item ) {
				$order_item_string = $order_item->get_quantity().'x '.$order_item->get_name();
				if($order_item->get_product()) {
					$product = $order_item->get_product();
					if($product->get_sku()) {
						$order_item_string .= ' ('.$product->get_sku().')';
					}
				}
				$order_items_strings[] = $order_item_string;
			}

			//Setup replacements
			$note_replacements = apply_filters('vp_woo_pont_'.$provider.'_label_placeholders', array(
				'{order_number}' => $order->get_order_number(),
				'{customer_note}' => $order->get_customer_note(),
				'{order_items}' => implode(', ', $order_items_strings)
			), $order, $data);

			//Replace stuff:
			$note = str_replace( array_keys( $note_replacements ), array_values( $note_replacements ), $note);

			//If custom option set(manual generate)
			if(isset($data['options']) && $data['options']['package_contents'] && $data['options']['package_contents'] != '') {
				$note = $data['options']['package_contents'];
			}

			return $note;
		}

		//Get reference number based on settings
		public function get_reference_number($order) {
			$ref = '';
			$number_type = VP_Woo_Pont_Helpers::get_option('label_reference_number', 'order_number');
			$order_number = $order->get_order_number();
			$order_id = $order->get_id();
			$invoice_number_szamlazz = $order->get_meta('_wc_szamlazz_invoice');
			$invoice_number_billingo = $order->get_meta('_wc_billingo_plus_invoice_name');

			if($number_type == 'order_number') {
				return $order_number;
			}

			if($number_type == 'order_id') {
				return $order_id;
			}

			//For számlázz.hu
			if($number_type == 'invoice_number' && $invoice_number_szamlazz) {
				return $invoice_number_szamlazz;
			}

			//For woo billingo plus
			if($number_type == 'invoice_number' && $invoice_number_billingo) {
				return $invoice_number_billingo;
			}

			//If invoice is missing, return the order number as a default
			return $order_number;
		}

		//Helper function to create bulk pdf
		public function create_bulk_pdf($order_ids) {

			//Init PDF merger
			require_once plugin_dir_path(__FILE__) . '../vendor/autoload.php';
			$merger = new Merger(new TcpdiDriver);
			$processed = array();

			//Create bulk pdf file
			$bulk_pdf_file = $this->get_pdf_file_path('bulk', 0);

			//Process selected posts, first by grouping them into providers
			$grouped_by_providers = array();
			foreach ( $order_ids as $order_id ) {
				if($this->is_label_generated($order_id)) {

					//Get provider id
					$order = wc_get_order($order_id);
					$provider_id = $order->get_meta('_vp_woo_pont_provider');
					if(!isset($grouped_by_providers[$provider_id])) $grouped_by_providers[$provider_id] = array();

					//And create a new array, which is order ids grouped by providers
					$grouped_by_providers[$provider_id][] = $order_id;
				}
			}

			//Loop through providers and see if we can generate a bulk pdf, or just one by one
			foreach ($grouped_by_providers as $provider_id => $order_ids) {
				if(in_array($provider_id, $this->supports_bulk_printing)) {
					$pdf_labels = $this->download_labels($order_ids, $provider_id);
					if(isset($pdf_labels['labels'])) {
						$merger->addFile($pdf_labels['labels']);
						$processed[] = $processed + $order_ids;
					}
				} else {
					foreach ($order_ids as $order_id) {
						$order = wc_get_order($order_id);
						$pdf_file = $this->generate_download_link($order, true);
						if($pdf_file && file_exists($pdf_file)) {
							$merger->addFile($pdf_file);
							$processed[] = $order_id;
						}

					}
				}
			}

			//Bail if theres no $documents
			if(!$processed) {
				return false;
			}

			//Create bulk pdf file
			$merged_pdf_file = $merger->merge();

			//Store PDF
			global $wp_filesystem;
			if ( !$wp_filesystem ) WP_Filesystem();
			$wp_filesystem->put_contents( $bulk_pdf_file['path'], $merged_pdf_file );

			//If theres only one type of labels, check if we can print it on a single page
			if(count($grouped_by_providers) == 1) {

				//Get provider type
				$provider = array_key_first($grouped_by_providers);

				//Check if merged printing is enabled
				if(in_array($provider, $this->supports_merged_printing) && VP_Woo_Pont_Helpers::get_option($provider.'_sticker_merge', 'no') == 'yes') {

					//Get sticker parameters
					$sticker_parameters = VP_Woo_Pont_Helpers::get_pdf_label_positions($provider);

					//If we don't have parameters, the selected label size doesn't support A4 format
					if($sticker_parameters['format']) {

						//Setup mpdf
						$pdf_path = $bulk_pdf_file['path'];
						$mpdf = new \Mpdf\Mpdf(array('mode' => 'c', 'format' => $sticker_parameters['format']));
						$page_count = $mpdf->setSourceFile($pdf_path);
						$processed = 0;

						//Loop through the pages and merge them onto new A4 pages
						for ($i = 0; $i < $page_count; $i++) {
							$processed++;
							$template_id = $mpdf->ImportPage($i + 1);
							$position_id = ($i % $sticker_parameters['sections']);
							$mpdf->UseTemplate($template_id, $sticker_parameters['x'][$position_id], $sticker_parameters['y'][$position_id]);
							if ($i % $sticker_parameters['sections'] == $sticker_parameters['sections']-1 && $page_count > $processed) $mpdf->AddPage();
						}

						//Save new merged PDF
						$mpdf->Output($pdf_path, \Mpdf\Output\Destination::FILE);

					}

				}

			}

			return array(
				'processed' => $processed,
				'bulk_pdf_file' => $bulk_pdf_file
			);
		}

		//Ajax function to print labels
		public function ajax_print_labels() {
			check_ajax_referer( 'vp_woo_pont_print_labels', 'nonce' );
			$orders = $_POST['orders'];

			//Sanitize order parameters
			$orders = wp_parse_id_list($orders);

			//Create merged pdf
			$bulk_results = $this->create_bulk_pdf($orders);

			//Bail if theres no $documents
			if(!$bulk_results) {
				wp_send_json_error();
			}

			//Get PDF url
			$pdf = $bulk_results['bulk_pdf_file']['baseurl'].$bulk_results['bulk_pdf_file']['name'];

			wp_send_json_success(array('url' => $pdf));
		}

	}

endif;
