<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'VP_Woo_Pont_Customizer', false ) ) :

	class VP_Woo_Pont_Customizer {

		public function __construct() {
			add_action( 'customize_register', array( $this, 'add_section' ) );
			add_action( 'customize_controls_print_styles', array( $this, 'add_styles' ) );
			add_action( 'customize_preview_init', array( $this, 'load_customizer_preview_js' ) );
			add_action( 'customize_controls_print_scripts', array( $this, 'load_customizer_js' ), 30 );
			add_action( 'wp_enqueue_scripts', array( $this, 'load_inline_styles' ), 20 );
		}

		public function add_styles() {
			?>
			<style type="text/css">
				#customize-control-vp_woo_pont_custom_icon img.attachment-thumb {
					width: 48px;
					height: 48px;
					object-fit: contain;
				}
			</style>
			<?php
		}

		//Create extra section
		public function add_section( $wp_customize ) {

			//Create a new section in the WooCommerce menu
			$wp_customize->add_section('vp_woo_pont', array(
				'title' => _x('Pickup points', 'admin',  'vp-woo-pont'),
				'panel' => 'woocommerce',
				'description' => __('You can customize the look & feel of the pickup point modal window with these options.', 'vp-woo-pont')
	    ));

			//Create color options
			$color_options = array(
				'primary' => array(
					'label' => __( 'Primary color', 'vp-woo-pont' ),
					'default' => '#2471B1'
				),
				'text' => array(
					'label' => __( 'Text color', 'vp-woo-pont' ),
					'default' => '#838383'
				),
				'price' => array(
					'label' => __( 'Price color', 'vp-woo-pont' ),
					'default' => '#49B553'
				),
				'cluster_large' => array(
					'label' => __( 'Large cluster color', 'vp-woo-pont' ),
					'default' => '#E86100'
				),
				'cluster_medium' => array(
					'label' => __( 'Medium cluster color', 'vp-woo-pont' ),
					'default' => '#F7CB1E'
				),
				'cluster_small' => array(
					'label' => __( 'Small cluster color', 'vp-woo-pont' ),
					'default' => '#49B654'
				)
			);

			//Loop through color options to create settings and controls for them
			foreach ($color_options as $id => $option) {
				$wp_customize->add_setting(
					'vp_woo_pont_'.$id.'_color',
					array(
						'default'           => $option['default'],
						'sanitize_callback' => 'sanitize_hex_color',
						'transport'         => 'postMessage',
					)
				);

				$wp_customize->add_control(
					new WP_Customize_Color_Control(
						$wp_customize,
						'vp_woo_pont_'.$id.'_color',
						array(
							'label'    => $option['label'],
							'section'  => 'vp_woo_pont',
							'settings' => 'vp_woo_pont_'.$id.'_color'
						)
					)
				);
			}

			//Create an image field to upload an icon for custom pickup points
			$wp_customize->add_setting(
				'vp_woo_pont_custom_icon',
				array(
					'default' => '',
					'transport' => 'postMessage',
					'type' => 'option'
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Image_Control(
					$wp_customize,
					'vp_woo_pont_custom_icon',
					array(
						'label' => __( 'Store Pickup icon', 'vp-woo-pont' ),
						'section' => 'vp_woo_pont',
						'settings' => 'vp_woo_pont_custom_icon',
						'extensions' => array( 'jpg', 'jpeg', 'gif', 'png', 'svg' ),
						'description' => __('You can set a custom icon to be visible on the map markers for your own pickup points. For SVG files, please install the SVG Support extension first.', 'vp-woo-pont')
					)
				)
			);

			//Show and hide small icons in the shipping mode selector
			$wp_customize->add_setting(
				'vp_woo_pont_small_icons',
				array(
					'default' => 'yes',
					'type' => 'option',
					'capability' => 'manage_woocommerce',
					'sanitize_callback' => 'wc_bool_to_string',
					'sanitize_js_callback' => 'wc_string_to_bool',
					'transport' => 'postMessage',
				)
			);

			$wp_customize->add_control(
				'vp_woo_pont_small_icons',
				array(
					'label' => __( 'Show provider icons below the shipping option', 'vp-woo-pont' ),
					'section' => 'vp_woo_pont',
					'settings' => 'vp_woo_pont_small_icons',
					'type' => 'checkbox',
				)
			);

			//Show picker on cart page
			$wp_customize->add_setting(
				'vp_woo_pont_show_on_cart',
				array(
					'default' => 'yes',
					'type' => 'option',
					'capability' => 'manage_woocommerce',
					'sanitize_callback' => 'wc_bool_to_string',
					'sanitize_js_callback' => 'wc_string_to_bool',
				)
			);

			$wp_customize->add_control(
				'vp_woo_pont_show_on_cart',
				array(
					'label' => __( 'Show the selector on the cart page too', 'vp-woo-pont' ),
					'section' => 'vp_woo_pont',
					'settings' => 'vp_woo_pont_show_on_cart',
					'type' => 'checkbox',
				)
			);

			//Create font size options
			$font_options = array(
				'title' => array(
					'label' => __( 'Title font size', 'vp-woo-pont' ),
					'default' => 14
				),
				'text' => array(
					'label' => __( 'Text font size', 'vp-woo-pont' ),
					'default' => 12
				),
				'price' => array(
					'label' => __( 'Price font size', 'vp-woo-pont' ),
					'default' => 12
				),
			);

			//Loop through font options to create settings and controls for them
			foreach ($font_options as $id => $option) {
				$wp_customize->add_setting(
					'vp_woo_pont_'.$id.'_font_size',
					array(
						'default'           => $option['default'],
						'sanitize_callback' => 'absint',
						'transport'         => 'postMessage',
					)
				);
				$wp_customize->add_control(
					'vp_woo_pont_'.$id.'_font_size',
					array(
						'label'    => $option['label'],
						'section'  => 'vp_woo_pont',
						'settings' => 'vp_woo_pont_'.$id.'_font_size',
						'type'        => 'number',
						'input_attrs' => array(
							'min'  => 10,
							'max'  => 20,
							'step' => 1,
						),
					)
				);
			}


		}

		//To do live refresh while customizing
		public function load_customizer_preview_js() {
			wp_enqueue_script( 'vp_woo_pont_customizer_js', VP_Woo_Pont()->plugin_url.'/assets/js/customizer.js', array('jquery','customize-preview'), VP_Woo_Pont::$version, true );
		}

		//To do live refresh while customizing
		public function load_customizer_js() {
			?>
			<script type="text/javascript">
				jQuery( function( $ ) {
					//When the VP Woo Pont section is selected, redirect to the checkout page
					wp.customize.section( 'vp_woo_pont', function( section ) {
						section.expanded.bind( function( isExpanded ) {
							if ( isExpanded ) {
								wp.customize.previewer.previewUrl.set( '<?php echo esc_js( wc_get_page_permalink( 'checkout' ) ); ?>' );
							}
						});
					});
				});
			</script>
			<?php
		}

		//Function to generate inline styles for colors and other stuff
		public function load_inline_styles() {

			//Load customizer CSS
			$primary_color = get_theme_mod('vp_woo_pont_primary_color', '#2471B1');
			$primary_color_rgb = $this->hex_to_rgb($primary_color);
			$text_color = get_theme_mod('vp_woo_pont_text_color', '#838383');
			$price_color = get_theme_mod('vp_woo_pont_price_color', '#49B553');

			$cluster_large_color = get_theme_mod('vp_woo_pont_cluster_large_color', '#E86100');
			$cluster_medium_color = get_theme_mod('vp_woo_pont_cluster_medium_color', '#F7CB1E');
			$cluster_small_color = get_theme_mod('vp_woo_pont_cluster_small_color', '#49B654');
			$cluster_large_color_rgb = $this->hex_to_rgb($cluster_large_color);
			$cluster_medium_color_rgb = $this->hex_to_rgb($cluster_medium_color);
			$cluster_small_color_rgb = $this->hex_to_rgb($cluster_small_color);

			$title_font_size = get_theme_mod('vp_woo_pont_title_font_size', 14);
			$text_font_size = get_theme_mod('vp_woo_pont_text_font_size', 12);
			$price_font_size = get_theme_mod('vp_woo_pont_price_font_size', 12);

			$custom_css = ':root{';
			$custom_css .= '--vp-woo-pont-primary-color: '.$primary_color.';';
			$custom_css .= '--vp-woo-pont-primary-color-alpha-20: rgba('.$primary_color_rgb[0].','.$primary_color_rgb[1].','.$primary_color_rgb[2].',0.2);';
			$custom_css .= '--vp-woo-pont-primary-color-alpha-10: rgba('.$primary_color_rgb[0].','.$primary_color_rgb[1].','.$primary_color_rgb[2].',0.1);';
			$custom_css .= '--vp-woo-pont-primary-color-alpha-05: rgba('.$primary_color_rgb[0].','.$primary_color_rgb[1].','.$primary_color_rgb[2].',0.05);';
			$custom_css .= '--vp-woo-pont-text-color: '.$text_color.';';
			$custom_css .= '--vp-woo-pont-price-color: '.$price_color.';';
			$custom_css .= '--vp-woo-pont-cluster-large-color: rgba('.$cluster_large_color_rgb[0].','.$cluster_large_color_rgb[1].','.$cluster_large_color_rgb[2].',0.9);';
			$custom_css .= '--vp-woo-pont-cluster-medium-color: rgba('.$cluster_medium_color_rgb[0].','.$cluster_medium_color_rgb[1].','.$cluster_medium_color_rgb[2].',0.9);';
			$custom_css .= '--vp-woo-pont-cluster-small-color: rgba('.$cluster_small_color_rgb[0].','.$cluster_small_color_rgb[1].','.$cluster_small_color_rgb[2].',0.9);';
			$custom_css .= '--vp-woo-pont-title-font-size: '.$title_font_size.'px;';
			$custom_css .= '--vp-woo-pont-text-font-size: '.$text_font_size.'px;';
			$custom_css .= '--vp-woo-pont-price-font-size: '.$price_font_size.'px;';
			$custom_css .= '}';

			//If a custom icon is set
			if(get_option('vp_woo_pont_custom_icon')) {
				$custom_css .= '.vp-woo-pont-provider-icon-custom{background-image:url('.get_option('vp_woo_pont_custom_icon').')}';
			} else {
				$custom_css .= '.vp-woo-pont-modal-sidebar-filters label[for="provider-custom"]{padding-left: 10px !important}';
			}

			//Load the inline styles
			wp_add_inline_style( 'vp_woo_pont_frontend_css', $custom_css );

		}

		//Helper function to convert hex to to rgb(we need rgba for a couple of colors)
		public function hex_to_rgb($hex) {
			// Remove the "#" symbol from the beginning of the color.
			$hex = ltrim( $hex, '#' );

			// Make sure there are 6 digits for the below calculations.
			if ( 3 === strlen( $hex ) ) {
				$hex = substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) . substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) . substr( $hex, 2, 1 ) . substr( $hex, 2, 1 );
			}

			// Get red, green, blue.
			$red   = hexdec( substr( $hex, 0, 2 ) );
			$green = hexdec( substr( $hex, 2, 2 ) );
			$blue  = hexdec( substr( $hex, 4, 2 ) );

			return [$red, $green, $blue];
		}

	}

	new VP_Woo_Pont_Customizer();

endif;
