<?php
defined( 'ABSPATH' ) || exit;

$settings = array(
	'title'      => array(
		'title'       => __( 'Method title', 'vp-woo-pont' ),
		'type'        => 'text',
		'description' => __( 'This controls the title which the user sees during checkout.', 'vp-woo-pont' ),
		'default'     => _x( 'Pickup point', 'frtonend', 'vp-woo-pont' ),
		'desc_tip'    => true,
	),
);

return $settings;
