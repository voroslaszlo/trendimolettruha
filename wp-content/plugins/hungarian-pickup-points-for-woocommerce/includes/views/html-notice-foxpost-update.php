<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="notice notice-info vp-woo-pont-notice vp-woo-pont-foxpost-update">
	<div class="vp-woo-pont-welcome-body">
    <button type="button" class="notice-dismiss vp-woo-pont-hide-notice" data-nonce="<?php echo wp_create_nonce( 'vp-woo-pont-hide-notice' )?>" data-notice="foxpost-update"><span class="screen-reader-text"><?php esc_html_e( 'Dismiss', 'vp-woo-pont' ); ?></span></button>
		<h2><?php esc_html_e('Update Foxpost settings', 'vp-woo-pont'); ?></h2>
		<p><?php esc_html_e("This version of the extension is using a newer Foxpost API, so you need to update your connection details in the settings in order to generate labels.", 'vp-woo-pont'); ?></p>
		<p>
			<a class="button-secondary" href="<?php echo esc_url(admin_url( wp_nonce_url('admin.php?page=wc-settings&tab=shipping&section=vp_pont&foxpost-update=1', 'vp-woo-pont-hide-notice' ) )); ?>"><?php esc_html_e( 'Go to Settings', 'vp-woo-pont' ); ?></a>
		</p>
	</div>
</div>
