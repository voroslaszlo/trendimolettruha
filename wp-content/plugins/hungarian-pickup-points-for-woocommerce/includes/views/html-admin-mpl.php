<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="wrap">
	<h1><?php esc_html_e('MPL Delivery notes', 'vp-woo-pont'); ?></h1>

	<?php

	//Get table data for pending orders
	$orders_table = new VP_Woo_Pont_MPL_Pending_Table();
	$orders_table->prepare_items();

	//Get table data for closed shipments
	$packages_table = new VP_Woo_Pont_MPL_Shipments_Table();
	$packages_table->prepare_items();

	?>

	<?php	if ( empty( $orders_table->items ) && empty( $packages_table->items ) ): ?>
		<div class="vp-woo-pont-admin-mpl-no-results">
			<p><?php esc_html_e('Generate at least one MPL label, after that, you can generate the necessary delivery note for them.', 'vp-woo-pont'); ?></p>
		</div>
	<?php endif; ?>

	<?php	if ( ! empty( $orders_table->items ) ): ?>
		<h2><?php esc_html_e('Waiting for closing', 'vp-woo-pont'); ?></h2>
		<p><?php esc_html_e('The following packages are waiting for closing and a delivery note.', 'vp-woo-pont'); ?></p>
		<div class="vp-woo-pont-admin-mpl-notice notice notice-error" style="display:none"><p></p></div>
		<div class="vp-woo-pont-admin-mpl-table">
			<?php $orders_table->display(); ?>
			<a class="button button-primary button-large" href="#" id="vp_woo_pont_mpl_close_shipments" data-nonce="<?php echo wp_create_nonce( 'vp-woo-pont-mpl-close-shipments' )?>"><?php esc_html_e( 'Generate delivery note', 'vp-woo-pont' ); ?></a>
			<a class="button button-secondary button-large" href="#" id="vp_woo_pont_mpl_close_orders" data-nonce="<?php echo wp_create_nonce( 'vp-woo-pont-mpl-close-shipments' )?>"><?php esc_html_e( 'Mark as shipped', 'vp-woo-pont' ); ?></a>
		</div>
		<hr>
	<?php endif; ?>

	<?php	if ( ! empty( $packages_table->items ) ): ?>
		<h2><?php esc_html_e('Closed packages', 'vp-woo-pont'); ?></h2>
		<p><?php esc_html_e('You can find all your closed packages and delivery notes here.', 'vp-woo-pont'); ?></p>
		<div class="vp-woo-pont-admin-mpl-table">
			<?php $packages_table->display(); ?>
		</div>
	<?php endif; ?>
</div>
