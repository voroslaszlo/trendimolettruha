<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="notice notice-info vp-woo-pont-notice vp-woo-pont-bulk-actions vp-woo-pont-print">
	<?php if($action == 'print'): ?>
		<p>
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#2371B1" d="M19 8H5c-1.66 0-3 1.34-3 3v6h4v4h12v-4h4v-6c0-1.66-1.34-3-3-3zm-3 11H8v-5h8v5zm3-7c-.55 0-1-.45-1-1s.45-1 1-1 1 .45 1 1-.45 1-1 1zm-1-9H6v4h12V3z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
			<span><?php echo sprintf( esc_html__( '%s shipping label(s) selected for printing.', 'vp-woo-pont' ), $print_count); ?></span>
			<a href="<?php echo $pdf_file_url; ?>" id="vp-woo-pont-bulk-print" data-pdf="<?php echo $pdf_file_url; ?>"><?php esc_html_e('Print', 'vp-woo-pont'); ?></a>
		</p>
	<?php elseif($action == 'download'): ?>
		<p>
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path fill="#2371B1" d="M19.35 10.04C18.67 6.59 15.64 4 12 4 9.11 4 6.6 5.64 5.35 8.04 2.34 8.36 0 10.91 0 14c0 3.31 2.69 6 6 6h13c2.76 0 5-2.24 5-5 0-2.64-2.05-4.78-4.65-4.96zM17 13l-5 5-5-5h3V9h4v4h3z"/></svg>
			<span><?php echo sprintf( esc_html__( '%s shipping label(s) selected for download.', 'vp-woo-pont' ), $print_count); ?></span>
			<a href="<?php echo $pdf_file_url; ?>" id="vp-woo-pont-bulk-download" download data-pdf="<?php echo $pdf_file_url; ?>"><?php esc_html_e('Download', 'vp-woo-pont'); ?></a>
		</p>
	<?php elseif($action == 'generate'): ?>
		<?php if(count($documents) > apply_filters('vp_woo_pont_generate_defer_limit', 5)): ?>
			<p>
				<span><?php echo sprintf( esc_html__( '%1$s order(s) selected to create shipping labels. Documents are being created in the background.', 'vp-woo-pont' ), count($documents)); ?></span>
			</p>
		<?php else: ?>
			<p>
				<i class="vp-woo-pont-bulk-actions-icon"></i>
				<span><?php echo sprintf( esc_html__( '%1$s order(s) selected:', 'vp-woo-pont' ), count($documents)); ?></span>
			</p>
			<?php $orders_with_labels = array(); ?>
			<?php foreach ($documents as $order_id): ?>
				<?php $temp_order = wc_get_order($order_id); ?>
				<?php if($temp_order): ?>
					<p>
						<a href="<?php echo esc_url($temp_order->get_edit_order_url()); ?>"><?php echo esc_html($temp_order->get_order_number()); ?></a> -
						<?php $provider = $temp_order->get_meta('_vp_woo_pont_provider'); ?>
						<?php if(!$provider) $provider = VP_Woo_Pont_Helpers::get_paired_provider($temp_order); ?>
						<?php if($provider): ?>
							<?php if($temp_order->get_meta('_vp_woo_pont_parcel_number')): ?>
								<?php $orders_with_labels[] = $order_id; ?>
								<?php echo esc_html($temp_order->get_meta('_vp_woo_pont_parcel_number')); ?>
							<?php elseif ($temp_order->get_meta('_vp_woo_pont_parcel_pending')): ?>
								<?php esc_html_e( 'Work in progress', 'vp-woo-pont' ); ?>
							<?php else: ?>
								<?php esc_html_e( 'Unable to create a label, check the order notes for more info', 'vp-woo-pont' ); ?>
							<?php endif; ?>
						<?php else: ?>
							<?php esc_html_e( 'Not a pickup point order', 'vp-woo-pont' ); ?>
						<?php endif; ?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>

			<?php if(count($orders_with_labels)>0): ?>
				<p>
					<a href="#" class="button" id="vp-woo-pont-bulk-print-generate" data-orders="<?php echo wc_esc_json(wp_json_encode($orders_with_labels)); ?>" data-nonce="<?php echo wp_create_nonce( "vp_woo_pont_print_labels" ); ?>"><?php printf( _n( 'Print %s label', 'Print %s labels', count($orders_with_labels), 'vp-woo-pont' ), count($orders_with_labels) ); ?></a><br>
				</p>
			<?php endif; ?>

		<?php endif; ?>
	<?php endif; ?>
</div>
