<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<?php

//Get some default options
$provider_id = VP_Woo_Pont_Helpers::get_provider_from_order($order);
if(!$provider_id) $provider_id = 'gls';
$providers = VP_Woo_Pont_Helpers::get_supported_providers();
if($provider_id == 'posta') {
	$provider_name = __('MPL', 'vo-woo-pont');
} else {
	$provider_name = $providers[$provider_id];
}
$has_label = $order->get_meta( '_vp_woo_pont_parcel_pdf' );
$pending = $order->get_meta('_vp_woo_pont_parcel_pending');
$providers_for_home_delivery = VP_Woo_Pont_Helpers::get_supported_providers_for_home_delivery();

//For home delivery, set default provider_id if possible
if(!$order->get_meta('_vp_woo_pont_point_id')) {
	$provider_name = $providers_for_home_delivery[$provider_id];
}

?>

	<?php if(!in_array($provider_id, $this->supported_providers) && $order->get_meta('_vp_woo_pont_point_name')): //TODO: check for api keys for labels ?>
		<p class="vp-woo-pont-metabox-unsupported">A kiválasztott szolgáltatóhoz(<?php echo esc_html($provider_name); ?>) jelenleg nem érhető el a címkenyomtatás funkció.</p>
	<?php endif; ?>

	<div class="vp-woo-pont-metabox-content" data-order="<?php echo $order->get_id(); ?>" data-nonce="<?php echo wp_create_nonce( "vp_woo_pont_manage" ); ?>" data-provider_id="<?php echo $provider_id; ?>">
		<div class="vp-woo-pont-metabox-messages vp-woo-pont-metabox-messages-label vp-woo-pont-metabox-messages-success" style="display:none;">
			<div class="vp-woo-pont-metabox-messages-content">
				<ul></ul>
				<a href="#"><span class="dashicons dashicons-no-alt"></span></a>
			</div>
		</div>

		<ul class="vp-woo-pont-metabox-rows">

			<?php //If its a normal shipping ?>
			<?php if(!$order->get_meta('_vp_woo_pont_point_id')): ?>
			<li class="vp-woo-pont-metabox-rows-data vp-woo-pont-metabox-rows-data-provider <?php if($provider_id): ?>show<?php endif; ?>">
				<div class="vp-woo-pont-metabox-rows-data-inside">
					<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($provider_id); ?>"></i>
					<strong><?php echo $provider_name; ?></strong>
					<?php if(!$has_label): ?>
						<a href="#" id="vp_woo_pont_modify_provider"><?php echo esc_html__('Modify', 'vp-woo-pont'); ?></a>
					<?php endif; ?>
				</div>
			</li>
			<li class="vp-woo-pont-metabox-rows-data vp-woo-pont-metabox-rows-data-home-delivery-providers <?php if(!$provider_id): ?>show<?php endif; ?>">
				<ul>
					<?php if(!$provider_id) $provider_id = 'gls'; ?>
					<?php foreach ($providers_for_home_delivery as $id => $name): ?>
						<li>
							<input type="radio" name="home_delivery_provider" data-label="<?php echo esc_attr($name); ?>" id="home_delivery_provider_<?php echo esc_attr($id); ?>" <?php checked($id, $provider_id); ?> value="<?php echo esc_attr($id); ?>">
							<label for="home_delivery_provider_<?php echo esc_attr($id); ?>">
								<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($id); ?>"></i>
								<?php echo esc_html($name); ?>
							</label>
						</li>
					<?php endforeach; ?>
				</ul>
			</li>
			<?php endif; ?>

			<?php //If its a pickup point ?>
			<?php if($order->get_meta('_vp_woo_pont_point_id')): ?>
			<li class="vp-woo-pont-metabox-rows-data vp-woo-pont-metabox-rows-data-provider show">
				<div class="vp-woo-pont-metabox-rows-data-inside">
					<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($provider_id); ?>"></i>
					<strong><?php echo $provider_name; ?></strong>
					<span><?php echo $order->get_meta('_vp_woo_pont_point_name'); ?></span>
				</div>
			</li>
			<li class="vp-woo-pont-metabox-rows-data vp-woo-pont-metabox-rows-data-remove <?php if(!$has_label && !$pending): ?>show<?php endif; ?>">
				<div class="vp-woo-pont-metabox-rows-data-inside">
					<a href="#" data-trigger-value="<?php esc_attr_e('Remove selected point','vp-woo-pont'); ?>" data-question="<?php echo esc_attr_x('Are you sure?', 'Delete point', 'vp-woo-pont'); ?>" class="delete"><?php esc_html_e('Remove selected point','vp-woo-pont'); ?></a>
				</div>
			</li>
			<li class="vp-woo-pont-metabox-rows-data vp-woo-pont-metabox-rows-data-replace <?php if(!$has_label && !$pending): ?>show<?php endif; ?>">
				<div class="vp-woo-pont-metabox-rows-data-inside">
					<a href="#"><?php esc_html_e('Replace selected point','vp-woo-pont'); ?></a>
				</div>
			</li>
			<?php endif; ?>

			<?php //Shipping label data, tracking number, delete button ?>
			<?php if(VP_Woo_Pont_Pro::is_pro_enabled()): ?>
				<li class="vp-woo-pont-metabox-rows-link vp-woo-pont-metabox-rows-link-pdf <?php if($has_label || $pending): ?>show<?php endif; ?> <?php if($pending): ?>pending<?php endif; ?>">
					<a target="_blank" href="<?php echo $this->generate_download_link($order); ?>">
						<span><?php esc_html_e('Shipping label', 'vp-woo-pont'); ?></span>
						<strong><?php echo esc_html($order->get_meta('_vp_woo_pont_parcel_id')); ?></strong>
					</a>
					<small class="pending-download"><?php _e('The PDF is being downloaded', 'vp-woo-pont'); ?><i>.</i><i>.</i><i>.</i></small>
				</li>
				<li class="vp-woo-pont-metabox-rows-data vp-woo-pont-metabox-rows-link-tracking <?php if($has_label): ?>show<?php endif; ?>">
					<div class="vp-woo-pont-metabox-rows-data-inside">
						<span><?php esc_html_e('Tracking number', 'vp-woo-pont'); ?></span>
						<strong><a href="<?php echo VP_Woo_Pont()->tracking->get_tracking_link($order); ?>" target="_blank"><?php echo esc_html($order->get_meta('_vp_woo_pont_parcel_number')); ?></a></strong>
					</div>
				</li>
				<li class="vp-woo-pont-metabox-rows-data vp-woo-pont-metabox-rows-data-void <?php if($has_label): ?>show<?php endif; ?>">
					<div class="vp-woo-pont-metabox-rows-data-inside">
						<a href="#" data-trigger-value="<?php esc_attr_e('Delete label','vp-woo-pont'); ?>" data-question="<?php echo esc_attr_x('Are you sure?', 'Delete label', 'vp-woo-pont'); ?>" class="delete"><?php esc_html_e('Delete label','vp-woo-pont'); ?></a>
					</div>
				</li>
				<?php if($order->get_meta('_vp_woo_pont_mpl_closed') && $order->get_meta('_vp_woo_pont_mpl_closed') != 'no'): ?>
					<li class="vp-woo-pont-metabox-rows-data show">
						<div class="vp-woo-pont-metabox-rows-data-inside">
							<span><?php esc_html_e('Delivery note ID', 'vp-woo-pont'); ?></span>
							<strong><?php echo esc_html($order->get_meta('_vp_woo_pont_mpl_closed')); ?></strong>
						</div>
					</li>
				<?php endif; ?>
			<?php endif; ?>

		</ul>

		<?php if(VP_Woo_Pont_Pro::is_pro_enabled() && !$pending && in_array($provider_id, $this->supported_providers)): ?>
		<div class="vp-woo-pont-metabox-generate <?php if(!$has_label): ?>show<?php endif; ?>">
			<div class="vp-woo-pont-metabox-generate-buttons">
				<a href="#" id="vp_woo_pont_label_options"><span class="dashicons dashicons-admin-generic"></span><span><?php esc_html_e('Options','vp-woo-pont'); ?></span></a>

				<a href="#" id="vp_woo_pont_label_generate" class="button button-primary" target="_blank" data-question="<?php echo esc_attr_x('Are you sure?', 'Generate label', 'vp-woo-pont'); ?>">
					<?php esc_html_e('Generate label', 'vp-woo-pont'); ?>
				</a>
			</div>

			<ul class="vp-woo-pont-metabox-generate-options" style="display:none">
				<li data-providers="[gls]">
					<label for="vp_woo_pont_package_count"><?php esc_html_e('Number of packages','vp-woo-pont'); ?></label>
					<input type="number" id="vp_woo_pont_package_count" value="1" />
				</li>
				<li data-providers="[gls]">
					<label for="vp_woo_pont_pickup_date"><?php esc_html_e('Pickup date','vp-woo-pont'); ?></label>
					<input type="text" class="date-picker" id="vp_woo_pont_pickup_date" maxlength="10" value="" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])">
				</li>
				<li>
					<label for="vp_woo_pont_package_contents"><?php esc_html_e('Package contents','vp-woo-pont'); ?></label>
					<input type="text" id="vp_woo_pont_package_contents" value="<?php echo VP_Woo_Pont()->labels->get_package_contents_label(array('order' => $order), $provider_id); ?>">
				</li>
			</ul>

		</div>
		<?php endif; ?>
	</div>


<?php include( dirname( __FILE__ ) . '/html-modal-replace-point.php' ); ?>
