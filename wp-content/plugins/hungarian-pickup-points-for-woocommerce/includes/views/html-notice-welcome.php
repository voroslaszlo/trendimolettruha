<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="notice notice-info vp-woo-pont-notice vp-woo-pont-welcome">
	<div class="vp-woo-pont-welcome-body">
    <button type="button" class="notice-dismiss vp-woo-pont-hide-notice" data-nonce="<?php echo wp_create_nonce( 'vp-woo-pont-hide-notice' )?>" data-notice="welcome"><span class="screen-reader-text"><?php esc_html_e( 'Dismiss', 'vp-woo-pont' ); ?></span></button>
		<h2><?php esc_html_e('Hungarian Pickup Points for WooCommerce', 'vp-woo-pont'); ?></h2>
		<p><?php esc_html_e("Thank you for using this extension. To get started, go to the settings page and enable your shipping providers and setup the shipping costs. Please keep in mind that this extension is still in development, so main functions are missing, like label printing and shiment tracking. It will be available soon as a PRO version.", 'vp-woo-pont'); ?></p>
		<p>
			<a class="button-secondary" href="<?php echo esc_url(admin_url( wp_nonce_url('admin.php?page=wc-settings&tab=shipping&section=vp_pont&welcome=1', 'vp-woo-pont-hide-notice' ) )); ?>"><?php esc_html_e( 'Go to Settings', 'vp-woo-pont' ); ?></a>
		</p>
	</div>
</div>
