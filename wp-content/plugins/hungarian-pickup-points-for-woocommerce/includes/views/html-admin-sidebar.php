<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="vp-woo-pont-settings-sidebar">

	<?php if(VP_Woo_Pont_Pro::is_pro_enabled() || (!VP_Woo_Pont_Pro::is_pro_enabled() && VP_Woo_Pont_Pro::get_license_key())): ?>

		<div class="vp-woo-pont-settings-widget vp-woo-pont-settings-widget-pro vp-woo-pont-settings-widget-pro-active vp-woo-pont-settings-widget-pro-<?php if(VP_Woo_Pont_Pro::is_pro_enabled()): ?>state-active<?php else: ?>state-expired<?php endif; ?>">

			<?php if(VP_Woo_Pont_Pro::is_pro_enabled()): ?>
				<h3><span class="dashicons dashicons-yes-alt"></span> <?php _e('The PRO version is active', 'vp-woo-pont'); ?></h3>
				<p><?php _e('You have successfully activated the PRO version.', 'vp-woo-pont'); ?></p>
			<?php else: ?>
				<h3><span class="dashicons dashicons-warning"></span> <?php _e('The PRO version is expired', 'vp-woo-pont'); ?></h3>
				<p><?php _e('The following license key is expired.', 'vp-woo-pont'); ?></p>
			<?php endif; ?>

			<p>
				<span class="vp-woo-pont-settings-widget-pro-label"><?php _e('License key', 'vp-woo-pont'); ?></span><br>
				<?php echo esc_html(VP_Woo_Pont_Pro::get_license_key()); ?>
			</p>

			<?php $license = VP_Woo_Pont_Pro::get_license_key_meta(); ?>
			<?php if(isset($license['type'])): ?>
			<p class="single-license-info">
				<span class="vp-woo-pont-settings-widget-pro-label"><?php _e('License type', 'vp-woo-pont'); ?></span><br>
				<?php if ( $license['type'] == 'unlimited' ): ?>
					<?php _e( 'Unlimited', 'vp-woo-pont' ); ?>
				<?php else: ?>
					<?php _e( 'Subscription', 'vp-woo-pont' ); ?>
				<?php endif; ?>
			</p>
			<?php endif; ?>

			<?php if(isset($license['next_payment'])): ?>
			<p class="single-license-info">
				<span class="vp-woo-pont-settings-widget-pro-label"><?php _e('Next payment', 'vp-woo-pont'); ?></span><br>
				<?php echo esc_html($license['next_payment']); ?>
			</p>
			<?php endif; ?>

			<div class="vp-woo-pont-settings-widget-pro-deactivate">
				<p>
					<a class="button-secondary" id="vp_woo_pont_deactivate_pro"><?php esc_html_e( 'Deactivate license', 'vp-woo-pont' ); ?></a>
					<a class="button-secondary" id="vp_woo_pont_validate_pro"><?php esc_html_e( 'Reload license', 'vp-woo-pont' ); ?></a>
				</p>
				<p><small><?php esc_html_e( 'If you want to activate the license on another website, you must first deactivate it on this website.', 'vp-woo-pont' ); ?></small></p>
			</div>
		</div>

	<?php else: ?>

		<div class="vp-woo-pont-settings-widget vp-woo-pont-settings-widget-pro">
			<h3><?php esc_html_e( 'PRO version', 'vp-woo-pont' ); ?></h3>
			<p><?php esc_html_e( 'If you have already purchased the PRO version, enter the license key used to purchase:', 'vp-woo-pont' ); ?></p>

			<div class="vp-woo-pont-settings-widget-pro-notice" style="display:none">
				<span class="dashicons dashicons-warning"></span>
				<p></p>
			</div>

			<fieldset>
				<input class="input-text regular-input" type="text" name="woocommerce_vp_woo_pont_pro_key" id="woocommerce_vp_woo_pont_pro_key" value="" placeholder="<?php esc_html_e( 'License key', 'vp-woo-pont' ); ?>"><br>
			</fieldset>
			<p>
				<button class="button-primary" type="button" id="vp_woo_pont_activate_pro"><?php _e('Activate', 'vp-woo-pont'); ?></button>
			</p>
			<h4><?php esc_html_e( 'Why should I use the PRO version?', 'vp-woo-pont' ); ?></h4>
			<ul>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Generate shipping labels', 'vp-woo-pont' ); ?></li>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Display tracking numbers', 'vp-woo-pont' ); ?></li>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Billingo & Számlázz.hu integration', 'vp-woo-pont' ); ?></li>
				<li><span class="dashicons dashicons-yes"></span> <?php esc_html_e( 'Premium support', 'vp-woo-pont' ); ?></li>
			</ul>
			<div class="vp-woo-pont-settings-widget-pro-cta">
				<a href="https://visztpeter.me/woocommerce-csomagpont-integracio/"><span class="dashicons dashicons-cart"></span> <span><?php esc_html_e( 'Purchase PRO version', 'vp-woo-pont' ); ?></span></a>
				<span>
					<small><?php esc_html_e( 'net', 'vp-woo-pont' ); ?></small>
					<strong><?php esc_html_e( '30 EUR / year', 'vp-woo-pont' ); ?></strong>
				</span>
			</div>

		</div>

	<?php endif; ?>

	<div class="vp-woo-pont-settings-widget vp-woo-pont-settings-widget-files" data-nonce="<?php echo wp_create_nonce( 'vp-woo-pont-import-json-manually' )?>">
		<h3><?php esc_html_e('Pickup point list', 'vp-woo-pont'); ?></h3>
		<?php
		$provider_groups = VP_Woo_Pont_Helpers::get_external_provider_groups();
		$download_folders = VP_Woo_Pont_Helpers::get_download_folder();
		?>
		<div class="vp-woo-pont-settings-widget-files-notice" style="display:none">
			<span class="dashicons dashicons-warning"></span>
			<span class="dashicons dashicons-yes-alt"></span>
			<p></p>
		</div>
		<ul>
			<?php foreach ($provider_groups as $provider_id => $provider_label): ?>
				<li>
					<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($provider_id); ?>"></i>
					<strong><?php echo esc_html($provider_label); ?></strong>
					<nav>
						<?php $filename = get_option('_vp_woo_pont_file_'.$provider_id); ?>
						<?php $url = $download_folders['url'].$filename; ?>
						<a target="_blank" class="download-link" <?php if($filename): ?>style="display:block"<?php endif; ?> href="<?php echo esc_url($url); ?>"><?php esc_html_e('JSON file', 'vp-woo-pont'); ?></a>
						<?php if(!$filename): ?>
						<span><?php esc_html_e('Import failed or not finished yet', 'vp-woo-pont'); ?></span>
						<?php endif; ?>
						<a href="#" data-provider="<?php echo esc_attr($provider_id); ?>" class="import"><?php esc_html_e('Refresh', 'vp-woo-pont'); ?></a>
					</nav>
				</li>
			<?php endforeach; ?>
		</ul>
		<p><?php esc_html_e('JSON files are synced daily automatically. Click refresh to run the import manually. You can see every import in WooCommerce / Status / Scheduled Actions.', 'vp-woo-pont'); ?></p>
	</div>

	<?php if(!get_option('_vp_woo_pont_hide_rate_request')): ?>
		<div class="vp-woo-pont-settings-widget vp-woo-pont-settings-widget-rating">
			<h3><?php esc_html_e('Rating', 'vp-woo-pont'); ?> <span>⭐️⭐️⭐️⭐️⭐️</span></h3>
			<p><?php _e( 'Enjoyed <strong>VP Woo Pont</strong>? Please leave us a ★★★★★ rating. We appreciate your support!', 'vp-woo-pont' ); ?></p>
			<p>
				<a class="button-primary" target="_blank" rel="noopener noreferrer" href="https://wordpress.org/support/plugin/vp-woo-pont/reviews/?filter=5#new-post"><?php esc_html_e( 'Leave a review', 'vp-woo-pont' ); ?></a>
				<a class="button-secondary" data-nonce="<?php echo wp_create_nonce( 'vp-woo-pont-hide-rate-request' )?>"><?php esc_html_e( 'Hide this message', 'vp-woo-pont' ); ?></a>
			</p>
		</div>
	<?php endif; ?>

	<div class="vp-woo-pont-settings-widget">
		<h3><?php esc_html_e('Support', 'vp-woo-pont'); ?></h3>
		<ul>
			<li><a href="https://visztpeter.me/dokumentacio" target="_blank"><?php esc_html_e('Documentation', 'vp-woo-pont'); ?></a></li>
			<li><a href="mailto:support@visztpeter.me"><?php esc_html_e('E-mail (support@visztpeter.me)', 'vp-woo-pont'); ?></a></li>
			<li><a class="vp-woo-pont-restart-setup-wizard" href="#" data-url="<?php echo esc_url(admin_url( 'options.php?page=vp-woo-pont-walkthrough' )); ?>" data-nonce="<?php echo wp_create_nonce( 'vp-woo-pont-restart-setup-wizard' )?>"><?php esc_html_e('Setup wizard', 'vp-woo-pont'); ?></a></li>
		</ul>
	</div>

</div>
