<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Get saved values
$saved_values = get_option('vp_woo_pont_home_delivery', array());

//When to generate these documents
$shipping_methods = VP_Woo_Pont_Helpers::get_available_shipping_methods();
$providers = VP_Woo_Pont_Helpers::get_supported_providers_for_home_delivery();

?>

<tr valign="top">
	<th scope="row" class="titledesc"><?php echo esc_html( $data['title'] ); ?></th>
	<td class="forminp <?php echo esc_attr( $data['class'] ); ?>">
		<table class="vp-woo-pont-settings–inline-table vp-woo-pont-settings-home-deliveries">
			<?php foreach ($shipping_methods as $shipping_method_id => $shipping_method_name): ?>
				<tr>
					<td>
						<strong><?php echo esc_html($shipping_method_name); ?></strong>
					</td>
					<td>
						<ul>
							<li>
								<input type="radio" id="provider-<?php echo esc_attr($shipping_method_id); ?>-0" <?php if(isset($saved_values[$shipping_method_id])) checked($saved_values[$shipping_method_id],''); ?> name="vp_woo_pont_home_delivery[<?php echo esc_attr($shipping_method_id); ?>]" value="">
								<label for="provider-<?php echo esc_attr($shipping_method_id); ?>-0">
									<i class="vp-woo-pont-provider-icon-none"></i>
									<strong><?php esc_html_e('None', 'vp-woo-pont'); ?></strong>
								</label>
							</li>
							<?php foreach ($providers as $provider_id => $label): ?>
								<?php if($provider_id != 'custom' && VP_Woo_Pont_Helpers::is_provider_configured($provider_id)): ?>
									<li>
										<input type="radio" id="provider-<?php echo esc_attr($shipping_method_id); ?>-<?php echo esc_attr($provider_id); ?>" <?php if(isset($saved_values[$shipping_method_id])) checked($saved_values[$shipping_method_id], $provider_id); ?> name="vp_woo_pont_home_delivery[<?php echo esc_attr($shipping_method_id); ?>]" value="<?php echo esc_attr($provider_id); ?>">
										<label for="provider-<?php echo esc_attr($shipping_method_id); ?>-<?php echo esc_attr($provider_id); ?>">
											<i class="vp-woo-pont-provider-icon-<?php echo esc_attr($provider_id); ?>"></i>
											<strong><?php echo esc_html($label); ?></strong>
										</label>
									</li>
								<?php endif; ?>
							<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<?php echo $this->get_description_html( $data ); // WPCS: XSS ok. ?>
	</td>
</tr>
