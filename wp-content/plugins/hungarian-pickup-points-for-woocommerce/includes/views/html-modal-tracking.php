<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<script type="text/template" id="tmpl-vp-woo-pont-modal-tracking">
	<div class="wc-backbone-modal vp-woo-pont-modal-tracking">
		<div class="wc-backbone-modal-content">
			<section class="wc-backbone-modal-main" role="main">
				<header class="wc-backbone-modal-header">
					<h1><?php echo esc_html_e('Tracking informations', 'vp-woo-pont'); ?></h1>
					<button class="modal-close modal-close-link dashicons dashicons-no-alt">
						<span class="screen-reader-text"><?php esc_html_e( 'Close modal panel', 'woocommerce' ); ?></span>
					</button>
				</header>
				<# if ( data.has_events ) { #>
					<article>
						{{{ data.events }}}
					</article>
				<# } else { #>
					<article>
						<ul class="order_notes">
							<li class="note">
								<div class="note_content">
									<p><?php esc_html_e('Package created. Tracking number:', 'vp-woo-pont'); ?> {{data.parcel_number}}</p>
								</div>
							</li>
						</ul>
					</article>
				<# } #>
				<footer>
					<div class="inner">
						<span><?php echo esc_html('Tracking number', 'vp-woo-pont'); ?>: <strong>{{data.parcel_number}}</strong></span>
						<a class="button button-primary button-large" target="_blank" href="{{data.link}}"><?php esc_html_e( 'Tracking externally', 'vp-woo-pont' ); ?></a>
					</div>
				</footer>
			</section>
		</div>
	</div>
	<div class="wc-backbone-modal-backdrop modal-close"></div>
</script>
